<?php 
include_once 'control/class/Session.class.php';

$sesion = new SesionManager();
	if($sesion->GetStatusSession()){
		header("Location: control/.");
		exit();
	}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>Administraci&oacute;n</title>
		<!-- Local -->
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="./css/bootstrap.min.css">
		<!-- Optional theme -->
		<link rel="stylesheet" href="./css/bootstrap-theme.min.css">
        <!-- Remote-->
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		<!-- Main Style-->
		<link rel="stylesheet" type="text/css" href="./css/acceso.css" />
    </head>
	
    <body style="background-color: white;">
        <section id="login" style="border-bottom: 2px solid blue">
        	<div style="background-color: rgba(0,0,100, 0.7); color: white; font: Arial, sans-serif; height: 50px; text-align: center;">
				<div class="col-lg-8 col-lg-offset-2"><b> Acceso a Panel </b></div>
        	</div>
        	<br><br>
			<br><br>
            <form method="post">
            	<div class="container">
            		<div class="row" style="text-align: center;">
            			<div class="col-lg-6 col-lg-offset-3">
            				<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Administre su p&aacute;gina a trav&eacute;s del Panel</h3>
								</div>
            					<div class="panel-body">
            						<div id="status"></div>
            						<div class="form-group">
            						<label>Usuario</label>
            						<input id="uname" type="text" required="required" class="form-control" name="uname">
            						<label>Contrase&ntilde;a</label>
            						<input id="upass" type="password" required="required" class="form-control" name="upass">
            						<br>
            						<input type="button" value="Enviar" onclick="IniciarSesion();" class="btn btn-primary btn-block" >
            						</div>
            					</div>
								<div class="panel-footer">
									<a id="to_recover" class="btn btn-block">¿Olvid&oacute; su correo o contrase&ntilde;a? <br>Pulse para recuperar</a>
								</div>
            				</div>
            			</div>
            		</div>
            	</div>
            </form>
			<br><br>
			<br><br>
        </section>
        
		<section id="recovery">
			<div style="background-color: rgba(0, 150, 0, 0.7); color: white; font: Arial, sans-serif; height: 50px; text-align: center;">
				<div class="col-lg-8 col-lg-offset-2"><b> Recuperaci&oacute;n </b></div>
        	</div>
        	<br><br>
			<form class="form">
				<div class="container">
					<div class="row">
						<div class="col-xs-6 col-xs-offset-3">
							<div class="panel panel-primary">
								<div class="panel-heading">
									Opciones de Recuperaci&oacute;n
								</div>
								<div class="panel-body" style="text-align: center;">
									<b>Proporcione su nombre de usuario</b>
									<input type="text" id="recover_user" placeholder="Ej. user" class="form-control"/>
									<br><b>o</b><br><br>
									<b>Proporcione su correo electr&oacute;nico</b>
									<input type="text" id="recover_mail" placeholder="Ej. mail@mail.com" class="form-control"/>
								</div>
								<div class="panel-footer">
									<div class="btn-group btn-group-justified" role="group">
										<div class="btn-group" role="group">
											<input id="recover_send" class="btn btn-success" value="Enviar"/>
										</div>
										<div class="btn-group" role="group">
											<input id="recover_cancel" class="btn btn-danger" value="Cancelar"/>
										</div>
									</div>
								</div>
							</div>
							<div id="stat_recover"></div>
						</div>
					</div>
				</div>
			</form>
		</section>
        <!-- Scripts Area-->
    	<!-- JQuery -->
		<script src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<!-- Local -->
		<!-- JQuery -->
    	<script type="text/javascript" src="./js/jquery-2.2.4.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script type="text/javascript" src="./js/bootstrap.min.js"></script>	
        <script type="text/javascript" src="./js/Acceso.js"></script>
        <script type="text/javascript" src="./js/util/Alert.js"></script>
    </body>
</html>