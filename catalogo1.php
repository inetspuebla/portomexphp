<!DOCTYPE html>

<html lang="es">

<head>
	<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
	<title>Porto Mex. Puertas Autom&aacute;ticas</title>
	<link href="./images/PM_ico_small.ico" type="image/x-icon" rel="shortcut icon" />
	<!-- Local -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<!-- Optional theme -->
	<link rel="stylesheet" href="./css/bootstrap-theme.min.css">
	<!-- Remote-->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	
	<link rel="stylesheet" type="text/css" href="./css/MainStyle.css" />
	<link rel="stylesheet" type="text/css" href="./css/catalogo1.css" />

</head>

<body>
	<heder>
		<?php include "./inc/menu.php" ?>
	</heder>

	<div style="height: 110px; background-color:black;">
	</div>

	<div class="container-fluid" style="margin-top: 70px;">
		<div class="row">
			<div class="col-xs-offset-1 col-xs-10 col-md-2 hidden-sm hidden-xs">
				<ul id="menu_pruductos" class="nav nav-pills nav-stacked">
					<li class="controles"><a  data-toggle="pill" href="#controles"> Controles </a></li>
					<li class="abre_rejas"><a  data-toggle="pill" href="#abre_rejas"> Abre Rejas </a></li>
					<li id="btn_abre_puertas" class="abre_puertas"><a  data-toggle="pill" href="#abre_puertas"> Abre Puertas </a></li>
				</ul>
			</div>

			<div class="col-xs-11 col-xs-offset-1 col-sm-9 col-sm-offset-3 visible-sm visible-xs" style="margin-bottom: 20px;">
				<ul id="menu_pruductos" class="nav nav-pills visible-sm visible-xs">
					<li class="controles"><a  data-toggle="pill" href="#controles" style="width: 132px; text-align: center;"> Controles </a></li>
					<li class="abre_rejas"><a  data-toggle="pill" href="#abre_rejas" style="width: 132px; text-align: center;"> Abre Rejas </a></li>
					<li id="btn_abre_puertas" class="abre_puertas"><a  data-toggle="pill" href="#abre_puertas" style="width: 132px; text-align: center;"> Abre Puertas </a></li>
				</ul>
			</div>

			<div class="col-xs-offset-1 col-xs-10 col-sm-10 col-sm-offset-1 col-md-offset-0 col-md-8">
				<div class="tab-content">
					<div id="controles" class="tab-pane fade in active">
						<div id="titulo1">
							<h1> CONTROLES </h1>
						</div>

						<div class="row">
							<div class="col-sm-4 col-md-4">
								<div class="thumbnail">
									<img src="./images/catalogo1/control1.png" >
									<div class="caption">
										<p> NOMBRE: CONTROL 1 </p>
										<p> MODELO: </p>
										<p> MARCA: </p>
									</div>
								</div>
							</div>
							<div class="col-sm-4 col-md-4">
								<div class="thumbnail">
									<img src="./images/catalogo1/control2.png" >
									<div class="caption">
										<p> NOMBRE: CONTROL 2 </p>
										<p> MODELO: </p>
										<p> MARCA: </p>
									</div>
								</div>
							</div>
							<div class="col-sm-4 col-md-4">
								<div class="thumbnail">
									<img src="./images/catalogo1/control3.png" >
									<div class="caption">
										<p> NOMBRE: CONTROL 3 </p>
										<p> MODELO: </p>
										<p> MARCA: </p>
									</div>
								</div>
							</div>
							<div class="col-sm-4 col-md-4">
								<div class="thumbnail">
									<img src="./images/catalogo1/control2.png" >
									<div class="caption">
										<p> NOMBRE: CONTROL 4 </p>
										<p> MODELO: </p>
										<p> MARCA: </p>
									</div>
								</div>
							</div>
							<div class="clearfix">
							</div>
						</div>
					</div>

					<div id="abre_rejas" class="tab-pane fade">
						<div id="titulo1">
							<h1> ABRE REJAS </h1>
						</div>

						<div class="row">
							<div class="col-sm-4 col-md-4">
								<div class="thumbnail">
									<img src="./images/catalogo1/a-rejas1.png" >
									<div class="caption">
										<p> NOMBRE:  </p>
										<p> MODELO: </p>
										<p> MARCA: </p>
									</div>
								</div>
							</div>
							<div class="col-sm-4 col-md-4">
								<div class="thumbnail">
									<img src="./images/catalogo1/a_rejas2.png" >
									<div class="caption">
										<p> NOMBRE:  </p>
										<p> MODELO: </p>
										<p> MARCA: </p>
									</div>
								</div>
							</div>
							<div class="col-sm-4 col-md-4">
								<div class="thumbnail">
									<img src="./images/catalogo1/a_rejas3.png" >
									<div class="caption">
										<p> NOMBRE:  </p>
										<p> MODELO: </p>
										<p> MARCA: </p>
									</div>
								</div>
							</div>
							<div class="col-sm-4 col-md-4">
								<div class="thumbnail">
									<img src="./images/catalogo1/a_rejas2.png" >
									<div class="caption">
										<p> NOMBRE:  </p>
										<p> MODELO: </p>
										<p> MARCA: </p>
									</div>
								</div>
							</div>
							
							<div class="col-sm-4 col-md-4">
								<div class="thumbnail">
									<img src="./images/catalogo1/a_rejas3.png" >
									<div class="caption">
										<p> NOMBRE:  </p>
										<p> MODELO: </p>
										<p> MARCA: </p>
									</div>
								</div>
							</div>
							<div class="col-sm-4 col-md-4">
								<div class="thumbnail">
									<img src="./images/catalogo1/a-rejas1.png" >
									<div class="caption">
										<p> NOMBRE:  </p>
										<p> MODELO: </p>
										<p> MARCA: </p>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div id="abre_puertas" class="tab-pane fade">
						<div id="titulo1">
							<h1> ABRE PUERTAS </h1>
						</div>

						<div class="row">
							<div class="col-sm-4 col-md-4">
								<div class="thumbnail">
									<img src="./images/catalogo1/a-puertas1.png" >
									<div class="caption">
										<p> NOMBRE:  </p>
										<p> MODELO: </p>
										<p> MARCA: </p>
									</div>
								</div>
							</div>
							<div class="col-sm-4 col-md-4">
								<div class="thumbnail">
									<img src="./images/catalogo1/a-puertas2.png" >
									<div class="caption">
										<p> NOMBRE:  </p>
										<p> MODELO: </p>
										<p> MARCA: </p>
									</div>
								</div>
							</div>
							<div class="col-sm-4 col-md-4">
								<div class="thumbnail">
									<img src="./images/catalogo1/a-puertas3.png" >
									<div class="caption">
										<p> NOMBRE:  </p>
										<p> MODELO: </p>
										<p> MARCA: </p>
									</div>
								</div>
							</div>
							<div class="clearfix">
							</div>
							<div class="col-sm-4 col-md-4">
								<div class="thumbnail">
									<img src="./images/catalogo1/a-puertas4.png" >
									<div class="caption">
										<p> NOMBRE:  </p>
										<p> MODELO: </p>
										<p> MARCA: </p>
									</div>
								</div>
							</div>
							
							<div class="col-sm-4 col-md-4">
								<div class="thumbnail">
									<img src="./images/catalogo1/puerta_aluminio.png" >
									<div class="caption">
										<p> NOMBRE: PUERTA ESTILO CARRUAJE </p>
										<p> MODELO: </p>
										<p> MARCA: </p>
									</div>
								</div>
							</div>
							<div class="col-sm-4 col-md-4">
								<div class="thumbnail">
									<img src="./images/catalogo1/puerta_carruaje.png" >
									<div class="caption">
										<p> NOMBRE: PUERTA ESTILO CARRUAJE </p>
										<p> MODELO: </p>
										<p> MARCA: </p>
									</div>
								</div>
							</div>
							<div class="clearfix">
							</div>						
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<!-- Scripts Area-->
	<!-- Local -->
	<!-- JQuery -->
	<script type="text/javascript" src="./js/jquery-2.2.4.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script type="text/javascript" src="./js/bootstrap.min.js"></script>
	<!-- Remote -->
	<!-- JQuery -->
	<script src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<!-- Requires -->
		<!-- Toogle Mobile Menu -->
	<script>
		function toggleMobileMenu()
		{
			var $mobileMenu = $('#mobile-main-menu');
			$mobileMenu.slideToggle('fast');
		}
		$(document).ready(function() {
			$('#mobile-menu-button').on('click', toggleMobileMenu);
			$('.itmnmbl').on('click', toggleMobileMenu);        
		});
	</script>

<?php
if($_SERVER['REQUEST_METHOD'] == "GET"){
	if(isset($_GET['categoria'])){
		$var = $_GET['categoria'];

		if($var != ""){
			$msg = '
				<script type="text/javascript">
				if($("#'.$var.'").length >0){
					$(".tab-pane").removeClass("in active");
					$("#'.$var.'").addClass("in active");
					$(".'.$var.'").addClass("active");
				}
				</script>
			';

			echo $msg;
		}
	}else {
		
	}
}
?>
</body>
</html>

