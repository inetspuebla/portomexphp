<?php
require_once("base_login.php");

class DBManager{
    /** Acceso estatico */
    private static $db = null;

    /** PDO */
    private static $pdo;

    /** Constructor */
    final private function __construct(){
    	try
    	{
			// Crear nueva conexi�n PDO
			self::getDB();
		}
		catch (PDOException $e)
		{
			// Manejo de excepciones
		}
    }

    /** Destructor */
    function _destructor(){
        self::$pdo = null;
    }

    /**
    * Retorna la referencia de BD
    * @return DBManager object, null on exception
    */
    public static function getInstance(){
		if (self::$db === null) {
			self::$db = new self();
		}
		return self::$db;
    }

    /**
    * Crear una nueva conexion PDO con base en las credenciales
    * @return PDO object, null on exception
    */
    public function getDB(){
		if (self::$pdo == null) {
			self::$pdo = new PDO(
					'mysql:dbname=' . DB_NAME.
					';host=' . HOST_NAME . ";",
					USER_NAME,
					USER_PASS,
					array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
					);

			// Habilitar excepciones
			self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}

		return self::$pdo;
    }

    /* Evitar la clonacion de instancia */
    final protected function __clone() {}
}
?>
