<?php
/* Include Area */
include_once 'core.php';
include_once 'src_mail/mail_defines.php';
/* Envio de la Respuesta en formato JSON */
header("Content-Type: application/json");

$code = 0;
$message = "";
$data = array();

try {
	$bd_conn= ConexionBD::obtenerInstancia()->obtenerBD();
	
	if($_SERVER['REQUEST_METHOD'] == "GET"){
		$code = 302;
		$message = "Conexion Establecida";
	}
	elseif($_SERVER['REQUEST_METHOD'] == "POST")
	{
		if( isset($_POST['nombre']) 
		and isset($_POST['lugar']) 
		and isset($_POST['telefono']) 
		and isset($_POST['correo']) 
		and isset($_POST['mensaje']) ){
			
			$nom = trim($_POST['nombre']);
			$lug = trim($_POST['lugar']);
			$tel = trim($_POST['telefono']);
			$mail = trim($_POST['correo']);
			$msg = trim($_POST['mensaje']);
			
			/*Guardar en la Base de Datos*/
			/* Preparar Consulta */
			$sql = "INSERT INTO ".
				" mensaje (`sNombre`, `sLugar`, `sCorreo`, `iTelefono`, `sTexto`)".
				" VALUES (?,?,?,?,?)";
			
			$prep_sta = $bd_conn->prepare($sql);
			
			$prep_sta->bindParam(1, $nom, PDO::PARAM_STR);
			$prep_sta->bindParam(2, $lug, PDO::PARAM_STR);
			$prep_sta->bindParam(3, $mail, PDO::PARAM_STR);
			$prep_sta->bindParam(4, $tel, PDO::PARAM_STR);
			$prep_sta->bindParam(5, $msg, pdo::PARAM_STR);
			
			$msg_en_bd = CORREO_PREPARANDO;
			
			$result = $prep_sta->execute();
			if(!$result)
				$msg_en_bd = CORREO_ERROR_ALMACENAR;
			else
				$msg_en_bd = CORREO_ALMACENADO;
			
			$last_id = $bd_conn->lastInsertId();

			$sql = "SELECT sEmail as toemail FROM perfil WHERE iId = 1";

			$prep = $bd_conn->prepare($sql);

			$res = $prep->execute();

			$arr = array();
			
			if($res){
				$arr = $prep->fetchAll(PDO::FETCH_ASSOC);

				if(count($arr) == 1){
						$arr = $arr[0];
				}else {
					throw new PetitionException("Mensaje Recibido. Gracias!", 201); 
				}
			}else {
				throw new PetitionException("Mensaje Recibido. Gracias!", 201); 
			}
			
			$data['send_to'] = $arr;

			/*Enviar Por Correo Electronico */
			$msg_en_mail = CORREO_PREPARANDO;

			# Definimos el contenido HTML del correo
			//* Llamar al contenido de la Plantilla HTML de Correo
			$dom = new DOMDocument();
			$root = $dom->createElement("p");
			try{
				@$root = $dom->load("../inc/Correo_Modelo_2.html");
#				@$root = $dom->load("../inc/PlantillaCorreo.html");
			}catch (Exception $e){
				throw $e;
			}
			//* Se obtiene la estructura del archivo en una cadena de caracteres
			$string = $dom->saveHTML($dom);
			if(trim($string) == ""){
				$data = print_r($dom, true);
				throw new PetitionException("No se pudo procesar el email.", 403);
			}
			unset($dom); unset($root);
			//* Reemplazar los contenidos correspondientes
			//*$string = str_replace("<!DOCTYPE html>\n", "", $string);
			$string = str_replace("<html lang=\"es\">\n", "", $string);
			$string = str_replace("</html>\n", "", $string);
			/*$string = str_replace("<body>\n", "", $string);
			$string = str_replace("</body>\n", "", $string);*/
			$string = str_replace("%PARAM_NAME%", $nom, $string);
			$string = str_replace("%PARAM_PLACE%", $lug, $string);
			$string = str_replace("%PARAM_PHONE%", $tel, $string);
//			$string = str_replace("\"", "'", $string);
			$email = "";
			if($mail == "")
				$email = "<i>No proporcionado</i>";
			else
				$email = "<a style='color:gold;' href=\"mailto:".$mail."\">".$mail."</a>";
			
			$string = str_replace("%PARAM_MAIL%", $email, $string);
			$string = str_replace("%PARAM_DATE%", date("r"), $string);
			$string = str_replace("%PARAM_MSG%", $msg , $string);

			$cabeceras = 'Content-type: text/html; charset=utf-8' . "\r\n".
    			'From: PortoMex Contacto <contacto@portomex.com.mx>' . "\r\n" .
    			'Reply-To: PortoMex Contacto <'.$arr['toemail'].'>' . "\r\n" .
    			'X-Mailer: PHP/' . phpversion()."\r\n" ;
			
			$res = @mail($arr['toemail'], "Nuevo Mensaje de su Página Web", $string, $cabeceras);
			$res = @mail("test_inets@outlook.com", "Nuevo Mensaje de su Página Web", $string, $cabeceras);

			if($res){
				$msg_en_mail = CORREO_ENVIADO;
			}else {
				$msg_en_mail = CORREO_ERROR_ENVIAR;
			}

			$general_estatus = $msg_en_bd | $msg_en_mail;
			
			if($general_estatus == CORREO_OK_ALMACENADO_OK_ENVIADO){
				$code = 201;
				$message = "Enviado Con &Eacute;xito";
				$data['str'] = $string;
				$sql = "UPDATE mensaje SET bCorreo = ? WHERE iId = ?";
				
				$prep_sta = $bd_conn->prepare($sql);
				$val = 1;
				$prep_sta->bindParam(1, $val, PDO::PARAM_INT);
				$prep_sta->bindParam(2, $last_id, PDO::PARAM_INT);
				
				$res = $prep_sta->execute();
				
			}elseif ($general_estatus == CORREO_NO_ALMACENADO_NO_ENVIADO) {
				$code = 403;
				$message = "Error de Envío, Intente nuevamente.";
			}elseif ($general_estatus == CORREO_NO_ALMACENADO_OK_ENVIADO ) {
				$data['str'] = $string;
				$code = 201;
				$message = "Mensaje Enviado. Gracias!";
			}elseif ($general_estatus == CORREO_OK_ALMACENADO_NO_ENVIADO) {
				$data['str'] = $string;
				$code = 201;
				$message = "Mensaje Recibido. Gracias!";
			}else {
				throw new PetitionException("Error Al Ejecutar la Operacion", 400);
			}
		}else {
			throw new PetitionException("Error en las variables de peticion", 400);
		}
	}else{
		$code = 404;
		$message = "Peticion No Reconocida: ".$_SERVER['REQUEST_METHOD'];
	}
} catch (PDOException $pdoex){
	$code = $pdoex->getCode();
	$message = "Error con la Base de Datos";
} catch (Exception $ex) {
	$code = $ex->getCode();
	$message = $ex->getMessage();
}

$out_put = array('code'=> $code, 'message' => $message, 'data' => $data);

$json = json_encode($out_put);

echo $json;
?>