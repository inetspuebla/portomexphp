<?php
include_once 'class/Session.class.php';
$sesion = new SesionManager();
if(!$sesion->GetStatusSession()){ header("Location: ../acceso.php"); exit(); }

if($sesion->GetValue('username') == null) {$sesion->CloseSession(); unset($sesion); header("Location: ../acceso.php"); exit();}
?>

<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<title>Panel</title>
	<!-- Local -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<!-- Optional theme -->
	<link rel="stylesheet" href="../css/bootstrap-theme.min.css">
	<!-- Remote-->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
		integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
		crossorigin="anonymous">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
		integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
		crossorigin="anonymous">

	<link rel="stylesheet" type="text/css" href="../css/sidebar.css"
		media="all">
	<link rel="stylesheet" type="text/css" href="../css/Panel.css">
	<link rel="stylesheet" type="text/css" href="../css/jquery-te-1.4.0.css">
</head>

<body id="page_top">
	<div id="wrapper">
		<!-- Menu Lateral -->
		<div id="sidebar-wrapper">
			<!--sidebar-nav-->
			<ul class="nav nav-pills nav-stacked">
				<li class="sidebar-brand" align="center">
					<a href="."> <span class="glyphicon glyphicon-home"></span>						Panel de Control v0.1
					</a>
				</li>
				<li><br><br></li>
				<li><a href="../." target="_blank"><span></span>Mi Sitio</a></li>
				<li><br></li>
				<li><a href="."> <span class="glyphicon glyphicon-arrow-left"
						aria-hidden="true"></span><i> Regresar</i>
				</a></li>
				<li><a><span></span><b>Operaciones</b></a></li>
				<li><a href="#page_top"> <span class="glyphicon glyphicon-book"
						aria-hidden="true"></span> Presentaci&oacute;n
				</a></li>
				<li><a href="#options_images"> <span class="glyphicon glyphicon-book"
						aria-hidden="true"></span> Administrar Opciones
				</a></li>
				<li><a href="#catalogos"> <span class="glyphicon glyphicon-book"
						aria-hidden="true"></span> Cat&aacute;logos
				</a></li>
				<li><a class="disabled"><span></span>Acciones</a></li>
				<li><a id="menu-close" href="#"> <span class="glyphicon glyphicon-menu-hamburger"
							aria-hidden="true"></span> Ocultar Men&uacute;
				</a></li>

			</ul>
		</div>
		<!-- Boton Menu Desplegable-->
		<div id="menu_up">
			<a id="menu-toggle" class="btn btn-lg"> <span class="glyphicon glyphicon-menu-hamburger"
					aria-hidden="true"></span> Men&uacute; Lateral
			</a>
			<label class="btn btn-lg"> Nombre de Usuario: &nbsp; <span
				class="glyphicon glyphicon-user" aria-hidden="true"></span>&nbsp;<b id="user_name"></b>
			</label>
		</div>
		<!-- Contenido -->
		<div id="page-content-wrapper">
			<!-- Images -->
			<div id="catalogos" class="areaitem">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-8 col-xs-offset-2 col-md-8 col-md-offset-1 area_item_title">
							<h2>Cat&aacute;logos</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-12 col-md-offset-0">
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="btn-group btn-group-justified" role="group">
										<div class="btn-group" role="group">
											<label class="btn"><b>Acciones</b></label>
										</div>
										<div class="btn-group" role="group">
											<button type="button" class="btn btn-success" data-toggle="modal"
								data-target="#modal_new_image" onclick="$('#status').html('');">Nueva Imagen</button>
										</div>
										<div class="btn-group" role="group">
											<button type="button" class="btn btn-primary"
								onclick="CargarTodo();">Recargar</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div id="st_cat" class="col-xs-12"></div>
					</div>
					<div class="row">
						<div class="col-xs-3 col-md-2">
							<ul id="nav_catg" class="nav nav-pills nav-stacked" >
								<li><a data-toggle="pill" href="#cat_producto">Productos</a></li>
								<li><a data-toggle="pill" href="#cat_galeria">Galer&iacute;a</a></li>
								<li><a data-toggle="pill" href="#cat_promocion">Promociones</a></li>
								<li><a data-toggle="pill" href="#cat_otros">Otros</a></li>
							</ul>
						</div>
						<div class="col-xs-8 col-md-9">
							<div id="tab_catg_data" class="tab-content">
								<div id="cat_producto" class="tab-pane fade in active">
									1
								</div>
								<div id="cat_galeria" class="tab-pane fade">
									2
								</div>
								<div id="cat_promocion" class="tab-pane fade">
									3
								</div>
								<div id="cat_otros" class="tab-pane fade">
									4
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<!--
			<button class="btn btn-lg" data-toggle="modal" data-target="#modal_new_image" tabindex="-1" role="dialog">
				Show Modal New Image
			</button>
			-->
			<!--
			<button class="btn btn-lg" data-toggle="modal" data-target="#modal_edit_image" tabindex="-1" role="dialog">
				Show Modal New Image
			</button>
			-->
		</div>
	</div>

	<!-- Modals -->
	<div id="modal_new_image" class="modal fade">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4>Subir Imagen</h4>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
						<div class="row">
							<div id="estatus_upload" class="col-xs-12"></div>
							<div class="col-xs-5">
								<div class="thumbnail">
									<img id="imagen-up" alt="No-Image" src="../images/no-thumbnail-square.png">
								</div>
							</div>
							<div class="col-xs-7">
								<div id="image_upload_panel" class="panel panel-default">
									<div class="panel-heading">
										<label for="archivo_imagen">Subir imagen</label>
									</div>
									<div class="panel-body">
										<div class="form-group">
											<form id="form-upload" enctype="multipart/form-data"
												method="POST" role="form">
												<div id="inp_img_selct">
													<input type="file" id="archivo_imagen" name="uploadedfile"
														accept="image/*" onchange="HandleOnChange_FileChooser();"
														class="btn btn-block btn-info" />
												</div>
												<input type="button" id="imgupload" value="Subir archivo"
													disabled="disabled" class="btn btn-primary"
													onclick="EnviarImage();" />
											</form>
										</div>
									</div>
								</div>

								<div id="pnl-desc" class="panel panel-default hideElem">
									<div class="panel-heading">
										<label> A&ntilde;ada una descripci&oacute;n</label>
									</div>
									<div class="panel-body">
										<div class="form-group">
											<div class="col-xs-8">
											<label for="nombre_imagen">T&iacute;tulo</label>
											<input id="nombre_imagen" type="text" placeholder="Nombre de Imagen"
												required="required" class="form-control"/>
											</div>
											<div class="col-xs-4">
											<label>Tipo Imagen</label>
											<!--<select id="n_tipo_imagen" class="form-control" name="Tipo de Imagen"></select>-->
											<div class="dropdown">
												<button id="opt_rtipoimagen" class="btn btn-default btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
													<label id="sel_rtimg" value="0">Tipo</label>
													<span class="caret"></span>
												</button>
												<ul id="ul_registrar_imagen" class="dropdown-menu" aria-labelledby="opt_rtipoimagen">
													<li><a>None</a></li>
												</ul>
											</div>
											</div>
											<div class="col-xs-12">
											<label for="descripcion_imagen">Descripci&oacute;n</label>
											<textarea id="descripcion_imagen" rows="10" placeholder="Introduce una descripcion"
												required="required" class=""></textarea>
											<input type="button" value="Guardar Cambios" class="btn btn-success"
													onclick="RegistrarImagen()"/>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="modal_edit_image" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modaledit">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4> Editar Imagen </h4>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
						<div class="row">
							<div id="editstat" class="col-xs-12"></div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<input id="eid" type="hidden" value="0">
								<div class="thumbnail">
									<img id="eimage" alt="none"
										src="../images/no-thumbnail-square.png" class="thumbnail"
										style="max-width: 200px; max-height: 200px;"
									/>
								</div>
							</div>
							<div class="col-xs-8">
								<div class="container-fluid">
									<div class="row">
										<div class="col-xs-8">
											<label>T&iacute;tulo</label>
											<input id="ename" type="text" class="form-control"/>
										</div>
										<div class="col-xs-4">
											<label>Tipo Imagen</label>
											<div class="dropdown">
												<button id="opt_etipoimagen" class="btn btn-default btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
													<label id="sel_etimg" value="0">Tipo</label>
													<span class="caret"></span>
												</button>
												<ul id="ul_editar_imagen" class="dropdown-menu" aria-labelledby="opt_etipoimagen">
													<li><a>None</a></li>
												</ul>
											</div>
										</div>
										<div class="col-xs-12">
											<label>Descripci&oacute;n</label>
											<textarea id="etext" rows="10" cols="15" class="form-control"></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-success" onclick="SalvarCambiosImagen()">Salvar Cambios</button>
					<button id="eclose" type="button" class="btn btn-danger"
						data-dismiss="modal">Cancelar</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Scripts Area-->
	<!-- Remote -->
	<!-- JQuery -->
	<script src="https://code.jquery.com/jquery-2.2.4.min.js"
		integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
		crossorigin="anonymous"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
		integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
		crossorigin="anonymous"></script>
	
	<!-- Local-->
	<script type="text/javascript" src="../js/jquery-1.12.2.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<!-- Requires -->
	<script type="text/javascript" src="../js/jquery-te-1.4.0.min.js"></script>
	<script type="text/javascript" src="../js/util/Alert.js"></script>
	<script type="text/javascript" src="../js/util/DOM_Element.js"></script>
	<script type="text/javascript" src="../js/util/Imagen_Thumbnail.js"></script>
	<script type="text/javascript" src="../js/util/Thumbnail_Bootstrap.js"></script>

	<script type="text/javascript" src="../js/neg/CatalogoImagen.js"></script>
	<script type="text/javascript" src="../js/neg/UploadImage.js"></script>



	<!-- Menu Toggle Script -->
	<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    $("#menu-close").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
		<script type="text/javascript">
	//Plugin de Editor de Texto HTML
	
	$(document).ready(function() {
		$("#descripcion_imagen").jqte();
		$("#etext").jqte();
	}); 
	
	</script>
</body>

</html>
