<?php
include_once 'core.php';
/* Envio de la Respuesta en formato JSON */
header("Content-Type:application/json");
$code = 0;
$message = "";
$data = array();

try{
	$bd_conn= ConexionBD::obtenerInstancia()->obtenerBD();
	if($_SERVER['REQUEST_METHOD'] == "GET"){
		$session = new SesionManager();
		if($session->GetStatusSession()){
			if(isset($_GET['keyall']) and $session->GetValue('userid') != null){
				
				$sql = "SELECT ".				
				" sNombre    as nombre    ,".
				" sCalle     as calle     ,".
				" sNumext    as numExt    ,".
				" sNumint    as numInt    ,".
				" sColonia   as colonia   ,".
				" sMunicipio as municipio ,".
				" sEstado    as estado    ,".
				" iTel1      as t0        ,".
				" iTel2      as t1        ,".
				" sEmail     as mail      ,".
				" sDesc      as descr      ".
				" FROM perfil ".
				"WHERE iId =".intval($session->GetValue('userid'));
				
				$res = $bd_conn->query($sql);
				
				if($res->rowCount() > 0){
					$code = 200;
					$message = "Ok";
					$arr = $res->fetchAll(PDO::FETCH_ASSOC)[0];
					$data = array(
							'nombre'=>$arr['nombre'], 
							'direccion'=> array(
									'calle'=>$arr['calle'],
									'numext'=>$arr['numExt'],
									'numint'=>$arr['numInt'],
									'colonia' => $arr['colonia'],
									'municipio' => $arr['municipio'],
									'estado'=>$arr['estado']
							), 
							'tel'=>array($arr['t0'], $arr['t1']),
							'mail'=>$arr['mail'],
							'descripcion'=>$arr['descr']
							);
				}else{
					throw new PetitionException("Consulta Inv&aacute;lida", 404);
				}
			}elseif(isset($_GET['nameu'])){
				$name = $session->GetValue('username');

				if($name != null){
					$code = 200;
					$message = $name;
				} else {
					$code = 400;
					$message = null;
				}
			}else{
				throw new RequestException("Parametros Inv&aacute;lidos", 404);
			}
		}else{
			throw new PetitionException("Requiere inicio de Sesion.", 400);
		}
	}else if($_SERVER['REQUEST_METHOD'] == "POST"){
		$session = new SesionManager();
		if( $session->GetStatusSession() ){
			if( isset($_POST['nombre']) 
			and isset($_POST['direccion']) 
			and isset($_POST['tel']) 
			and isset($_POST['mail'])
			and isset($_POST['descripcion'])
			and isset($_SESSION['userid'])
			){
				$nombre = $_POST['nombre'];
				$calle = $_POST['direccion']['calle'];
				$numext = $_POST['direccion']['numext'];
				$numint = $_POST['direccion']['numint'];
				$colonia = $_POST['direccion']['colonia'];
				$municipio = $_POST['direccion']['municipio'];
				$estado = $_POST['direccion']['estado'];
				$tel = $_POST['tel'];
				$mail = $_POST['mail'];
				$descripcion = $_POST['descripcion'];
				
				$tel[0] == "" ? 0:$tel[0];
				$tel[1] == "" ? 0:$tel[1];
				$ii =  intval($_SESSION['userid']);
				$sql = "UPDATE perfil SET ".
					" sNombre    = ?, ".
					" sCalle     = ?, ".
					" sNumext    = ?, ".
					" sNumint    = ?, ".
					" sColonia   = ?, ".
					" sMunicipio = ?, ".
					" sEstado    = ?, ".
					" iTel1      = ?, ".
					" iTel2      = ?, ".
					" sEmail     = ?, ".
					" sDesc      = ?  ".
					" WHERE ".
					" iId        = ?  ";
				
				$prep = $bd_conn->prepare($sql);
				
				$prep->bindParam( 1, $nombre, PDO::PARAM_STR);
				$prep->bindParam( 2, $calle, PDO::PARAM_STR);
				$prep->bindParam( 3, $numext, PDO::PARAM_STR);
				$prep->bindParam( 4, $numint, PDO::PARAM_STR);
				$prep->bindParam( 5, $colonia, PDO::PARAM_STR);
				$prep->bindParam( 6, $municipio, PDO::PARAM_STR);
				$prep->bindParam( 7, $estado, PDO::PARAM_STR);
				$prep->bindParam( 8, $tel[0], PDO::PARAM_INT);
				$prep->bindParam( 9, $tel[1], PDO::PARAM_INT);
				$prep->bindParam(10, $mail, PDO::PARAM_STR);
				$prep->bindParam(11, $descripcion, PDO::PARAM_STR);
				$prep->bindParam(12, $ii, PDO::PARAM_INT);

				$res = $prep->execute();
				if($res){
					$code = 201;
					$message = "Actualizado Correctamente";
					$data = $_POST;
				}else {
					$code = 401;
					$message = "Error al actualizar";
				}
			}else{
				throw new RequestException("Parametros Invalidos para la Petici&oacute;n", 400);
			}
		}else {
			throw new PetitionException("Inicie Sesi&oacute;n primero");
		}
	}else {
		throw RequestException("Parametros Invalidos para la Petici&ooacute;n", 400);
	}
}
catch (RequestException $rqex){
	$code = $rqex->getCode();
	$message = $rqex->getMessage();
}
catch (PetitionException $ptex){
	$code = $ptex->getCode();
	$message = $ptex->getMessage();
}
catch (PDOException $pdoex){
	$code = $pdoex->getCode();
	$message = "Error con la base de datos<br>".$pdoex->getMessage();
}
catch (Exception $ex){
	$code = $ex->getCode(); //Undefined
	$message = "Error";
}

/*Preparar Respuesta*/
$out_put = array('code'=> $code, 'message'=> $message, 'data'=>$data);
/*Encode JSON*/
$json_res = json_encode($out_put);
/*Desplegar*/
echo $json_res;

?>