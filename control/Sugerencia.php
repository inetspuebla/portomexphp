<?php 
include_once 'core.php';
include_once 'src_mail/mail_defines.php';
/* Envio de la Respuesta en formato JSON */
header("Content-Type:application/json");
$code = 0;
$message = "";
$data = array();

try{
    if($_SERVER['REQUEST_METHOD'] == "GET"){
		$code = 302;
		$message = "Conexion Establecida";
	}
	elseif($_SERVER['REQUEST_METHOD'] == "POST")
	{
		if(isset($_POST['nombre']) and isset($_POST['asunto']) and isset($_POST['texto']))
		{
			$nom =$_POST['nombre'];
			$asunto=$_POST['asunto'];
			$msg=$_POST['texto'];

			
			/*Enviar por Mail*/
			include("src_mail/phpmailer.php");

			$smtp=new PHPMailer();

			# Indicamos que vamos a utilizar un servidor SMTP
			$smtp->IsSMTP();

			# Definimos el formato del correo con UTF-8
			$smtp->CharSet="UTF-8";

			# autenticación con nuestro servidor smtp
			$smtp->SMTPAuth   = true;
			$smtp->SMTPSecure = SECURITY_PROTOCOL;
			$smtp->Host       = HOST_MAIL;
			$smtp->Username   = NAME_MAIL;
			$smtp->Password   = PASS_MAIL;
			$smtp->Port       = PORT_MAIL;
			# datos de quien realiza el envio
			$smtp->From       = REMITENTE_CORREO; 
			$smtp->FromName   = REMITENTE_NOMBRE; 

			# Indicamos las direcciones donde enviar el mensaje con el formato
			#   "correo"=>"nombre usuario"
			# Se pueden poner tantos correos como se deseen
			$mailTo=array(
			   "kohana.gt@gmail.com"=>"Google Mail"
			);

			# establecemos un limite de caracteres de anchura
			$smtp->WordWrap   = 50; // set word wrap

			# NOTA: Los correos es conveniente enviarlos en formato HTML y Texto para que
			# cualquier programa de correo pueda leerlo.

			# Definimos el contenido HTML del correo
			//* Llamar al contenido de la Plantilla HTML de Correo
			$dom = new DOMDocument();
			$root = $dom->load("../inc/plantila_sugerencia.html");
			
			//* Se obtiene la estructura del archivo en una cadena de caracteres
			$string = $dom->saveHTML($dom);
			
			unset($dom); unset($root);
			//* Reemplazar los contenidos correspondientes
			$string = str_replace("<body>\n", "", $string);
			$string = str_replace("</body>\n", "", $string);
			$string = str_replace("%nom%", $nom, $string);
			$string = str_replace("%asunto%", $asunto, $string);
			$string = str_replace("%msg%", $msg, $string);
			$string = str_replace("\"", "'", $string);
			$string = str_replace("%pm%", "cid:pm", $string);
			
			//* Asignamos la cadena formateada para el cuerpo del correo
			$contenidoHTML = $string;

			
			# Definimos el contenido en formato Texto del correo
			$contenidoTexto = "Soporte para PortoMex\n";
			$contenidoTexto.= "Nombre: ".$nom."\n";
			$contenidoTexto.= "Asunto: ".$asunto."\n";
			$contenidoTexto.= "Mensaje: ".$msg;
			

			# Definimos el subject
			$smtp->Subject="Nuevo mensaje de soporte: PortoMex";

			# Adjuntamos el archivo "leameLWP.txt" al correo.
			# Obtenemos la ruta absoluta de donde se ejecuta este script para encontrar el
			# archivo leameLWP.txt para adjuntar. Por ejemplo, si estamos ejecutando nuestro
			# script en: /home/xve/test/sendMail.php, nos interesa obtener unicamente:
			# /home/xve/test para posteriormente adjuntar el archivo leameLWP.txt, quedando
			# /home/xve/test/leameLWP.txt
			#$rutaAbsoluta=substr($_SERVER["SCRIPT_FILENAME"],0,strrpos($_SERVER["SCRIPT_FILENAME"],"/"));
			#$smtp->AddAttachment($rutaAbsoluta."/leameLWP.txt", "LeameLWP.txt");

			# Indicamos el contenido
			$smtp->AltBody=$contenidoTexto; //Text Body
			$smtp->MsgHTML($contenidoHTML); //Text body HTML
			$smtp->AddEmbeddedImage("../images/Portomex.ico", "pm", "Portomex.ico");
			$errcode = 0;
			$item = array();
			
			foreach($mailTo as $mail=>$name)
			{
			    $smtp->ClearAllRecipients();
			    $smtp->AddAddress($mail,$name);

			    if(!$smtp->Send())
			    {
			    	$errcode++;
			    	$item[] = array($mail, $smtp->ErrorInfo);
			    }else{
			    	;
			    }
			}
			if($errcode > 0){
				$code = 403;
				$message = "Error de Envío, Intente nuevamente.";
				$data = array('errcode'=> $errcode, 'item' => $item);
			}else{
				$code = 201;
				$message = "Enviado Con &Eacute;xito";
			}
		}else {
			$code = 400;
			$message = "No Hay Variables";
		}
        
    }
	else{
		$code = 404;
		$message = "Peticion No Reconocida";
		}
}
catch (PDOException $pdoex)
{
	$code = 500; //(500 Internal Server Error)
	$message = "Error de base de datos";
}
catch (Exception $ex)
{
	$code = $ex->getCode(); //Undefined
	$message = "Error";
}

/*Preparar Respuesta*/
$out_put = array('code'=> $code, 'message'=> $message, 'data'=>$data);
/*Encode JSON*/
$json_res = json_encode($out_put);
/*Desplegar*/
echo $json_res;
?>