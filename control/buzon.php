<?php
/* Include Area */
include_once 'core.php';
include_once 'src_mail/mail_defines.php';
/* Envio de la Respuesta en formato JSON */
header("Content-Type: application/json");

$code = 0;
$message = "";
$data = array();

try {
	$bd_conn= ConexionBD::obtenerInstancia()->obtenerBD();
	
	if($_SERVER['REQUEST_METHOD'] == "GET"){
		$code = 302;
		$message = "Conexion Establecida";
	}
	elseif($_SERVER['REQUEST_METHOD'] == "POST")
	{
		if( isset($_POST['nombre']) 
		and isset($_POST['lugar']) 
		and isset($_POST['telefono']) 
		and isset($_POST['correo']) 
		and isset($_POST['mensaje']) ){
			
			$nom = $_POST['nombre']; 
			$nom = trim($nom);
			$lug = $_POST['lugar'];
			$lug = trim($lug);
			$tel = $_POST['telefono'];
			$tel = trim($tel);			
			$mail = $_POST['correo'];
			$mail = trim($mail);
			$msg = $_POST['mensaje'];
			$msg = trim($msg);
			
			/*Guardar en la Base de Datos*/
			/* Preparar Consulta */
			$sql = "INSERT INTO ".
				" mensaje (`sNombre`, `sLugar`, `sCorreo`, `iTelefono`, `sTexto`)".
				" VALUES (?,?,?,?,?)";
			
			$prep_sta = $bd_conn->prepare($sql);
			
			$prep_sta->bindParam(1, $nom, PDO::PARAM_STR);
			$prep_sta->bindParam(2, $lug, PDO::PARAM_STR);
			$prep_sta->bindParam(3, $mail, PDO::PARAM_STR);
			$prep_sta->bindParam(4, $tel, PDO::PARAM_STR);
			$prep_sta->bindParam(5, $msg, pdo::PARAM_STR);
			
			$msg_en_bd = CORREO_PREPARANDO;
			
			$result = $prep_sta->execute();
			if(!$result)
				$msg_en_bd = CORREO_ERROR_ALMACENAR;
			else
				$msg_en_bd = CORREO_ALMACENADO;
			
			$last_id = $bd_conn->lastInsertId();
			/*Enviar por Mail*/
			include("src_mail/phpmailer.php");

			$smtp=new PHPMailer();

			# Indicamos que vamos a utilizar un servidor SMTP
			$smtp->IsSMTP();

			# Definimos el formato del correo con UTF-8
			$smtp->CharSet="UTF-8";

			# autenticación con nuestro servidor smtp
			$smtp->SMTPAuth   = true;
			$smtp->SMTPSecure = SECURITY_PROTOCOL;
			$smtp->Host       = HOST_MAIL;
			$smtp->Username   = NAME_MAIL;
			$smtp->Password   = PASS_MAIL;
			$smtp->Port       = PORT_MAIL;
			# datos de quien realiza el envio
			$smtp->From       = REMITENTE_CORREO; 
			$smtp->FromName   = REMITENTE_NOMBRE; 

			# Indicamos las direcciones donde enviar el mensaje con el formato
			#   "correo"=>"nombre usuario"
			# Se pueden poner tantos correos como se deseen
			$mailTo=array(
			    DESTINATARIO_CORREO => DESTINATARIO_NOMBRE
			);

			# establecemos un limite de caracteres de anchura
			$smtp->WordWrap   = 50; // set word wrap

			# NOTA: Los correos es conveniente enviarlos en formato HTML y Texto para que
			# cualquier programa de correo pueda leerlo.

			# Definimos el contenido HTML del correo
			//* Llamar al contenido de la Plantilla HTML de Correo
			$dom = new DOMDocument();
			$root = $dom->createElement("p");
			try{
				@$root = $dom->load("../inc/PlantillaCorreo.html");
			}catch (Exception $e){
				throw $e;
			}
			//* Se obtiene la estructura del archivo en una cadena de caracteres
			$string = $dom->saveHTML($dom);
			if(trim($string) == "")
				throw new PetitionException("No se pudo procesar el email.", 403);
			
			unset($dom); unset($root);
			//* Reemplazar los contenidos correspondientes
			//*$string = str_replace("<!DOCTYPE html>\n", "", $string);
			$string = str_replace("<html>\n", "", $string);
			$string = str_replace("</html>\n", "", $string);
			$string = str_replace("<body>\n", "", $string);
			$string = str_replace("</body>\n", "", $string);
			$string = str_replace("%PARAM_NAME%", $nom, $string);
			$string = str_replace("%PARAM_PLACE%", $lug, $string);
			$string = str_replace("%PARAM_PHONE%", $tel, $string);
			$string = str_replace("\"", "'", $string);
			$email = "";
			if($mail == "")
				$email = "<i>No proporcionado</i>";
			else
				$email = "<a style='color:gold;' href=\"mailto:".$mail."\">".$mail."</a>";
			
			$string = str_replace("%PARAM_MAIL%", $email, $string);
			$string = str_replace("%PARAM_DATE%", date("r"), $string);
			$string = str_replace("%PARAM_MSG%", $msg , $string);
			
			//* Asignamos la cadena formateada para el cuerpo del correo
			$contenidoHTML = $string;
			
			# Definimos el contenido en formato Texto del correo
			$contenidoTexto = "Recibió un Nuevo Mensaje en su página PortoMex\n";
			$contenidoTexto.= "Nombre: ".$nom."\n";
			$contenidoTexto.= "\nMensaje: ".$msg;
			$contenidoTexto.= "\n\nDatos de Contacto: \n";
			$contenidoTexto.= $nom.", ".$lug.", ".$tel.", ".$mail."\n";

			# Definimos el subject
			$smtp->Subject="Nuevo Mensaje de su Página PortoMex";

			# Adjuntamos el archivo "leameLWP.txt" al correo.
			# Obtenemos la ruta absoluta de donde se ejecuta este script para encontrar el
			# archivo leameLWP.txt para adjuntar. Por ejemplo, si estamos ejecutando nuestro
			# script en: /home/xve/test/sendMail.php, nos interesa obtener unicamente:
			# /home/xve/test para posteriormente adjuntar el archivo leameLWP.txt, quedando
			# /home/xve/test/leameLWP.txt
			#$rutaAbsoluta=substr($_SERVER["SCRIPT_FILENAME"],0,strrpos($_SERVER["SCRIPT_FILENAME"],"/"));
			#$smtp->AddAttachment($rutaAbsoluta."/leameLWP.txt", "LeameLWP.txt");

			# Indicamos el contenido
			$smtp->AltBody=$contenidoTexto; //Text Body
			$smtp->MsgHTML($contenidoHTML); //Text body HTML
			
			$msg_en_mail = CORREO_PREPARANDO;
			
			$errcode = 0;
			$item = array();
			
			foreach($mailTo as $mail=>$name)
			{
			    $smtp->ClearAllRecipients();
			    $smtp->AddAddress($mail,$name);

			    if(!$smtp->Send())
			    {
			    	$errcode++;
			    	$item[] = array($mail, $smtp->ErrorInfo);
			    }else{
			    	;
			    }
			}
			if($errcode > 0){
				if(count($mailTo) == $errcode)
					$msg_en_mail = CORREO_ERROR_ENVIAR;
				
				$data = array('errcode'=> $errcode, 'item' => $item);
			}else{
				$msg_en_mail = CORREO_ENVIADO;
			}
			
			$general_estatus = $msg_en_bd | $msg_en_mail;
			
			if($general_estatus == CORREO_OK_ALMACENADO_OK_ENVIADO){
				$code = 201;
				$message = "Enviado Con &Eacute;xito";
				
				$sql = "UPDATE mensaje SET bCorreo = ? WHERE iId = ?";
				
				$prep_sta = $bd_conn->prepare($sql);
				$val = 1;
				$prep_sta->bindParam(1, $val, PDO::PARAM_INT);
				$prep_sta->bindParam(2, $last_id, PDO::PARAM_INT);
				
				$res = $prep_sta->execute();
				
			}elseif ($general_estatus == CORREO_NO_ALMACENADO_NO_ENVIADO) {
				$code = 403;
				$message = "Error de Envío, Intente nuevamente.";
			}elseif ($general_estatus == CORREO_NO_ALMACENADO_OK_ENVIADO ) {
				$code = 201;
				$message = "Mensaje Enviado. Gracias!";
			}elseif ($general_estatus == CORREO_OK_ALMACENADO_NO_ENVIADO) {
				$code = 201;
				$message = "Mensaje Recibido. Gracias!";
			}else {
				throw new PetitionException("Error Al Ejecutar la Operacion", 400);
			}
			
		}else {
			$code = 400;
			$message = "No Hay Variables";
		}
		
	}else{
		$code = 404;
		$message = "Peticion No Reconocida: ".$_SERVER['REQUEST_METHOD'];
	}
} catch (PDOException $pdoex){
	$code = $pdoex->getCode();
	$message = "Error con la Base de Datos";
} catch (Exception $ex) {
	$code = $ex->getCode();
	$message = $ex->getMessage();
}

$out_put = array('code'=> $code, 'message' => $message, 'data' => $data);

$res = json_encode($out_put);

echo $res;
?>