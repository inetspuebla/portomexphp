<?php
/**
 * Session Manager Class
 * */
class SesionManager{
	#public $id = 0;
	private $status = false;
	
	public function __construct(){
		session_start();
		$this->status = $this->GetStatusSession();
	}
	
	public function __clone(){
		;
	}
	
	public function GetStatusSession() {
		$v = $this->GetValue('start');
		if($v == null || $v != 1)
			return false;
		else
			return true;
	}
	
	public function CloseSession(){
		session_unset();
		session_commit();
	}
	
	public function AbortSession(){
		session_abort();
		session_commit();
	}
	public function SetValue($key, $value){
		$_SESSION[$key] = $value;
	}
	
	public  function GetValue($key) {
		if(isset($_SESSION[$key])){
			return $_SESSION[$key];
		}else {
			return null;
		}
	}
	
	public function UnsetValue($key){
		if(isset($_SESSION[$key])){
			unset($_SESSION[$key]);
			return true;
		}else {
			return false;
		}
	}
}