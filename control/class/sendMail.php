<?php
/**
 * Envio de correo mediante un servidor SMTP
 */

include("phpmailer.php");
$nombre="susanita";
$num="num";
$email="xx@mail.com";
$lugar="lugar";
$correo="nuevo mundo";
header("Content-Type:application/json");
$code = 0;
$message = "";
$data = array();
$errcode = 0;
$item = array();

try{
	$smtp=new PHPMailer();
	
	# Indicamos que vamos a utilizar un servidor SMTP
	$smtp->IsSMTP();
	
	# Definimos el formato del correo con UTF-8
	$smtp->CharSet="UTF-8";
	
	# autenticación contra nuestro servidor smtp
	/*
	$smtp->SMTPAuth   = true;						// enable SMTP authentication
	$smtp->Host       = "local";			// sets MAIL as the SMTP server
	$smtp->Username   = "nombre@miservidor.com";	// MAIL username
	$smtp->Password   = "passwordDelCorreo";			// MAIL password
	*/
	$smtp->SMTPAuth   = true;
	$smtp->SMTPSecure = "tls";
	#$smtp->Host       = "smtp.live.com"; 
	//Asi esta configurado antes, pero encontre en las opciones de outlook que esta era el servidor SMTP para outlook
	$smtp->Host       = "smtp-mail.outlook.com";
	$smtp->Username   = "test_inets@outlook.com";
	$smtp->Password   = "Mail@1234";
	$smtp->Port       = 587;
	# datos de quien realiza el envio
	$smtp->From       = "test_inets@outlook.com"; // from mail
	$smtp->FromName   = "Test Inets Mail"; // from mail name
	
	# Indicamos las direcciones donde enviar el mensaje con el formato
	#   "correo"=>"nombre usuario"
	# Se pueden poner tantos correos como se deseen
	$mailTo=array(
	    
	    "test_inets@outlook.com"=>"Test Inets Mail"
	);
	
	# establecemos un limite de caracteres de anchura
	$smtp->WordWrap   = 50; // set word wrap
	
	# NOTA: Los correos es conveniente enviarlos en formato HTML y Texto para que
	# cualquier programa de correo pueda leerlo.
	
	# Definimos el contenido HTML del correo
	$contenidoHTML="<head>";
	$contenidoHTML.="<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">";
	$contenidoHTML.="</head><body>";
	$contenidoHTML.=" Mensaje de <b> '$nombre' </b> ";
	$contenidoHTML.="<br> Tel&eacute;fono: <b> '$num' </b>";
	$contenidoHTML.="<br> E-mail: <b> '$email' </b>";
	$contenidoHTML.="<br> Lugar: <b> '$lugar' </b>";
	$contenidoHTML.="<br> '$mensaje' </body>";
	# Definimos el contenido en formato Texto del correo
	$contenidoTexto="Nuevo Mensaje de:";
	$contenidoTexto.=" Mensaje";
	
	# Definimos el subject
	$smtp->Subject="Nuevo Mensaje de la pagina";
	
	# Adjuntamos el archivo "leameLWP.txt" al correo.
	# Obtenemos la ruta absoluta de donde se ejecuta este script para encontrar el
	# archivo leameLWP.txt para adjuntar. Por ejemplo, si estamos ejecutando nuestro
	# script en: /home/xve/test/sendMail.php, nos interesa obtener unicamente:
	# /home/xve/test para posteriormente adjuntar el archivo leameLWP.txt, quedando
	# /home/xve/test/leameLWP.txt
	#$rutaAbsoluta=substr($_SERVER["SCRIPT_FILENAME"],0,strrpos($_SERVER["SCRIPT_FILENAME"],"/"));
	#$smtp->AddAttachment($rutaAbsoluta."/leameLWP.txt", "LeameLWP.txt");
	
	# Indicamos el contenido
	$smtp->AltBody=$contenidoTexto; //Text Body
	$smtp->MsgHTML($contenidoHTML); //Text body HTML
	
	foreach($mailTo as $mail=>$name)
	{
	    $smtp->ClearAllRecipients();
	    $smtp->AddAddress($mail,$name);
	
	    if(!$smtp->Send())
	    {
	        $errcode++;
	        $item[] = array($mail, $smtp->ErrorInfo);
	    }else{
	    	;
	    }
	}
	
	$code = 200;
	$message = "Enviado Con &Eacute;xito";
	$data = array('errcode'=> $errcode, 'item' => $item);
	
} catch (phpmailerException $mailex){
	$code = 400;
	$message = $mailex->getMessage();
}catch (Exception $ex){
	$code = 500;
	$message = $mailex->getMessage();
}
$out_put = array('code'=> $code, 'message' => $message, 'data' => $data);
$json = json_encode($out_put);
echo $json;
?>
