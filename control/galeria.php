<?php
include_once 'core.php';
/* Formato de Respuesta: JSON */
header("Content-Type:application/json");
$code = 0;
$message = "";
$data = array();

try {
	$bd_conn= ConexionBD::obtenerInstancia()->obtenerBD();
	
	if($_SERVER['REQUEST_METHOD'] == "GET"){
		
		if(isset($_GET['keyall']) and isset($_GET['we_are'])){
			$sql = "";
			if($_GET['we_are'] == "NOPROMO"){
				$sql = "SELECT imagen.sNombre as imgname, imagen.sUrlNombre as url, imagen.sDescripcion as textdesc ".
				" FROM imagen ".
				" INNER JOIN (SELECT tipoimagen.iId as idimg FROM tipoimagen WHERE sDesc = 'Promocion') As R ".
				" ON imagen.iId_TipoImagen <> R.idimg";
			 } else if($_GET['we_are'] == "PROMO") {
				 $sql = "SELECT imagen.sNombre as imgname, imagen.sUrlNombre as url, imagen.sDescripcion as textdesc ".
				" FROM imagen ".
				" INNER JOIN (SELECT tipoimagen.iId as idimg FROM tipoimagen WHERE sDesc = 'Promocion') As R ".
				" ON imagen.iId_TipoImagen = R.idimg";
			 } else {
				 $sql = "SELECT imagen.sNombre as imgname, imagen.sUrlNombre as url, imagen.sDescripcion as textdesc ".
				" FROM imagen ";
			 }

			$result = $bd_conn->query($sql);
			
			if(!$result){
				throw new PetitionException("Error de Consulta", 404);
			}else {
				
				$arr = $result->fetchAll(PDO::FETCH_ASSOC);
				if(count($arr) == 0) {
					throw new PetitionException("No hay Im&aacute;genes", 400);
				}

				foreach ($arr as $key => $value){
					$arr[$key]['id'] = "image_".$key;
				}

				$code = 200;
				$message = "Resultados";
				$data = $arr;
			}
		}
		else if(isset($_GET['allimages']) and isset($_GET['whosare'])) {
			$type = $_GET['whosare'];  //'Otro';

			$sql = 'SELECT imagen.sNombre as imgname, imagen.sUrlNombre as url, imagen.sDescripcion as textdesc '.
				' FROM imagen '.
				' INNER JOIN (SELECT tipoimagen.iId as idimg FROM tipoimagen WHERE sDesc = ? ) As R '.
				' ON imagen.iId_TipoImagen = R.idimg ';

			$prep = $bd_conn->prepare($sql);
			$prep->bindParam(1, $type);

			$res = $prep->execute();

			if($res){
				$arr = $prep->fetchAll(PDO::FETCH_ASSOC);

				if(count($arr) == 0){
					throw new PetitionException('Sin Resultados para el cat&aacute;logo '.$type, 404);
				}else {
					foreach ($arr as $key => $value){
						$arr[$key]['id'] = $type . '_image_'.$key;
					}
					$code = 200;
					$message = 'Im&aacute;genes encontradas para la categor&iacute;a ' . $type;
					$data = $arr;
				}
			}else {
				throw new PetitionException("Error en consulta", 400);
			}
		}
		else
		{
			$code = 1;
			$message = "What's up? :)";
		}
	}
	elseif($_SERVER['REQUEST_METHOD'] == "POST")
	{
		$code = 1;
		$message = "Wazaaaaaaa! :)";
	}
} catch (PDOException $pdoex){
	$code = $pdoex->getCode();
	$message = "Error con la Base de Datos";
} catch (Exception $ex) {
	$code = $ex->getCode();
	$message = $ex->getMessage();
}

/* Preparar Salida */
$out_put = array('code'=> $code, 'message' => $message, 'data' => $data);
/* Codificar salida a JSON */
$res = json_encode($out_put);
/* Enviar Resultado de la Peticion*/
echo $res;
?>