<?php

if($_SERVER['REQUEST_URI'] == $_SERVER['SCRIPT_NAME']){
	header("Location: ./.");
	exit();
}
include_once 'core.php';

$session = new SesionManager();

if(intval(count($_SESSION)) == 0 )
	header("Location: ./.");
else 
	$session->CloseSession();

unset($session);
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8" >
<title>Cerrando Sesi&oacute;n</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-10 col-xs-offset-1">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">Cerrando Sessi&oacute;n</h3>
					</div>
					<div class="panel-body">
						<label>Si la p&aacute;gina no cambia en 5 segundos,
							Actualize o presione <a href="./.">Aqu&iacute;</a>
						</label>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		window.location = "./.";
	</script>
</body>
</html>