<?php 
include_once 'core.php';
/* Envio de la Respuesta en formato JSON */
header("Content-Type:application/json");
$code = 0;
$message = "";
$data = array();

try{
	$bd_conn= ConexionBD::obtenerInstancia()->obtenerBD();
	
	if($_SERVER['REQUEST_METHOD'] == "GET")
	{
		$sesion = new SesionManager();
		if($sesion->GetStatusSession())
		{
			if(isset($_GET['getallimages'])){
				$sql=" SELECT iId as id, sUrlNombre as urlnombre, sNombre as nombre, sDescripcion as descripcion, iId_TipoImagen as tipo FROM imagen ";
				$prep = $bd_conn->prepare($sql) ;
				//$prep->bindParam(1, $type, PDO::PARAM_STR);

				$result = $prep->execute;

				if(!$result) {
					throw new PetitionException("Sin resultados", 404) ;
				}
				else
				{
					$arr = $prep->fetchAll(PDO::FETCH_ASSOC);
			
					$code = 200;
					$message = "Resultados imágenes" ;
					$data = $arr;
				}
			}else if(isset($_GET['keyall']) and isset($_GET['typecat']) ){
				$type = $_GET['typecat'];
				$sql=
				' SELECT iId as id, sUrlNombre as urlnombre, sNombre as nombre, sDescripcion as descripcion, iId_TipoImagen as tipo '.
				' FROM imagen '.
				' INNER JOIN (SELECT tipoimagen.iId as idimg FROM tipoimagen WHERE sDesc = ? ) As R '.
				' ON imagen.iId_TipoImagen = R.idimg ';	

				$prep = $bd_conn->prepare($sql) ;
				$prep->bindParam(1, $type, PDO::PARAM_STR);

				$result = $prep->execute();

				if(!$result) {
					throw new PetitionException("Sin resultados", 404) ;
				}
				else
				{
					$arr = $prep->fetchAll(PDO::FETCH_ASSOC);
			
					$code = 200;
					$message = "Resultados imágenes" ;
					$data = $arr;
				}
			} elseif(isset($_GET['keytypes'])){
				$sql = "SELECT iId as keyid, sDesc as valuetp from tipoimagen";

				$result = $bd_conn->query($sql) ;
					
				if(!$result) {
					throw new PetitionException("Sin resultados", 404) ;
				}
				else
				{
					$arr = $result->fetchAll(PDO::FETCH_ASSOC);
			
					$code = 200;
					$message = "Tipo Imagen" ;
					$data = $arr;
				}

			}else {
				throw new PetitionException("Peticion Mal Formada", 400);
				
			}
		}
		else
		{
			$sesion->AbortSession();
			$code = 302;
			$message = "Welcome :) ";
		}
	}
	elseif ($_SERVER['REQUEST_METHOD'] == "POST")
	{	
		if(isset($_POST['delet']) and isset($_POST['key'])) {
				
			$sql = "DELETE FROM imagen WHERE iId = ".$_POST['key'];
			
			$result = $bd_conn->query($sql);
			
			if($result->rowCount() > 0){
				/*Delete File from Server*/
				
				$code = 201;
				$message = "Eliminado";
			}else {
				throw new PetitionException("Consulta Inv&aacute;lida: ", 404);
			}
		} else if ( isset($_POST['edit']) and isset($_POST['eid']) and isset($_POST['ename']) and isset($_POST['etype']) and isset($_POST['etext'])){

			$name = $_POST['ename'];
			$type = $_POST['etype'];
			$desc = $_POST['etext'];
			$eid  = $_POST['eid'];

			$sql = "UPDATE imagen SET sNombre=?, sDescripcion=?, iId_TipoImagen=? WHERE iId = ?";

			$prep = $bd_conn->prepare($sql);

			$prep->bindParam(1, $name);
			$prep->bindParam(2, $desc);
			$prep->bindParam(3, $type);
			$prep->bindParam(4, $eid);

			$result = $prep->execute();
				
			if(!$result){
				throw new PetitionException("Consulta Inv&aacute;lida: Ex", 404);
			}else{
				$code = 201;
				$message ="Editado";
			}
			
		} else {
			$code = 1;
			$message = "Hello :p";
		}
	}
	else 
	{
		throw new RequestException("Peticion No Reconocida: ".$_SERVER['REQUEST_METHOD'], 400);	
	}
}
catch (RequestException $reqex)
{
	$code = $reqex->getCode();
	$message = $reqex->getCode();
}
catch (PetitionException $pttex)
{
	$code = $pttex->getCode();
	$message = $pttex->getMessage();
}
catch (PDOException $pdoex)
{
	$code = $pdoex->getCode();
	$message = "Error Con la Base de Datos";
}
catch (Exception $ex)
{
	$code = $ex->getCode(); //Undefined
	$message = $ex->getMessage(); 
}
/*Preparar Respuesta*/
$out_put = array('code'=> $code, 'message'=> $message, 'data'=>$data);
/*Encode JSON*/
$json_res = json_encode($out_put);
/*Desplegar*/
echo $json_res;
?>