<?php
include_once 'class/Session.class.php';
$sesion = new SesionManager();
if(!$sesion->GetStatusSession()){ header("Location: ../acceso.php"); exit(); }

if($sesion->GetValue('username') == null) {$sesion->CloseSession(); unset($sesion); header("Location: ../acceso.php"); exit();}
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Panel</title>
	<!-- Local -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<!-- Optional theme -->
	<link rel="stylesheet" href="../css/bootstrap-theme.min.css">
	<!-- Remote-->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<link rel="stylesheet" type="text/css" href="../css/sidebar.css" media="all">
	<link rel="stylesheet" type="text/css" href="../css/Panel.css">
	<link rel="stylesheet" type="text/css" href="../css/scrolling-nav.css" media="all">
	<link rel="stylesheet" type="text/css" href="../css/jquery-te-1.4.0.css">
</head>

<body id="page_top">
<div id="wrapper">
<!-- Menu Lateral -->
		<div id="sidebar-wrapper"> <!--sidebar-nav-->
			<ul class="nav nav-pills nav-stacked">
				<li class="sidebar-brand" align="center"><a href="."> <span
						class="glyphicon glyphicon-home"></span> Panel de Control v0.1
				</a></li>
				<li><br><br></li>
				<li><a href="../." target="_blank"><span></span>Mi Sitio</a></li>
				<li><br></li>
                <li><a><span></span>Operaciones</a></li>
				<li><a href="#area_inicio"> <span class="glyphicon glyphicon-pencil"
						aria-hidden="true"></span> Perfil
				</a></li>
				<li><a href="#area_catalogo"> <span class="glyphicon glyphicon-floppy-open" 
						aria-hidden="true"></span> Cat&aacute;logo
				</a></li>
				<li><a href="#area_mensajes"> <span class="glyphicon glyphicon-inbox"
						aria-hidden="true"></span> Mensajes
				</a></li>
				<li><a href="#area_admon"> <span class="glyphicon glyphicon-book"
						aria-hidden="true"></span> Administraci&oacute;n
				</a></li>
				<li><a href="#area_servicios"> <span class="glyphicon glyphicon-wrench"
						aria-hidden="treu"></span> Servicios
				</a></li>
				<li onclick="window.location = './goodbye.php?close='"><a href="./goodbye.php?close=" > <span class="glyphicon glyphicon-off" 
						aria-hidden="true"></span> Cerrar sesi&oacute;n
				</a></li>
				<li><a class="disabled"><span></span>Acciones</a></li>
				<li><a id="menu-close" href="#"> <span class="glyphicon glyphicon-menu-hamburger" 
						aria-hidden="true"></span> Ocultar Men&uacute;
				</a></li>
			</ul>
		</div>
		<!-- Boton Menu Desplegable-->
		<div id="menu_up">
			<a id="menu-toggle" class="btn btn-lg"> <span
				class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
				Men&uacute; Lateral
			</a>
			<label class="btn btn-lg"> Nombre de Usuario: &nbsp; <span
				class="glyphicon glyphicon-user" aria-hidden="true"></span>&nbsp;<b id="user_name"></b>
			</label>
		</div>
		<!-- Contenido -->
		<div id="page-content-wrapper">
		
			<div id="area_inicio" class="areaitem">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-8 col-xs-offset-2 col-md-8 col-md-offset-1 area_item_title">
							<h2>Datos de la Empresa</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-11 col-md-offset-0">
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="btn-group btn-group-justified" role="group">
										<div class="btn-group" role="group">
											<label class="btn"><b>Perfil</b></label>
										</div>
										<div class="btn-group" role="group">
											<button id="edit-info" class="btn btn-primary"
												onclick="EditarPerfil()">Editar</button>
										</div>
										<div class="btn-group" role="group">
											<button id="save-info" disabled="disabled"
												class="btn btn-default" onclick="SalvarPerfil()">Guardar</button>
										</div>
									</div>
								</div>
								<div class="panel-body">
									<div class="container-fluid">
										<div class="row">
											<div id="estatus_perfil" class="col-xs-12"></div>
											<div class="col-xs-2">
												<label>Nombre:</label>
											</div>
											<div class="col-xs-10">
												<input id="empresa_nombre" type="text" value=""
													placeholder="Nombre de la empresa"
													disabled="disabled" class="fInfo form-control">
												<br>
											</div>

											<div class="col-xs-2">
												<label>Calle:</label>
											</div>
											<div class="col-xs-10">
												<input id="empresa_calle" type="text" value=""
													placeholder="Direcci&oacute;n de la empresa" disabled="disabled"
													class="fInfo form-control">
												<br>
											</div>

											<div class="col-xs-2">
												<label>Num. Ext:</label>
											</div>
											<div class="col-xs-4">
												<input id="empresa_num_ext" type="text" value=""
													placeholder="N&uacute;mero exterior"
													disabled="disabled" class="fInfo form-control">
												<br>
											</div>
											<div class="col-xs-2">
												<label>Num. Int:</label>
											</div>
											<div class="col-xs-4">
												<input id="empresa_num_int" type="text" value=""
													placeholder="N&uacute;mero interior"
													disabled="disabled" class="fInfo form-control">
												<br>
											</div>
											<div class="col-xs-2">
												<label>Colonia:</label>
											</div>
											<div class="col-xs-4">
												<input id="empresa_colonia" type="text" value=""
													placeholder="Colonia" disabled="disabled"
													class="fInfo form-control">
												<br>
											</div>
											<div class="col-xs-2">
												<label>Municipio:</label>
											</div>
											<div class="col-xs-4">
												<input id="empresa_municipio" type="text" value=""
													placeholder="Municipio" disabled="disabled"
													class="fInfo form-control">
												<br>
											</div>
											<div class="col-xs-2">
												<label>Estado:</label>
											</div>
											<div class="col-xs-10">
												<input id="empresa_estado" type="text" value=""
													placeholder="Estado" disabled="disabled"
													class="fInfo form-control"><br>
											</div>
											<div class="col-xs-2">
												<label>T&eacute;l. 1:</label>
											</div>
											<div class="col-xs-4">
												<input id="empresa_tel_1" type="number" value=""
													placeholder="Telefono 1" disabled="disabled"
													class="fInfo form-control"><br>
											</div>
											<div class="col-xs-2">
												<label>T&eacute;l. 2:</label>
											</div>
											<div class="col-xs-4">
												<input id="empresa_tel_2" type="number" value=""
													placeholder="Telefono 2" disabled="disabled"
													class="fInfo form-control"><br>
											</div>
											<div class="col-xs-2">
												<label>Email:</label>
											</div>
											<div class="col-xs-10">
												<input id="empresa_email" type="email" value=""
													placeholder="mail@mail.com" disabled="disabled"
													class="fInfo form-control"><br>
											</div>
											<div class="col-xs-12">
												<label>Descripci&oacute;n</label>
												<div id="empresa_desc_html" class="panel-body" style="text-align: justify;"></div>
												<div id="entry_empresa_desc" style="display:none;">
													<textarea id="empresa_desc" rows="10"
														placeholder="Descripci&oacute;n" disabled="disabled"
														class="fInfo form-control" style="display: none;"></textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-11 col-md-offset-0">
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="btn-group btn-group-justified" role="group">
										<div class="btn-group" role="group">
											<label class="btn"><b>Video Principal</b></label>
										</div>
										<div id="group_edt_video" class="btn-group" role="group">
											<button id="btnEditVideo" class="btn btn-primary btn-block"
												onclick="Editar_Video()">Editar</button>
										</div>
										<div id="group_sav_video" class="btn-group" role="group" style="display:none;">
											<button id="btnSaveVideo" class="btn btn-default btn-block"
												onclick="Salvar_Video()" disabled="disabled">Guardar</button>
										</div>
									</div>
								</div>
								<div class="panel-body">
									<div class="container-fluid">
										<div class="row">
											<div class="col-xs-12 col-md-6">
												<div class="form-group">
													<label>Direcci&oacute;n electr&oacute;nica: </label> <span
														class="label label-primary">Nota: Si el video no puede
														visualizar correctamente, verifique la direccion brindada</span>
													<div class="input-group">
														<input type="text" disabled="disabled" id="empresa_video"
															class="form-control"
															placeholder="Ej.: http://www.youtube.com/watch/v?cp9B6KjjDCQ" />
														<span class="input-group-btn">
															<button id="btnComprobeVideo" class="btn btn-primary"
																onclick="Comprobar_Video()">Comprobar</button>
														</span>
													</div>
													<button id="btn_empresa_video" class="btn btn-default"
														onclick="Salvar_Video()" disabled="disabled"
														style="display: none">Enviar</button>
												</div>
											</div>
											<div id="yt_h" class="col-xs-12 col-md-6">
												<div id="yt_" class="embed-responsive embed-responsive-16by9"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div id="area_catalogo" class="areaitem">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-10 col-xs-offset-1">
							<div class="jumbotron">
								<h1>Nuevo Apartado</h1>
								<p>Con el fin de mejorar el manejo se im&aacute;genes de tu p&aacute;gina 
									hemos creado un nuevo apartado para administrar los cat&aacute;logos. 
									<br> Por favor acceda <a href="PanelImagenes.php" class="btn btn-primary btn-lg">Aqu&iacute;</a>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="container-fluid">
					<div class="row">
						<div id="st_cat" class="col-xs-11"></div>
						<div id="long_catg" class="col-xs-12"></div>
						<div id="catg" class="col-xs-12">
							<!-- Images Here -->
						</div>
					</div>
				</div>
			</div>

			<div id="area_mensajes"  class="areaitem">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-8 col-xs-offset-2 col-md-8 col-md-offset-1 area_item_title">
							<h2>Mensajes</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="btn-group btn-group-justified" role="group">
										<div class="btn-group" role="group">
											<label class="btn"><b>Acciones</b></label>
										</div>
										<!--<div class="btn-group" role="group">
											<button class="btn btn-info">
												<span class="glyphicon glyphicon-refresh"></span>&nbsp;Recargar Buz&oacute;n
											</button>
										</div>-->
										<div class="btn-group" role="group">
											<button id="btn_edit_mail" class="btn btn-success" onclick="EditarCorreoEnvio();">
												<span class="glyphicon glyphicon-refresh"></span>&nbsp;Editar Correo
											</button>
										</div>
									</div>
								</div>
								<div id="pnl_body_edit_mail" class="panel-body">
									<div class="input-group">
										<input id="input_email_to_send" type="text" placeholder="mail@mail.com" class="form-control" disabled="disabled"/>
										<span class="input-group-btn">
											<button id="btn_chg_mail_send" class="btn btn-default" type="button" disabled="disabled" onclick="GuardarCorreoEnvio();">Guardar</button>
										</span>
									</div>
								</div>
								<div class="panel-footer">
									<label>Direcci&oacute;n de recepci&oacute;n de correo:</label>&nbsp;&nbsp;<i id="show_mail_send">mail@mail.com</i>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div id="num_msg" class="col-xs-12">
						</div>
						<!-- Barra de de estatus --> 
					</div>
				</div>
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-10 col-xs-offset-1"></div>
						<div class="col-xs-12">
							<div class="panel panel-primary buzon">
								<div class="panel-heading">
									<div class="btn-group btn-group-justified" role="group">
										<div class="btn-group" role="group">
											<label class="btn"><span class="glyphicon glyphicon-envelope"></span>&nbsp;Buz&oacute;n</label>
										</div>
										<div class="btn-group" role="group">
											<button id="reload_msgs" class="btn btn-success">
												<span class="glyphicon glyphicon-refresh"></span>&nbsp;Recargar Buz&oacute;n
											</button>
										</div>
									</div>
								</div>
								<ul id="buzon" class="list-group">
								<!--Dinamic Content-->
								</ul>
							</div>
						</div>
					</div>
				</div>

				<!--<button type="button" class="btn" data-toggle="modal"
					data-target="#show_message_modal">Ver Modal Mensaje</button>-->
				<div id="show_message_modal" class="modal fade" tabindex="-1"
					role="dialog" aria-labelledby="modalmsg">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-body">
								<div class="panel panel-primary">
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-4">
												<h4><span class="glyphicon glyphicon-inbox"></span> Detalles</h4>
											</div>
											<div class="col-xs-4">
												<label>
													<span class="glyphicon glyphicon-user"></span>  Nombre:&nbsp;&nbsp;
												</label>
												<div id="mname">%Nombre%</div>
											</div>
										</div>
									</div>
									<ul class="list-group">
										<li class="list-group-item">
											<div class="row">
												<div class="col-xs-6">
													<label> Lugar:</label>
													<div id="mlug">%Lugar%</div>
												</div>
												<div class="col-xs-6">
													<label> Telefono:</label>
													<div id="mtel">%0000000000%</div>
												</div>
											</div>
										</li>
										<li class="list-group-item">
											<div class="row">
												<div class="col-xs-6">
													<label> Correo:</label>
													<div id="mmail">%mail@mail.com%</div>
												</div>
												<div class="col-xs-6">
													<label> Fecha:</label>
													<div id="mdate">%Dia Mes Año, Hora, Min%</div>
												</div>
											</div>
										</li>
										<li class="list-group-item">
											<div class="row">
												<div class="col-xs-12 col-xs-offset-0 col-md-12">
													<label> Mensaje</label>
													<div id="mtext" style="padding-left: 10px; padding-right: 10px;">%SuperMensajeLargoQueDeberiaSerEscritoAqui%</div>
												</div>
											</div>
										</li>
									</ul>
									<div class="panel-footer">
										#Fin del Mensaje.
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button id="msgdelete" class="btn btn-primary">Eliminar</button>
								<button id="msgclose" class="btn btn-primary"
									data-dismiss="modal">Regresar</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div id="area_admon"  class="areaitem">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-8 col-xs-offset-2 col-md-8 col-md-offset-1 area_item_title">
							<h2>Administraci&oacute;n</h2>
							<hr/>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-11 col-xs-offset-1 col-sm-6 col-sm-offset-0 col-md-5 col-md-offset-0 ">
							<div class="panel panel-default">
								<div class="panel-heading" style="text-align: center;">
									<label>Cambiar nombre de usuario</label>
									<button id="change_user" class="btn btn-block btn-primary" onclick="CambiarUsuario()">Editar</button>
								</div>
								<div class="panel-body">
								<div id="st_chg_user"></div>
									<div class="form-group">
										<label>Usuario anterior</label>
										<input id="usuario" type="text" disabled="disabled"
											placeholder="@usuario" class="form-control">
										<label>Nuevo Usuario</label>
										<input id="n_usuario" type="text" disabled="disabled" 
											placeholder="@usuario" class="form-control">
										<br/>
										<button id="save_user" class="btn btn-block btn-success" disabled="disabled" onclick="SalvarUsuario()" style="display: none;">Guardar</button>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-11 col-xs-offset-1 col-sm-6 col-sm-offset-0 col-md-5 col-md-offset-1  ">
							<div class="panel panel-default">
								<div class="panel-heading" style="text-align: center;">
									<label>Cambiar Contrase&ntilde;a</label>
									<button id="change_password" class="btn btn-block btn-primary" onclick="CambiarContrasenia()">Editar</button>
								</div>
								<div class="panel-body">
									<div class="form-group">
									<div id="st_chg_psw"></div>
										<label>Contrase&ntilde;a Anterior</label>
										<input id="contrasenia" type="password" class="form-control" placeholder="*****" disabled="disabled"/>
										<label>Nueva Contrase&ntilde;a</label>
										<input id="n_contrasenia" type="password" class="form-control" placeholder="*****" disabled="disabled"/>
										<label>Repetir Contrase&ntilde;a</label>
										<input id="rn_contrasenia" type="password" class="form-control" placeholder="*****" disabled="disabled"/>
										<br/>
										<button id="save_password" class="btn btn-block btn-success" disabled="disabled" onclick="SalvarContraseña()" style="display: none;">Guardar</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		
			<div id="area_servicios" class="areaitem">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-8 col-xs-offset-2 col-md-8 col-md-offset-1 area_item_title">
							<h2>Servicios Inets</h2>
						</div>
						<div class="col-xs-10 col-xs-offset-1">
							<div class="panel panel-success">
								<div class="panel-heading">
									<label>¿Dudas, Sugerencias, Comentarios?</label>
									<br/>
									¡Escríbemos! Estamos para apoyarte.
								</div>
								<div class="panel-body">
									<div id="estatussugerencia">
									</div>
									<div class="form-group">
										<label>Nombre</label>
										<input id="text1" type="text" class="form-control" placeholder="Sr., Sra., Srita.">
										<label>Asunto</label>
										<input id="text2" type="text" class="form-control" placeholder="Asunto">
										<label>Descripci&oacute;n</label>
										<textarea id="text3" rows="10" cols="auto" class="form-control" placeholder="Escribe tu descripci&oacute;n"></textarea>
										<button id="sendsugerencia" class="btn btn-success" onclick="enviarsugerencia()">Enviar</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	<!-- Scripts Area-->
	<!-- Remote -->
    <!-- JQuery -->
	<script src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<!-- Local-->
	<script type="text/javascript" src="../js/jquery-1.12.2.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<!-- Requires -->
	<script type="text/javascript" src="../js/jquery-te-1.4.0.min.js"></script>
	<script type="text/javascript" src="../js/util/Alert.js"></script>
	<script type="text/javascript" src="../js/util/DOM_Element.js"></script>
	<script type="text/javascript" src="../js/util/Imagen_Thumbnail.js"></script>
	<script type="text/javascript" src="../js/util/Thumbnail_Bootstrap.js"></script>
	<script type="text/javascript" src="../js/neg/Panel_Admon.js"></script>
	<script type="text/javascript" src="../js/neg/CatalogoImagen.js"></script>
	<script type="text/javascript" src="../js/neg/UploadImage.js"></script>
	<script type="text/javascript" src="../js/neg/CtrlMensaje.js"></script>
	<script type="text/javascript" src="../js/neg/Sugerencia.js"></script>

	<!-- Menu Toggle Script -->
	<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    $("#menu-close").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
	<script type="text/javascript">
	//Plugin de Editor de Texto HTML
	$(document).ready(function() {
		$("#empresa_desc").jqte();
		$("#descripcion_imagen").jqte();
		$("#etext").jqte();
		$("#text3").jqte();
	});
	</script>
</body>
</html>