<?php 
include_once 'core.php';
/* Envio de la Respuesta en formato JSON */
header("Content-Type:application/json");
$code = 0;
$message = "";
$data = array();
try{
	$bd_conn= ConexionBD::obtenerInstancia()->obtenerBD();
	
	if($_SERVER['REQUEST_METHOD'] == "GET") 			/*Consultar Checkeo de Conexion*/
	{
		$code = 202; //(202 Accepted) 
		$message = "Hello!";
	}
	elseif ($_SERVER['REQUEST_METHOD'] == "POST") 		/*Consulta */
	{	
		if(isset($_POST['name']) and isset($_POST['pw'])){
			$uname = $_POST['name'];
			$upass = $_POST['pw'];
			
			#Autorizacion
			$sql = "SELECT id as uid, username as uname, userpass as upass, rol as urol FROM usuario WHERE username = ? and userpass = ?";
	
			$prep = $bd_conn->prepare($sql);
			
			$prep->bindParam(1, $uname, PDO::PARAM_STR);
			$prep->bindParam(2, $upass, PDO::PARAM_STR);
			
			$res_ex = $prep->execute();
			
			if($res_ex){
				
				if($prep->rowCount() == 1){	
					$arr = $prep->fetchAll(PDO::FETCH_ASSOC);
					$sesion = new SesionManager();
					$sesion->SetValue('start', true);
					$sesion->SetValue('id', session_id());
					$sesion->SetValue('userid', $arr[0]['uid']);
					$sesion->SetValue('username', $arr[0]['uname']);
					
					$code = 200;
					$message = "Ok";
				}else{
					throw new PetitionException("Credenciales Erroneas", 401);//Unauthorized
				}
			}
			else {
				throw new PetitionException("Error en la consulta", 400);
			}
		}elseif( isset($_POST['uedit']) and isset($_POST['anick']) and isset($_POST['unick']) ){
			$sesion = new SesionManager();
			
			if(!$sesion->GetStatusSession()){
				$sesion->AbortSession();
				throw new PetitionException("Requiere Accesar Primero", 400);
			}
			$antiguo_usr = $_POST['anick'];
			$nuevo_usr =$_POST['unick'];

			$sql="SELECT username FROM usuario WHERE id= ?";
			$prep = $bd_conn->prepare($sql);
			$prep->bindParam(1, $_SESSION['userid'], PDO::PARAM_STR);
			$res_ex = $prep->execute();
			$arr = $prep->fetchAll(PDO::FETCH_ASSOC);

			if($arr[0]['username']==$antiguo_usr ){
				$sql="UPDATE usuario SET username = ? WHERE id = ?";
				$prep = $bd_conn->prepare($sql);
				$prep->bindParam(1, $nuevo_usr, PDO::PARAM_STR);
				$prep->bindParam(2, $_SESSION['userid'] , PDO::PARAM_STR);

				$res_ex = $prep->execute();

				if($res_ex)
				{
						$code = 200;
					$message = "Ok";
				}
				else {
					$code = 400;
					$message = "Error";
				}
			}
			else{
				throw new PetitionException("Usuario incorrecto ", 400);
			}
		} elseif(isset($_POST['uedit']) and isset($_POST['apsw']) and isset($_POST['upsw'])){
			$sesion = new SesionManager();
				
			if(!$sesion->GetStatusSession()){
				$sesion->AbortSession();
				throw new PetitionException("Requiere Accesar Primero", 400);
			}
				$antiguo_psw= $_POST['apsw'];
				$nuevo_psw =$_POST['upsw'];
				

				$sql="SELECT userpass FROM usuario WHERE id= ?";
				$prep = $bd_conn->prepare($sql);
				$prep->bindParam(1, $_SESSION['userid'], PDO::PARAM_STR);
				$res_ex = $prep->execute();
				$arr = $prep->fetchAll(PDO::FETCH_ASSOC);


				if($arr[0]['userpass']==$antiguo_psw )
					{
						$sql="UPDATE usuario SET userpass = ? WHERE id = ?";
						$prep = $bd_conn->prepare($sql);
						$prep->bindParam(1, $nuevo_psw, PDO::PARAM_STR);
						$prep->bindParam(2, $_SESSION['userid'] , PDO::PARAM_STR);

						$res_ex = $prep->execute();

						if($res_ex)
						{
								$code = 200;
							$message = "Ok";
						}
						else {
							$code = 400;
							$message = "Error";
						}
					}
				else{
						throw new PetitionException("Contraseña incorrecta ", 400);
				}
		}	
		else
		{
			throw new PetitionException("Campos Inválidos", 400);
		}
	}
	else {
		throw new RequestException("Peticion No Reconocida: "+$_SERVER['REQUEST_METHOD'], 400); //(400 Bad Request)
	}
}
catch (RequestException $rqex){
	$code = $rqex->getCode();
	$message = $rqex->getMessage();
}
catch (PetitionException $ptex){
	$code = $ptex->getCode();
	$message = $ptex->getMessage();
}
catch (PDOException $pdoex)
{
	$code = $pdoex->getCode();
	$message = "Error con la Base de Datos";
}
catch (Exception $ex)
{
	$code = $ex->getCode(); //Undefined
	$message = "Error Desconocido"; 
}
/*Preparar Respuesta*/
$out_put = array('code'=>$code,'message'=>$message, 'data' => $data);
/*Encode JSON*/
$json_res = json_encode($out_put);
/*Desplegar*/
echo $json_res;
?>