<?php 
include_once 'core.php';
/* Envio de la Respuesta en formato JSON */
header("Content-Type:application/json");
$code = 0;
$message = "";
$data = array();

try{
	$bd_conn= ConexionBD::obtenerInstancia()->obtenerBD();
	
	if($_SERVER['REQUEST_METHOD'] == "GET")
	{
		$sesion = new SesionManager();
		if($sesion->GetStatusSession())
		{
				$sql=" SELECT iId as id, sNombre as nombre, sLugar as lugar, sCorreo as correo, iTelefono as telefono, fecha as fecha, sTexto as texto, bCorreo as env FROM mensaje ORDER BY fecha DESC";
					
				$result = $bd_conn->query($sql) ;
					
				if(!$result) {
					throw new PetitionException("Sin resultados", 404) ;
				}
				else
				{
					$arr = $result->fetchAll(PDO::FETCH_ASSOC);
					
					$cnt = count($arr);
					if($cnt == 0)
						throw new PetitionException("Sin resultados", 199);
					
					$code = 200;
					$message = "($cnt) Mensaje(s) Encontrados" ;
					$data = $arr;
				}
		}
		else
		{
			$sesion->AbortSession();
			$code = 302;
			$message = "Welcome :) ";
		}
	}
	elseif ($_SERVER['REQUEST_METHOD'] == "POST")
	{	
		$sesion = new SesionManager();
		if($sesion->GetStatusSession())
		{
			if(isset($_POST['del']) and isset($_POST['key']))
			{
				$sql = "DELETE FROM mensaje WHERE iId = ?"; 
				
				$prep = $bd_conn->prepare($sql);
				$prep->bindParam(1, $_POST['key']);
				
				$result = $prep->execute();
				
				if ($result){
					$code = 200;
					$message = "Eliminado Exit&oacute;samente";
				}else{
					$code = 404;
					$message = "Error de Consulta";
				}			
			}
			else if(isset($_POST['chg_mail']) and isset($_POST['setnmail']) and isset($_SESSION['userid']))
			{
				$data = $_POST;
				
				$nmail = $_POST['setnmail'];
				$ii =  intval($_SESSION['userid']);
				
				$sql = 'UPDATE perfil SET sEmail = ? WHERE iId = ? ';

				$prep = $bd_conn->prepare($sql);
				$prep->bindParam(1, $nmail, PDO::PARAM_STR);
				$prep->bindParam(2, $ii, PDO::PARAM_INT);

				$res = $prep->execute();

				if($res){
					$code = 201;
					$message = "El Correo ha sido modificado correctamente";
				}else {
					throw new PetitionException("No se pudo ejecutar la consulta", 400);
				}
			}
			else if(isset($_POST['getnmail']) and isset($_SESSION['userid'])){
				$sql = "SELECT sEmail as toemail FROM perfil WHERE iId = ?";

				$ii =  intval($_SESSION['userid']);
				$prep = $bd_conn->prepare($sql);
				$prep->bindParam(1, $ii, PDO::PARAM_INT);

				$res = $prep->execute();

				if($res){
					$arr = $prep->fetchAll(PDO::FETCH_ASSOC);

					if(count($arr) == 1){
						$code = 200;
						$message = "Ok";
						$data = $arr[0];
					}else {
						throw new PetitionException("Resultados no encontrados", 404);
					}
				}else {
					throw new PetitionException("No se pudo ejecutar la consulta", 400);
				}
			}
			else
			{
				throw new PetitionException("Error en Parametros de Entrada", 400);
			}
		}
		else
		{
			$sesion->AbortSession();
			$code = 404;
			$message = "Requiere Iniciar Sesi&oacute;n";
		}
	}
	else 
	{
		throw new Exception("Peticion No Reconocida: ".$_SERVER['REQUEST_METHOD'], 400);	
	}
}
catch (RequestException $reqex)
{
	$code = $reqex->getCode();
	$message = $reqex->getCode();
}
catch (PetitionException $pttex)
{
	$code = $pttex->getCode();
	$message = $pttex->getMessage();
}
catch (PDOException $pdoex)
{
	$code = $pdoex->getCode();
	$message = "Error Con la Base de Datos \n".$pdoex->getMessage();
}
catch (Exception $ex)
{
	$code = $ex->getCode(); //Undefined
	$message = $ex->getMessage(); 
}
/*Preparar Respuesta*/
$out_put = array('code'=> $code, 'message'=> $message, 'data'=>$data);
/*Encode JSON*/
$json_res = json_encode($out_put);
/*Desplegar*/
echo $json_res;
?>