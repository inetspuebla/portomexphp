-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-08-2016 a las 17:42:18
-- Versión del servidor: 5.6.26
-- Versión de PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `portomex`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagen`
--

DROP TABLE IF EXISTS `imagen`;
CREATE TABLE IF NOT EXISTS `imagen` (
  `iId` int(11) NOT NULL,
  `sNombre` varchar(30) NOT NULL,
  `sUrlNombre` varchar(50) NOT NULL,
  `sDescripcion` text NOT NULL,
  `iId_TipoImagen` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensaje`
--

DROP TABLE IF EXISTS `mensaje`;
CREATE TABLE IF NOT EXISTS `mensaje` (
  `iId` int(11) NOT NULL,
  `sNombre` varchar(40) NOT NULL,
  `sLugar` varchar(60) NOT NULL,
  `sCorreo` varchar(100) NOT NULL,
  `iTelefono` int(11) NOT NULL,
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sTexto` text NOT NULL,
  `bCorreo` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil`
--

DROP TABLE IF EXISTS `perfil`;
CREATE TABLE IF NOT EXISTS `perfil` (
  `iId` int(11) NOT NULL,
  `sNombre` varchar(40) NOT NULL,
  `sCalle` varchar(40) NOT NULL,
  `sNumext` varchar(10) NOT NULL,
  `sNumint` varchar(10) NOT NULL,
  `sColonia` varchar(40) NOT NULL,
  `sMunicipio` varchar(40) NOT NULL,
  `sEstado` varchar(30) NOT NULL,
  `iTel1` int(11) NOT NULL,
  `iTel2` int(11) NOT NULL,
  `sEmail` varchar(50) NOT NULL,
  `sDesc` text NOT NULL,
  `iId_Video` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Datos de la Empresa';

--
-- Volcado de datos para la tabla `perfil`
--

INSERT INTO `perfil` (`iId`, `sNombre`, `sCalle`, `sNumext`, `sNumint`, `sColonia`, `sMunicipio`, `sEstado`, `iTel1`, `iTel2`, `sEmail`, `sDesc`, `iId_Video`) VALUES
(1, 'PortoMex', 'Calle', '300', '', 'Puebla', 'Puebla', 'Puebla', 1234567890, 0, 'mail@mail.com.mx', '<span style="color:rgb(0,0,255);font-size: 16px;"><b><span style="color:rgb(0,255,255);font-size: 16px;"><i><span style="color:rgb(0,0,255);font-size: 16px;"><span style="color:rgb(0,0,255);font-size: 16px;">Descripción</span><font color="#6aa84f" style="color: rgb(0, 0, 255); font-size: 16px;"><span style="font-size: 16px; color: rgb(0, 0, 255);">&nbsp;de&nbsp;</span></font><span style="font-size: 16px; line-height: 28.5714px; color: rgb(0, 0, 255);">algo</span><font color="#6aa84f" style="color: rgb(0, 0, 255); font-size: 16px;"><span style="font-size: 16px; color: rgb(0, 0, 255);">&nbsp;pero no entiendo porque no salió el editor html Pero aqui esta y es todo pero veremos que poco a poco se va actualizando y es mejor visualizarlo de este modo usando un div inner porque no es correcto observar solo una cadena de html, parece que ya pero aun necesito hacer mas pruebas como esta que te permita ver que chingaos estaba pasando como esto</span></font></span></i></span></b></span>', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recuperacion`
--

DROP TABLE IF EXISTS `recuperacion`;
CREATE TABLE IF NOT EXISTS `recuperacion` (
  `id` int(11) NOT NULL,
  `validkey` varchar(200) NOT NULL,
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Tabla de acciones de recuperacion';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoimagen`
--

DROP TABLE IF EXISTS `tipoimagen`;
CREATE TABLE IF NOT EXISTS `tipoimagen` (
  `iId` int(11) NOT NULL,
  `sDesc` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipoimagen`
--

INSERT INTO `tipoimagen` (`iId`, `sDesc`) VALUES
(1, 'Producto'),
(2, 'Promocion'),
(3, 'Perfil'),
(4, 'Otro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `userpass` varchar(50) NOT NULL,
  `rol` enum('admin','user') NOT NULL DEFAULT 'user',
  `correoassoc` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `username`, `userpass`, `rol`, `correoassoc`) VALUES
(1, 'usr', 'login1234', 'user', 'test_inets@outlook.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `video`
--

DROP TABLE IF EXISTS `video`;
CREATE TABLE IF NOT EXISTS `video` (
  `iIdVideo` int(11) NOT NULL,
  `sVideo` varchar(15) NOT NULL,
  `sPlaylist` varchar(50) NOT NULL,
  `bActivo` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `video`
--

INSERT INTO `video` (`iIdVideo`, `sVideo`, `sPlaylist`, `bActivo`) VALUES
(1, 'HdOkgrK5GPM', '', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `imagen`
--
ALTER TABLE `imagen`
  ADD PRIMARY KEY (`iId`),
  ADD KEY `fk_Imagen_TipoImagen1_idx` (`iId_TipoImagen`);

--
-- Indices de la tabla `mensaje`
--
ALTER TABLE `mensaje`
  ADD PRIMARY KEY (`iId`);

--
-- Indices de la tabla `perfil`
--
ALTER TABLE `perfil`
  ADD PRIMARY KEY (`iId`),
  ADD KEY `fk_Perfil_Video_idx` (`iId_Video`);

--
-- Indices de la tabla `recuperacion`
--
ALTER TABLE `recuperacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipoimagen`
--
ALTER TABLE `tipoimagen`
  ADD PRIMARY KEY (`iId`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indices de la tabla `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`iIdVideo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `imagen`
--
ALTER TABLE `imagen`
  MODIFY `iId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `mensaje`
--
ALTER TABLE `mensaje`
  MODIFY `iId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `perfil`
--
ALTER TABLE `perfil`
  MODIFY `iId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `recuperacion`
--
ALTER TABLE `recuperacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tipoimagen`
--
ALTER TABLE `tipoimagen`
  MODIFY `iId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `video`
--
ALTER TABLE `video`
  MODIFY `iIdVideo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `perfil`
--
ALTER TABLE `perfil`
  ADD CONSTRAINT `fk_Perfil_Video` FOREIGN KEY (`iId_Video`) REFERENCES `video` (`iIdVideo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
