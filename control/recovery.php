<?php 
include_once 'core.php';
/* Envio de la Respuesta en formato JSON */
header("Content-Type:application/json");
$code = 0;
$message = "";
$data = array();
try{
	$bd_conn= ConexionBD::obtenerInstancia()->obtenerBD();
	
	if($_SERVER['REQUEST_METHOD'] == "GET") 			/*Consultar Checkeo de Conexion*/
	{
        ;
	}
	elseif ($_SERVER['REQUEST_METHOD'] == "POST") 		/*Consulta */
	{
        if(isset($_POST['recover']) and isset($_POST['rec_user']) and isset($_POST['rec_email']))
        {
			$user = trim($_POST['rec_user']);
			$mail = trim($_POST['rec_email']);

			if($user == "" and $mail == "")
            {
				throw new PetitionException("Campos Vac&iacute;os", 404);
			} 
            else 
            {
				$sql = "";
				$prep = $bd_conn->prepare($sql);
				if($user != "" and $mail != "")
                {
					$sql = "SELECT id, username, correoassoc FROM usuario WHERE username = ? and correoassoc = ?";
					$prep = $bd_conn->prepare($sql);
					$prep->bindParam(1, $user, PDO::PARAM_STR);
					$prep->bindParam(2, $mail, PDO::PARAM_STR);
				} 
                else if($user != "")
                {
					$sql = "SELECT id, username, correoassoc FROM usuario WHERE username = ? "; //and correoassoc = ?";
					$prep = $bd_conn->prepare($sql);
					$prep->bindParam(1, $user, PDO::PARAM_STR);
				}
                else if($mail != "")
                {
					$sql = "SELECT id, username, correoassoc FROM usuario WHERE correoassoc = ?";
					$prep = $bd_conn->prepare($sql);
					$prep->bindParam(1, $mail, PDO::PARAM_STR);
				}

				$res = $prep->execute();

				if(!$res){
					throw new PetitionException("Error en la Consulta", 400);
				} else {
					$arr = $prep->fetchAll(PDO::FETCH_ASSOC);

					if(count($arr) == 1){
						$arr = $arr[0];

						$mail = $arr['correoassoc'];
						$idusr = $arr['id'];
						$pauta = md5(md5(time()));
						$sql = "INSERT INTO recuperacion (`validkey`, `user`, `email`) VALUES (?, ?, ?)";

						$prep = $bd_conn->prepare($sql);
						$prep->bindParam(1, $pauta, PDO::PARAM_STR);
						$prep->bindParam(2, $idusr, PDO::PARAM_STR);
						$prep->bindParam(3, $mail, PDO::PARAM_STR);

						$res2 = $prep->execute();
						
						if($res2){
							$link = "http://".$_SERVER['HTTP_HOST']."/recover.php?peticion=".$pauta;

							$msg_mail_recover = "<a href=\"".$link."\">".$link."</a>";

                            $res3 = @mail($mail, "Peticion de Recuperacion", $msg_mail_recover);

                            if($res3){
                                $code = 201;
                                $message = "Mensaje Enviado Con Exito. Revise su correo Electronico";
                                $data = array('dir'=>$link);
                            }else {
                                $code = 210;
                                $message = "No se pudo enviar correo electronico. Acceda a la liga siguiente";
                                $data = array('dir'=>$link);
                            }
						}else {
							throw new PetitionException("Error en Peticion", 404);
						}
					}else {
						throw new PetitionException("Nombre de usuario y/o correo inexistente", 404);
					}
				}
			}
		}
		else if(isset($_POST['setrcvy']) and isset($_POST['datpws'])){
			$sesion = new SesionManager();
			$dat = $sesion->GetValue('DataRec');
			if($dat != null)
			{
//				$data = array_merge($_POST, $_SESSION);

				$uid = $dat['uid'];
				$apik = $dat['apikey'];
				$newpw = $_POST['datpws'];
				
				$sql="UPDATE usuario SET userpass = ? WHERE id = ?";
				$prep = $bd_conn->prepare($sql);

				$prep->bindParam(1, $newpw, PDO::PARAM_STR);
				$prep->bindParam(2, $uid, PDO::PARAM_STR);

				$res = $prep->execute();
				
				if(!$res)
				{
					throw new PetitionException("No se pudo Actualizar", 404);
				}

				$sql = "UPDATE `recuperacion` SET `activo`= 0 WHERE `validkey`= ?"; //As: 'd457b3431e581c2145a96c58cb6f615c'"
				$prep = $bd_conn->prepare($sql);
				$prep->bindParam(1, $apik, PDO::PARAM_STR);

				$res = $prep->execute();
				
				if(!$res){
					throw new PetitionException("Proceso Realizado!", 200);
				}else {
					$code = 201;
					$message = "Ok Proceso Completado";
					$sesion->UnsetValue('DataRec');
					session_commit();
				}
			}else {
				$sesion->CloseSession();
				throw new PetitionException("No hay Informaci&oacute;n para realizar el cambio de contrase&ntilde;a", 404);
			}
		}
		else
		{
            throw new RequestException("Error en Variables de Entrada", 400);
        }
    }
    else {
		throw new RequestException("Peticion No Reconocida: ".$_SERVER['REQUEST_METHOD'], 400); //(400 Bad Request)
	}
}
catch (RequestException $rqex){
	$code = $rqex->getCode();
	$message = $rqex->getMessage();
}
catch (PetitionException $ptex){
	$code = $ptex->getCode();
	$message = $ptex->getMessage();
}
catch (PDOException $pdoex)
{
	$code = $pdoex->getCode();
	$message = "Error con la Base de Datos";
}
catch (Exception $ex)
{
	$code = $ex->getCode(); //Undefined
	$message = "Error Desconocido"; 
}
/*Preparar Respuesta*/
$out_put = array('code'=>$code,'message'=>$message, 'data' => $data);
/*Encode JSON*/
$json_res = json_encode($out_put);
/*Desplegar*/
echo $json_res;
?>