<?php
/* Configuraciones del Servidor de Correo */
define("SECURITY_PROTOCOL", "tls");
define("HOST_MAIL", "smtp-mail.outlook.com");
define("NAME_MAIL", "test_inets@outlook.com");
define("PASS_MAIL", "Mail@1234");
define("PORT_MAIL", 587);

define("REMITENTE_CORREO", "test_inets@outlook.com");
define("REMITENTE_NOMBRE", "Test Inets Mail"); 

define("DESTINATARIO_CORREO", "mvaz.12.02@gmail.com");
define("DESTINATARIO_NOMBRE", "Google Mail");

/* Estatus de Correo */
define("CORREO_PREPARANDO", 0);

define("CORREO_ALMACENADO", 1);
define("CORREO_ERROR_ALMACENAR", 2);

define("CORREO_ENVIADO", 4);
define("CORREO_ERROR_ENVIAR", 8);

define("CORREO_OK_ALMACENADO_OK_ENVIADO", ( CORREO_ALMACENADO | CORREO_ENVIADO) );
define("CORREO_NO_ALMACENADO_OK_ENVIADO", ( CORREO_ERROR_ALMACENAR | CORREO_ENVIADO ));
define("CORREO_OK_ALMACENADO_NO_ENVIADO", ( CORREO_ALMACENADO | CORREO_ERROR_ENVIAR ));
define("CORREO_NO_ALMACENADO_NO_ENVIADO", ( CORREO_ERROR_ALMACENAR | CORREO_ERROR_ENVIAR ));
?>