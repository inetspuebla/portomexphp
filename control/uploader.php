<?php 
include_once 'core.php';
/* Envio de la Respuesta en formato JSON */
header("Content-Type:application/json");
$code = 0;
$message = "";
$data = array();

try{
	$bd_conn= ConexionBD::obtenerInstancia()->obtenerBD();
	
	if($_SERVER['REQUEST_METHOD'] == "GET") 			/*Consultar Checkeo de Conexion*/
	{
		$sesion = new SesionManager();
		if($sesion->GetStatusSession())
		{
				$sql=" SELECT iId_TipoImagen as tipo ".
						" FROM imagen ";
					
				$result = $bd_conn->query($sql) ;
					
				if(!$result) {
					throw new PetitionException("Sin resultados", 404);
				}
				else
				{
					$arr = $result->fetchAll(PDO::FETCH_ASSOC);
			
					$code = 200;
					$message = "Resultados imágenes" ;
					$data = $arr;
				}
		}
		else
		{
			session_abort();
			
			if (isset($_GET['keyall']))
			{
				$sql=" SELECT * ".
						" FROM imagen ";
					
				$result = $bd_conn->query($sql) ;
					
				if(!$result) {
					throw new PetitionException("Error en la Consulta", 404) ;
				}
				else
				{
					$arr = $result->fetchAll(PDO::FETCH_ASSOC);
					if(count($arr) == 0) {
						throw new PetitionException("Sin resultados", 404) ;
					}

					$code = 200;
					$message = "Resultados imágenes" ;
					$data = $arr;
				}
			}else{
				$code = 302;
				$message = "Welcome :) ";
			}
		}
	}
	elseif ($_SERVER['REQUEST_METHOD'] == "POST") 		/*Consulta */
	{	
		if( isset( $_FILES['uploadedfile'] ) )
		{
			
			$file = $_FILES['uploadedfile'];
			
			$name = explode('.', $file['name']);
			$ext = array_pop($name);
			$name = implode('', $name);
			$newname =  date_timestamp_get(date_create())."_".mt_rand(1000000, mt_getrandmax()).".".$ext;
			
			if (!copy($file['tmp_name'], "../res/".$newname))
			{
				$code = 400; 
				$message = "error de copia";
				#$data['fileobj'] = $file;
			}
			else {
				$code = 201;
				$message = "Subido con &Eacute;xito";
				$data = array('newname'=> $newname, 'nameorg'=> $name);
			}
		}
		elseif ( isset($_POST['name']) && isset($_POST['orgn'] ) && isset($_POST['ntype']) && isset($_POST['tex']) )
		{				
			$sql="INSERT INTO ".
				"imagen(`sNombre`, `sUrlNombre`, `sDescripcion`, `iId_TipoImagen`) ".
				"VALUES ( ?, ?, ?, ? ) ";
			
			$prep = $bd_conn->prepare($sql);
			$prep->bindParam(1, $_POST['orgn'], PDO::PARAM_STR);
			$prep->bindParam(2, $_POST['name'], PDO::PARAM_STR);
			$prep->bindParam(3, $_POST['tex'], PDO::PARAM_STR);
			$prep->bindParam(4, $_POST['ntype'], PDO::PARAM_STR);
			
			$result = $prep->execute();
																				
			if(!$result)
			{
				$code = 400;
				$message = "Error de Registro, intente nuevamente";
			}
			else {
				$code = 201;
				$message = "Imagen Registrada ";
			}
		}
		else 
		{
			$code = 400; // (400 Bad Request)
			$message = "Petici&oacute;n no reconocida";
		}
	}
	else 
	{
		$code = 400; //(400 Bad Request)
		$message = "Peticion No Reconocida: ".$_SERVER['REQUEST_METHOD'];
	}
}
catch (PDOException $pdoex)
{
	$code = $pdoex->getCode();
	$message = "Error con la Base de Datos";
}
catch (Exception $ex)
{
	$code = $ex->getCode(); //Undefined
	$message = $ex->getMessage();
}
/*Preparar Respuesta*/
$out_put = array('code'=> $code, 'message'=> $message, 'data'=>$data);
/*Encode JSON*/
$json_res = json_encode($out_put);
/*Desplegar*/
echo $json_res;
?>