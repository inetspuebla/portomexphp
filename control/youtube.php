<?php
/**
 * Long
 * https://www.youtube.com/watch?v=HdOkgrK5GPM
 * Short
 * https://youtu.be/HdOkgrK5GPM
 * full with list
 * https://www.youtube.com/watch?v=-KqIzH5Kq8k&index=1&list=PLA3B049D0C7CEC716
 * short with list
 * https://youtu.be/HdOkgrK5GPM?list=PLq0g-O2tr8LU-Il71eScRNb8H1CnsLF9_
 * Only Video
 * <iframe src="https://www.youtube.com/embed/cp9B6KjjDCQ?rel=0" allowfullscreen="allowfullscreen"></iframe>
 * Video and PlayList
 * <iframe width="560" height="315" src="https://www.youtube.com/embed/HdOkgrK5GPM?list=PLq0g-O2tr8LU-Il71eScRNb8H1CnsLF9_" frameborder="0" allowfullscreen></iframe> * Playlist
 * Only Playlist
 * <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLq0g-O2tr8LU-Il71eScRNb8H1CnsLF9_" frameborder="0" allowfullscreen></iframe>
 * */

require_once 'core.php';

/* Envio de la Respuesta en formato JSON */
header("Content-Type:application/json");
$code = 0;
$message = "";
$data = array();

try {
	$bd_conn= ConexionBD::obtenerInstancia()->obtenerBD();
	
	if($_SERVER['REQUEST_METHOD'] == "GET"){
		if(isset($_GET['video'])){
			$sql = "SELECT sVideo as idurl FROM video WHERE bActivo = 1";

			$res = $bd_conn->query($sql);

			if($res){
				$arr = $res->fetchAll(PDO::FETCH_ASSOC);

				if(count($arr) == 0){
					throw new PetitionException("No results", 400);
				}
				$code = 200;
				$message = "Ok";

				for ($i=0; $i < count($arr); $i++) { 
					$data[$i] = "http://www.youtube.com/embed/".$arr[$i]['idurl'];
				}
			}else {
				throw new PetitionException("Consulta Inv&aacute;lida", 404);
			}
		}else {
			throw new PetitionException("Sin Par&aacute;metros", 100);
		}
	}else if($_SERVER['REQUEST_METHOD'] == "POST"){
		$session = new SesionManager();
		
		if($session->GetStatusSession()){
			if(isset($_POST['setvideo'])){
				$extras = array();
				
				$input_url = $_POST['setvideo'];
					
				$url = explode("//", $input_url);
					
				//Extraer Protocolo
				$p = array_shift($url);
					
				if($p != "http:" and $p != "https:"){
					throw new Exception("Error de Protocolo <br>".$p, 404);
				}
				//Extraer Dominio
				$p = array_shift($url);
					
				if(count($url) > 0){
					$extras[] = $url;
				}
					
				$url = explode("/", $p);
					
				$p = array_shift($url);
				$short_link = false;
				if($p == "www.youtube.com" or $p == "youtube.com"){
					$short_link = false;
				}else if($p == "youtu.be"){
					$short_link = true;
				}else{
					throw new Exception("Error de Dominio <br>".$p, 404);
				}
					
				//Extraer Parametros
				$p = array_shift($url);
				
				if(count($url) > 0){
					$extras[] = $url;
				}
				$url = explode("&", $p);
				$assoc_params = array();
				$value_video = NULL;
				$value_playlist = NULL;
					
				if(!$short_link){
					foreach ($url as $item){
						$param = explode("=", $item);
						$key = array_shift($param);
						$assoc_params[$key] = array_shift($param);
					}
				
				
					if(isset($assoc_params["watch?v"])){
						$value_video = $assoc_params["watch?v"];
					}
					if(isset($assoc_params["list"])){
						$value_playlist = $assoc_params["list"];
					}
				}
				else {
					$p = array_shift($url);
					if(count($url) > 0){
						$extras[] = $url;
					}
				
					$url = explode("?", $p);
					$p = array_shift($url);
				
					foreach ($url as $item){
						$param = explode("=", $item);
						$key = array_shift($param);
						$val = array_shift($param);
							
						if($val != null)
							$assoc_params[$key] = $val;
							else
								$assoc_params[] = $key;
					}
				
					$assoc_params['video'] = $p;
				
					if(isset($assoc_params["video"])){
						$value_video = $assoc_params["video"];
					}
					if(isset($assoc_params["list"])){
						$value_playlist = $assoc_params["list"];
					}
				}
				
				$data = array('short_link' => $short_link, 'video' => $value_video, 'list' => $value_playlist, 'extras' => $extras);
				
				if( $value_video == null and $value_playlist == null){
					throw new PetitionException("Error");
				}
				
				$code = 201;
				$message = "Ok";
				
			}elseif (isset($_POST['getvideo'])){
				$sql = "SELECT iIdVideo as id, sVideo as video, sPlaylist as playlist FROM video WHERE bActivo = 1";
				
				$res = $bd_conn->query($sql);
				
				if($res){
					$data = $res->fetchAll(PDO::FETCH_ASSOC);
					if(count($data) == 0){
						throw new PetitionException("No hay video disponible", 400);
					}else{
					$code = 200;
					$message = "Video Encontrado";
					
					$data = $data[0];}
				}else{
					throw new PetitionException("No hay video disponible", 400);
				}
			}elseif (isset($_POST['changevideo']) and isset($_POST['cvideo']) and isset($_POST['cplaylist']) and isset($_POST['cidvideo'])) {
				$data = $_POST;
				@$id = intval($_POST['cidvideo']);
				$video = $_POST['cvideo'];
				$playlist = $_POST['cplaylist'];
				$activo = true;
				$sql = "";
				$prp = $bd_conn->prepare($sql);
				
				if($id == ""){
					$sql = "INSERT INTO video(`sVideo`, `sPlaylist`, `bActivo`) VALUES ( ? , ? , ? )";
					$prp = $bd_conn->prepare($sql);
					
					$prp->bindParam(1, $video, PDO::PARAM_STR);
					$prp->bindParam(2, $playlist, PDO::PARAM_STR);
					$prp->bindParam(3, $activo, PDO::PARAM_BOOL);
				}else {
					$sql = "UPDATE video SET sVideo = ? ,sPlaylist = ? ,bActivo= ? WHERE iIdVideo = ?";
					$prp = $bd_conn->prepare($sql);
					
					$prp->bindParam(1, $video, PDO::PARAM_STR);
					$prp->bindParam(2, $playlist, PDO::PARAM_STR);
					$prp->bindParam(3, $activo, PDO::PARAM_BOOL);
					$prp->bindParam(4, $id, PDO::PARAM_INT);
				}
				
				$result = $prp->execute();
				
				if($result){
					
				}
			}
			else {
				throw new PetitionException("Bad Params", 400);
			}
		}else {
			throw new RequestException("Unauthorized", 404);
		}
	}else {
		throw new RequestException("Unrecognized: ".$_SERVER['REQUEST_METHOD'], 410);
	}
} catch (PDOException $pdoex) {
	$code = $pdoex->getCode();
	$message = "Error con la Base de Datos";	
} catch (Exception $e) {
	$code = $e->getCode();
	$message = $e->getMessage();
}

/*Preparar Respuesta*/
$out_put = array('code'=> $code, 'message'=> $message, 'data'=>$data);
/*Encode JSON*/
$json_res = json_encode($out_put);
/*Desplegar*/
echo $json_res;

