<?php
if($_SERVER['REQUEST_URI'] == $_SERVER['SCRIPT_NAME']){
	header("Location: ../.");
	exit();
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>404 Recurso No Encontrado</title>
	<link href="http://www.portomex.com.mx/images/PM_ico_small.ico" type="image/x-icon" rel="shortcut icon" />
	<!-- Remote-->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	</head>
	<style type="text/css">
		body{
			background-image: url("http://www.portomex.com.mx/images/puertas/puerta_automatica1.jpg");
			background-repeat: no-repeat;
			background-attachment: fixed;
			background-size: cover;
		}
		.jumbotron {
			background-color: rgba(100, 100, 100, 0.5);
			color: white;
		}
		.dir_err {
			color: yellow;
			padding-right: 10px;
			padding-left: 10px;
		}
	</style>
<body>
	<div class="container">
		<div class="row">
			<div class="col-xs-12" style="heigth:50px;">
				&nbsp;
			</div>
		<div> 
		<div class="row">
			<div class="col-xs-10 col-xs-offset-1 col-md-8 col-md-offset-2">
				<div class="jumbotron" align="center">
					<h1>Mmm... Algo no sali&oacute; bien... :(</h1>
					<br>
					<p>
						<h4><b>Descripci&oacute;n</b></h4>
					</p>
					<p>
						La direcci&oacute;n 
						<b class="dir_err"><?php echo  $_SERVER['SERVER_NAME'] . $_SERVER['REDIRECT_URL'];?></b> no est&aacute; disponible o es inv&aacute;lida. <br> Por favor compruebe nuevamente
					</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-4 col-xs-offset-4">
				<label></label>
				<a href="." class="btn btn-block btn-success btn-lg">
					<h4><span class="glyphicon glyphicon-home"></span>
					&nbsp;Regresar a Inicio...</h4> 
				</a>
			</div>

		</div>
	</div>	
</body>
</html>