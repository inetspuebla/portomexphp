
<div id="menu" class="navbar navbar-fixed-top ">
	<div class="container">
		<div class="row">
			<div class="col-xs-2 col-sm-3">
				<div id="logo">
					<a href="."><img src="./images/LogoPortoMex.jpeg" alt="PortoMex. Puertas Au&oacute;maticas"></a>
				</div>
			</div>

			<div class="col-sm-8 hidden-xs hidden-sm">
				<div class="collapse navbar-collapse">
					<ul id="menu_list"
					class="nav navbar-var navbar-right nav-pills ">

					<li><a class="page-scroll" href="index.php#page-top"><span class="glyphicon glyphicon-home"></span> Inicio</a></li>
					<li><a class="page-scroll" href="index.php#nosotros"><span class="glyphicon glyphicon-user"></span> Servicios </a></li>
					<li><a class="page-scroll" href="index.php#productos"><span class="glyphicon glyphicon-picture"></span> Productos</a></li>
					<li><a class="page-scroll" href="index.php#promociones"><span class="glyphicon glyphicon-star"></span> Promociones</a></li>
					<li><a class="page-scroll" href="index.php#ubicacion"><span class="glyphicon glyphicon-map-marker"></span> Ubicaci&oacute;n</a></li>
					<li><a class="page-scroll" href="index.php#contacto"><span class="glyphicon glyphicon-inbox"></span> Contacto</a></li>
				</ul>
			</div>
			
		</div>

		<div class="col-xs-7 col-xs-offset-3 col-sm-4 col-sm-offset-5 visible-xs visible-sm" style="padding-top:15px;">
			<button id="mobile-menu-button"
			class="btn btn-default btn-block">Men&uacute; Principal
			<span class="glyphicon glyphicon-th-list"></span>
		</button>
	</div>
	
	<div class="col-xs-10 col-xs-offset-1 visible-xs visible-sm">
		<ul id="mobile-main-menu" class="nav navbar">
			<li><a class="page-scroll itmnmbl" href="index.php#page-top">
				Inicio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<span class="glyphicon glyphicon-home"></span></a></li>
				
				<li><a class="page-scroll itmnmbl" href="index.php#productos">
					Productos&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
					class="glyphicon glyphicon-picture"></span>
				</a></li>
				<li><a class="page-scroll itmnmbl" href="index.php#nosotros">
					servicios&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
					class="glyphicon glyphicon-user"></span>
				</a></li>
				<li><a class="page-scroll itmnmbl" href="index.php#promociones">Promociones&nbsp;&nbsp;<span
					class="glyphicon glyphicon-star"></span></a></li>
					<li><a class="page-scroll itmnmbl" href="index.php#ubicacion">
						Ubicaci&oacute;n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
						class="glyphicon glyphicon-map-marker"></span>
					</a></li>
					<li><a class="page-scroll itmnmbl" href="index.php#contacto">
						Contacto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
						class="glyphicon glyphicon-inbox"></span>
					</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
