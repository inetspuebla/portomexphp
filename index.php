<!DOCTYPE html>
<html lang="es">

<head>
	<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
	<title>Porto Mex. Puertas Autom&aacute;ticas</title>
	<link href="./images/PM_ico_small.ico" type="image/x-icon" rel="shortcut icon" />
	<!-- Local -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<!-- Optional theme -->
	<link rel="stylesheet" href="./css/bootstrap-theme.min.css">
	<!-- Remote-->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Main Theme -->
	<link rel="stylesheet" href="./css/MainStyle.css" />
	<link rel="stylesheet" type="text/css" href="./css/CarouselStyle.css" />
	<!-- Navigation Stylesheet -->
	<link rel="stylesheet" type="text/css" media="all" href="./css/scrolling-nav.css">
	<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Raleway:500,500i" rel="stylesheet">
	<!-- jQuery Text Entry Stylesheet -->
	<link rel="stylesheet" type="text/css" href="./css/jquery-te-1.4.0.css">
	<!-- Librerías que proporcionan soporte de HTML5 para IE8 -->
	<!--[if lt IE 9]>
    	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    	<link rel="stylesheet" href="./css/StyleAnteriores.css" />
    	<![endif]-->
    <link rel="stylesheet" type="text/css" href="./css/carrusel-servicios.css">

</head>

<body id="page-top" class="index" data-spy="scroll" data-target=".nav">
	<header>
		<?php include "./inc/menu.php"

    		?>
	</header>

	<div style="height: 110px; background-color:black;">
	</div>

	<div id="home" class="secciones">
		<div class="row">
			<div id="textinicio" class="col-md-4 col-lg-4">
				<h2>
					<div class="rw-words rw-words-1">
						<span>Fabricaci&oacute;n,</span>
					</div>
					<br/>
					<div class="rw-words rw-words-2">
						<span>reparaci&oacute;n</span>
					</div>
					<br/>
					<div class="rw-words rw-words-3">
						<span>y venta de</span>
					</div>
					<br/>
					<div class="rw-words rw-words-4">
						<span>controles.</span>
					</div>
				</h2>
			</div>
			<div class="col-md-7 col-md-offset-1 col-lg-7 col-lg-offset-1">
				<div id="videopresentacion" class="embed-responsive embed-responsive-16by9">
					<!--Dinamic Content-->
				</div>
			</div>
			<div id="text_inicio" align="center" class="col-md-offset-1 col-md-10 ">
				<h3>Somos una empresa encargada de la fabricaci&oacute;n,
					reparaci&oacute;n, venta e instalaci&oacute;n
					de controles y puertas autom&aacute;ticas.</h3>
			</div>

		</div>

	</div>

	<div id="nosotros" class="secciones">
		<div id="text_nosotros" align="center">
			<h3>Creaci&oacute;n e Instalaci&oacute;n</h3>
			<i> Puertas de Garaje, Rejas Corredizas y Abatibles </i>
		</div>
		 <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 800px; height: 456px; overflow: hidden; visibility: hidden; background-color:black;">
        <!-- Loading Screen -->
	        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
	            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
	            <div style="position:absolute;display:block;background:url('./images/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
	        </div>
	        <div id="jssor_slides_serv" data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 800px; height: 356px; overflow: hidden;">
	            <div data-p="144.50" style="display: none;">
	                <img data-u="image" src="./images/puertas/puerta_carruaje2.jpg" />
	                <img data-u="thumb" src="./images/puertas/puerta_carruaje2.jpg" />
	            </div>
	            <div data-p="144.50" style="display: none;">
	                <img data-u="image" src="./images/puertas/puerta_carruaje6.jpg" />
	                <img data-u="thumb" src="./images/puertas/puerta_carruaje6.jpg" />
	            </div>
	            <div data-p="144.50" style="display: none;">
	                <img data-u="image" src="./images/puertas/puerta_insulada4.jpg" />
	                <img data-u="thumb" src="./images/puertas/puerta_insulada4.jpg" />
	            </div>
	        </div>
        <!-- Thumbnail Navigator -->
	        <div data-u="thumbnavigator" class="jssort01" style="position:absolute;left:0px;bottom:0px;width:800px;height:100px;" data-autocenter="1">
	            <!-- Thumbnail Item Skin Begin -->
	            <div data-u="slides" style="cursor: default;">
	                <div data-u="prototype" class="p">
	                    <div class="w">
	                        <div data-u="thumbnailtemplate" class="t"></div>
	                    </div>
	                    <div class="c"></div>
	                </div>
	            </div>
	            <!-- Thumbnail Item Skin End -->
	        </div>
	        <!-- Arrow Navigator -->
	        <span data-u="arrowleft" class="jssora05l" style="top:158px;left:8px;width:40px;height:40px;"></span>
	        <span data-u="arrowright" class="jssora05r" style="top:158px;right:8px;width:40px;height:40px;"></span>
    	</div>
	</div>

	<div id="productos" class="secciones">
		<div class="container ">
			<div class="row">
				<div id="text_productos" align="center" class=" col-md-offset-2 col-md-8">
					<h3>Contamos con una gran variedad de productos </h3>
				</div>
			</div>
			<div class="row">
				<br>

				<div class="col-xs-12 col-md-12">
					<div class="col-xs-12 col-md-4">
						<div class="thumbnail">
							<a href="catalogo1.php?categoria=controles">
								<img src="./images/MINICONTROL.png">
							</a>
							<br>
							<a href="catalogo1.php?categoria=controles">
								<button id="boton" type="button" class="btn btn-lg col-xs-12 col-md-12"> 
                       				Controles
                   				</button>
							</a>
						</div>
					</div>
					<div class="col-xs-12 col-md-4">
						<div class="thumbnail">
							<a href="catalogo1.php?categoria=abre_rejas">
								<img src="./images/LA_500.png">
							</a>
							<br>
							<a href="catalogo1.php?categoria=abre_rejas">
								<button id="boton" type="button" class="btn btn-lg col-xs-12 col-md-12"> 
									Abre Rejas 
								</button >
							</a>
						</div>
					</div>
					<div class="col-xs-12 col-md-4">
						<div class="thumbnail">
							<a href="catalogo1.php?categoria=abre_puertas">
								<img src="./images/511SA.png" >
							</a>
							<br>     							
							<a href="catalogo1.php?categoria=abre_puertas">
								<button id="boton" type="button" class="btn  btn-lg col-xs-12 col-md-12"> 
									Abre Puertas 
								</button >
							</a>
						</div>
					</div>
        </div>
    </div>
</div>
</div>    

<div id="promociones" class="secciones triangulo-tricolor">
    <div id="text_promociones" align="center" class=" ">
       <h1>Conoce nuestras promociones  </h1>
   </div>
	<div id="carousel-promociones" class="carousel slide" data-ride="carousel" align="center">
		<!-- Indicators -->
     	<ol class="carousel-indicators">
			 <!-- items indicators -->
    	</ol>

    	<!-- Wrapper for slides -->
    	<div class="carousel-inner">
        	<!-- Inner Images -->
    	</div>

    	<!-- Controls -->
    	<a class="left carousel-control" href="#carousel-promociones" role="button" data-slide="prev">
        	<span class="glyphicon glyphicon-chevron-left"></span>
    	</a>
    	<a class="right carousel-control" href="#carousel-promociones" role="button" data-slide="next">
        	<span class="glyphicon glyphicon-chevron-right"></span>
		</a>
	</div>         
</div>


<div id="ubicacion" class="secciones">
  <div class="container">
     <div class="row">
        <div id="text_ubicacion" align="center" class="col-xs-12 col-md-5">
           <h3>Estamos ubicados en</h3>
           <p>
              Calle Melchor Ocampo No. 7, Momoxpan <br> Cholula, Pue.
          </p>
          <h3>Cont&aacute;ctanos:</h3>
          <p>
              Tel. 285-20-77. <br> Cel. 22-24-55-78-92.<br>
              servicio&#64;portomex.com.mx
          </p>
      </div>
      <div class="col-xs-12 col-md-offset-1 col-md-6">
       <div id="mapa_1" class="embed-responsive embed-responsive-16by9">
          <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3770.705286985499!2d-98.26350629146677!3d19.07669103192426!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTnCsDA0JzM1LjgiTiA5OMKwMTUnNDguNSJX!5e0!3m2!1ses!2smx!4v1465244712792"
          frameborder="0" allowfullscreen> </iframe>
      </div>


  </div>

</div>
</div>
</div>

<div id="contacto" class="panel secciones">
  <form id="contactMe" method="post" class="form">
     <div class="panel panel-default">
        <div class="panel-heading" align="center">
           <h4>¿Tienes preguntas o sugerencias? </h4>
           <h4> &iexcl;Escr&iacute;benos!</h4>
           <h4>Estamos para servirte</h4>
       </div>
       <div class="panel-body">
				<!--<div class="container">
				<div class="row">-->
					<div class="col-xs-10 col-xs-offset-1">
						<div id="EstatusContacto"></div>
					</div>
					<div
					class="col-xs-12 col-sm-4 col-sm-offset-1 col-md-4 col-md-offset-1 col-lg-4 col-lg-offset-1">
					<label> Nombre: </label> <input id="name" type="text"
					placeholder="Nombre" required class="form-control forminput"> <br>
					<label> Lugar: </label> <input id="place" type="text"
					placeholder="Lugar" class="form-control forminput"> <br> <label>
					Tel&eacute;fono: </label> <input id="tel" type="number"
					placeholder="Tel: 1234567890" class="form-control forminput"> <br />
					<label> Correo Electr&oacute;nico: </label> <input id="matter"
					type="email" placeholder="mail@mail.com"
					class="form-control forminput"> <br>
				</div>
				<div
				class="col-xs-12 col-sm-5 col-sm-offset-0 col-md-4 col-md-offset-1 col-lg-4 col-lg-offset-1">
				<label>Su Mensaje</label>
				<textarea id="msg" required placeholder="Escriba Aqu&iacute;..."
				rows="15" class="form-control forminput editor_jqte"></textarea>
				<br> <input type="button" id="send" value="Enviar"
				class="btn btn-block btn-success" onclick="EnviarMensaje();">
			</div>
				<!--</div>
			</div>-->
		</div>
	</div>
</form>
</div>

<div class="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-8 footer-left">
				<a href="."><img src="./images/LogoPortoMex.jpeg" alt="PortoMex. Puertas Au&oacute;maticas"></a>
				<br>2016 PortoMex. Puertas Aut&oacute;maticas.
			</div>
			<div class="col-md-4 footer-right">
				<a href="http://www.inets.com.mx" target="_blank"><img alt="..." src="./images/Logo_iNetSoluciones.png"></a>
				<br>Dise&ntilde;ado por:
			</div>
		</div>
	</div>     
</div>

<!-- Scripts Area-->
<!-- Local -->
<!-- JQuery -->
<script type="text/javascript" src="./js/jquery-2.2.4.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script type="text/javascript" src="./js/bootstrap.min.js"></script>
<!-- Remote -->
<!-- JQuery -->
<script src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!-- Requires -->
<script type="text/javascript" src="./js/jquery-te-1.4.0.min.js"></script>
<script type="text/javascript" src="./js/carrousel/jquery.easing.min.js"></script>  
<script type="text/javascript" src="./js/scrolling-nav.js"></script>
<script type="text/javascript" src="./js/util/DOM_Element.js"></script>
<script type="text/javascript" src="./js/util/Alert.js"></script>
<script type="text/javascript" src="./js/util/Imagen_Thumbnail.js"></script>
<script type="text/javascript" src="./js/Inicio.js"></script>
<script type="text/javascript" src="./js/Mensaje.js"></script>
<script type="text/javascript" src="./js/jssor.slider-21.1.5.min.js"></script>
<script type="text/javascript" src="./js/GaleriaControl.js"></script>
<script type="text/javascript" src="./js/GaleriaServicios.js"></script>
<!--  -->
<script type="text/javascript">
		//Plugin de Editor de Texto HTML
		$(".editor_jqte").jqte();
	</script>
	
	<!-- Toogle Mobile Menu -->
	<script>
		function toggleMobileMenu()
		{
			var $mobileMenu = $('#mobile-main-menu');
			$mobileMenu.slideToggle('fast');
		}
		$(document).ready(function() {
			$('#mobile-menu-button').on('click', toggleMobileMenu);
			$('.itmnmbl').on('click', toggleMobileMenu);        
		});
	</script>
</body>
</html>
