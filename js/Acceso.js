/**
 * Acceso.js
 * @author Miguel Vázquez
 */

var uname = "";
var upass = "";

/**
 * Iniciar Sesion
 * */
var IniciarSesion = function () {
	PrepararDatos();
	
	if(ValidarDatos()){
		$.ajax({
			data: {name: uname, pw: upass},
			url: "./control/login.php",
			type: "post",
			beforeSend: function() {
				var al = new Alert("Enviando", TypeAlert.Info, EnumDismiss.None);
				$("#status").html(al.get());
			},
			success: function(response) {
				if(typeof response === 'string'){
                    try {
                        response = $.parseJSON(response); 
                    } catch (error) {
                        console.log(error);                        
                        return;
                    }
                }
				RespuestaLogin(response);
			},
			error: function() {
				var al = new Alert("Error", TypeAlert.Danger, EnumDismiss.None);
				$("#status").html(al.get());
			}
		}
		);
		
		uname = "";
		upass = "";
	}else {
		al = new Alert("Campos Vac&iacute;os", TypeAlert.Warning, EnumDismiss.Dismiss);
		$("#status").html(al.get());
	}
};

/**
 * Preparacion de datos 
 * */
var PrepararDatos = function (){
	uname = $("#uname").val().trim();
	upass = $("#upass").val().trim();
};

/**
 * Validacion de Campos
 * */
var ValidarDatos = function () {
	if(uname == "")
		return false;
	if(upass == "")
		return false;
	
	return true;
};

/**
 * Respuesta de Acceso
 * */
var RespuestaLogin = function( response ) {
	if(typeof response === 'string'){
		try {
			response = $.parseJSON(response); 
		} catch (error) {
			console.log(error);                        
			return;
		}
	}
	if(response.code == 200)
	{
		$("#uname").val("");
		$("#upass").val("");
		window.location = "./control/.";
	}
	else if ( response.code == 401) {
		var al = new Alert(response.message+ "("+ response.code + ")", TypeAlert.Warning, EnumDismiss.Dismiss);
		$("#status").html(al.get());
	}else {
		var al = new Alert(response.message+ "("+ response.code + ")", TypeAlert.Danger, EnumDismiss.Dismiss);
		$("#status").html(al.get());
	}
};

/* Recuperacion de Contrasenia */
$(document).ready(function () { 
	$("#recover_send").on("click", HandleRecover);
	$("#recover_cancel").on("click", TogglePanelRecover);
	$("#to_recover").on("click", TogglePanelRecover);

	$("#recovery").hide();
 });

var Salida = "";
var TooglePanel = false;
var TogglePanelRecover = function() {
	TooglePanel = !TooglePanel;
	if(TooglePanel){
		$("#login").slideToggle("slow");
		$("#recovery").slideToggle("slow");
	}else {
		$("#recovery").slideToggle("slow");
		$("#login").slideToggle("slow");
	}
};

var optRec = {
	recover:"",
	rec_user: "",
	rec_email: ""
}
var HandleRecover = function() {
	if( Prepare_Recover() ){
		$.ajax({
			data: optRec,
			type: "post",
			url: "./control/recovery.php",
			beforeSend: function () { 

			},
			success: function(res) {
				if(typeof response === 'string'){
					try {
						response = $.parseJSON(response); 
					} catch (error) {
						console.log(error);                        
						return;
					}
				}
				
				if(res.code == 201 ){
					var al = new Alert(res.message, TypeAlert.Success, EnumDismiss.Dismiss);
					$("#stat_recover").html(al.get());
				}
				else if(res.code == 210){
					var al = new Alert(res.message + res.data.html, TypeAlert.Success, EnumDismiss.Dismiss);
					$("#stat_recover").html(al.get());
				}else if(res.code = 400 || res.code == 404) {
					var al = new Alert(response.message+ "("+ response.code + ")", TypeAlert.Warning, EnumDismiss.Dismiss);
					$("#stat_recover").html(al.get());
				}else {
					var al = new Alert(response.message+ "("+ response.code + ")", TypeAlert.Danger, EnumDismiss.Dismiss);
					$("#stat_recover").html(al.get());
				}
			},
			error: function () { 
				var al = new Alert("Error Al procesar su peticion", TypeAlert.Danger, EnumDismiss.None);
				$("#stat_recover").html(al.get());
			}
		});
	}else {
		var al = new Alert(Salida, TypeAlert.Warning, EnumDismiss.Dismiss);
		$("#stat_recover").html(al.get());
	}
};

var Prepare_Recover = function () { 
	var rec_user = $("#recover_user").val().trim();
	var rec_email = $("#recover_mail").val().trim();

	if(rec_email == ""){
		if(rec_user == ""){
			Salida = "Ambos Campos Vacios";
			return false;
		}
	}else if(rec_user != ""){
		$("#recover_mail").val("");
		rec_email = "";
	} else if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(rec_email))){
		Salida = "Correo Invalido";
		return false;
	}

	optRec.rec_user = rec_user;
	optRec.rec_email = rec_email;
	return true;
};