/* Galeria */
var pos_alert = '#catalogo_todo_estatus';
var galeria_promociones = '#carousel-promociones';
$(document).ready(function (param) {  
    CargarGaleria("PROMO");
    console.log($(galeria_promociones));
});

var CargarGaleria = function ( type ) {
    var xhr = $.ajax({
        data: { keyall:'', we_are: 'PROMO' },
        url:'./control/galeria.php',
        type:'get',
        beforeSend: function () {
            var alert = new Alert('Sending...', TypeAlert.None, EnumDismiss.None);
            $(pos_alert).html(alert.get());
        },
        success: function (res) {
            console.log(res);

            if(res.code == 200){
                var alert = new Alert(res.message + ' <b>(' + res.code + ')</b>', TypeAlert.Success, EnumDismiss.Dismiss);
                $(pos_alert).html(alert.get()); 
                ContruirGaleria(res.data);
            }else if(res.code == 400 || res.code == 404){
                var alert = new Alert(res.message + ' <b>(' + res.code + ')</b>', TypeAlert.Warning, EnumDismiss.Dismiss);
                $(pos_alert).html(alert.get()); 
                CeroItems_Promo();
            }else {
                var alert = new Alert(res.message + ' <b>(' + res.code + ')</b>', TypeAlert.Danger, EnumDismiss.Dismiss);
                $(pos_alert).html(alert.get());    
            }
        },
        error: function () {
            var alert = new Alert('Error...', TypeAlert.Danger, EnumDismiss.Dismiss);
            $(pos_alert).html(alert.get());
        },
    });
};

var array_promo = Array();
var li_item_promo = new DOMObj('li', {'data-target': galeria_promociones, 'data-slide-to':''});
var ContruirGaleria = function (datainfo) {
    /* Respaldar Array */
    array_promo = datainfo;

    var length = datainfo.length;

    /* Cadena de Salida */
    /* Listado */
    var li_out = '';

    for (var index = 0; index < length; index++) {
        li_item_promo.data['data-slide-to'] = index;
        //li_item_promo.inner = '' + index; 
        li_out += li_item_promo.getHtml();        
    }

    /* Thumbnails */
    var div_out = '';

    for (var index = 0; index < length; index++) {
        div_out += '<div class="item">';
        div_out += '<img src="./res/' + datainfo[index].url + '" alt="' + datainfo[index].imgname + '" />';
        div_out += '<div class="carousel-caption">';
        div_out += '<h3 class="caption-title">' + datainfo[index].imgname + '</h3>';
        div_out += '<div class="caption-description">' + datainfo[index].textdesc + '</div>';
        div_out += '</div>';
        div_out += '</div>';     
    }

    $(galeria_promociones + ' ol.carousel-indicators').html(li_out);
    $(galeria_promociones + ' ol.carousel-indicators li:first').toggleClass('active');
    $(galeria_promociones + ' div.carousel-inner').html(div_out);
    $(galeria_promociones + ' div.carousel-inner div:first').toggleClass('active');
};

var CeroItems_Promo = function ( head, msg ) {
    li_item_promo.data['data-slide-to'] = 0; 
    $(galeria_promociones + ' ol.carousel-indicators').html(li_item_promo.getHtml());
    $(galeria_promociones + ' ol.carousel-indicators li:first').toggleClass('active');
    
    var div_out = '';
    div_out += '<div class="item">';
    div_out += '<img src="./images/Logo_Meddium.png" alt="Sin Promociones" />';
    div_out += '<div class="carousel-caption">';
    div_out += '<h3 class="caption-title">Sin Promociones Actualmente</h3>';
    div_out += '<div class="caption-description">Pronto Mas promociones</div>';
    div_out += '</div>';
    div_out += '</div>'; 
    $(galeria_promociones + ' div.carousel-inner').html(div_out);
    $(galeria_promociones + ' div.carousel-inner div:first').toggleClass('active');
};