$(document).ready(function () {
    CargarVideoPrincipal();
});

var CargarVideoPrincipal = function () {
    $.ajax({
        data: {video: ""},
        url: "./control/youtube.php",
        type: "get",
        beforeSend: function () {
            console.log("Enviando...");
        },
        success: function (res) {
        	if(typeof res === 'string'){
                try {
                    res = $.parseJSON(res); 
                } catch (error) {
                    console.log(error);                        
                    return;
                }
            }
            if(res.code == 200){
                var frm_vdo = new DOMObj("iframe", {"src":res.data[0], "allowfullscreen":"true"}, "<!--close-->");
                $("#videopresentacion").html(frm_vdo.getHtml());
            }else if(res.code == 400 || res.code == 404){
                var img_prw = new DOMObj("img", {"src":"./images/Logo_Meddium.png", "alt":"None"}, "<!--close-->");
                $("#videopresentacion").html(img_prw.getHtml() + "<br>" + res.message + "("+res.code+")");
            }else {
                console.log( res.message + "("+res.code+")");
            }
        },
        error: function () {
            console.log("Error");
        }
    });
};