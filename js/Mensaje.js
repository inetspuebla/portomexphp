/**
 * 
 */

var fMsg = {
	nombre : "",
	lugar : "",
	telefono : "",
	correo : "",
	mensaje : ""
};

var EnviarMensaje = function() {
	$(".forminput").removeClass("empty-input");
	PrepararMensaje($('#name').val(), $('#place').val(), $('#tel').val(), $(
			'#matter').val(), $('#msg').val());

	if (VerificarCampos())
	{
		$.ajax({
			data : fMsg,
			url : "./control/MsgPagPrincipal.php",
			type: "post",
			beforeSend : function() {
				$("#send").attr("disabled", "disabled");
				var al = new Alert("Enviando", TypeAlert.Info, EnumDismiss.None);
				$("#EstatusContacto").html(al.get());
			},
			success : function(response) {
				if(typeof response === 'string'){
                    try {
                        response = $.parseJSON(response); 
                    } catch (error) {
                        console.log(error);
                        var al = new Alert("Error De Servidor.", TypeAlert.Danger, EnumDismiss.None);
        				$("#EstatusContacto").html(al.get());
                        return;
                    }
                }
            	RespuestaMensaje(response);
			},
			error : function() {
				$("#send").removeAttr("disabled");
				var al = new Alert("Error al procesar su mensaje.", TypeAlert.Danger, EnumDismiss.None);
				$("#EstatusContacto").html(al.get());
			}
		});
	}else{
    	var al = new Alert("El <b>"+where_empty.desc+" </b>est&aacute; vac&iacute;o. <br>Verifique por favor...", TypeAlert.Warning, EnumDismiss.Dismiss);
        $("#EstatusContacto").html(al.get());
		$("#"+where_empty.id).addClass("empty-input");
	}
};

var PrepararMensaje = function(_name, _place, _tel, _matter, _msg) {
	fMsg.nombre = _name.trim();
	fMsg.lugar = _place.trim();
	fMsg.telefono = _tel.trim();
	fMsg.correo = _matter.trim();
	fMsg.mensaje = _msg.trim();
};

var where_empty = {
	id: "",
	desc: ""
};
var VerificarCampos = function() {
	if (fMsg.nombre == "")
	{
		where_empty.id ="name";
		where_empty.desc = "Campo Nombre";
		return false;
	}
	if (fMsg.lugar == "")
	{
		where_empty.id = "place";
		where_empty.desc = "Campo Lugar";
		return false;
	}
	if (!(/^\d{10}$/.test(fMsg.telefono)))
	{
		where_empty.id = "tel";
		where_empty.desc = "Campo Tel&eacute;fono";
		return false;
	}
	if (fMsg.mensaje == "")
	{
		where_empty.id = "msg";
		where_empty.desc ="Campo Mensaje";
		return false;
	}
	if(fMsg.correo == "")
	{
		return true;
	}
	else if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(fMsg.correo)))
	{
		where_empty.id = "matter";
		where_empty.desc = "Campo Correo";
		return false;
	}

	return true;
};

var RespuestaMensaje = function(response) {
	$("#send").removeAttr("disabled");
	
	var al = null; 
	
	if(response.code == 201){
		al = new Alert(response.message, TypeAlert.Success, EnumDismiss.Dismiss);		
		$(".forminput").val("");
		$("#msg").jqteVal("");
		$(".forminput").removeClass("empty-input");
	}else if(response.code == 400 || response.code == 404 || response.code == 403){
		al = new Alert(response.message, TypeAlert.Warning, EnumDismiss.Dismiss);
	}else{
		al = new Alert(response.code+ ":"+response.message, TypeAlert.Danger, EnumDismiss.None);
	}
	
	$("#EstatusContacto").html(al.get());
};