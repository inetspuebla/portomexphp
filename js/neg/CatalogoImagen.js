/**
 * 
 */
var al = new Alert();
var alldata = new Array();

$(document).ready( function() {
	Action();
	CargarTodo();
	console.log(alldata);
} );

var CargarTodo = function () {
	$("#cat_producto, #cat_galeria, #cat_promocion, #cat_otros").html("Procesando"); 
	CargarCatalogo("producto","cat_producto");
	CargarCatalogo("perfil","cat_galeria");
	CargarCatalogo("promocion","cat_promocion");
	CargarCatalogo("otro","cat_otros");
};

var tipos = new Array();
var Action = function () {
    $.ajax({
        data: {keytypes:""},
        url:"img_manager.php",
        type:"get",
        beforeSend: function () {
            
        },
        success: function (res) {
            var msg = "";
            if(res.code == 200){
            	tipos = res.data;
				ConstruirSelectorTipoImagen("#ul_registrar_imagen", "#sel_rtimg", res.data);
				ConstruirSelectorTipoImagen("#ul_editar_imagen", "#sel_etimg", res.data);
				ConstruirSelectorTipoImagen("#ul_show_timg", "#sel_opts", res.data);
            }else {
                ;
            }	
        },
        error: function () {
            
        },
    });
};

var chgval = function (slc, id) { 
    $(slc).attr("value",tipos[id - 1].keyid).html(tipos[id - 1].valuetp);
 }
var a_opt = new DOMObj('a', {'onclick':""});
 var ConstruirSelectorTipoImagen = function (domid, parentdom, data) {
    var lgt = data.length;
	console.log(data);
    var msg = "";
    for (var index = 0; index < lgt; index++) {
		a_opt.data['onclick'] = "chgval(\'" +parentdom+"\', "+ tipos[index].keyid + ");";
		a_opt.inner = tipos[index].valuetp;		
        msg += "<li>"+a_opt.getHtml()+"</li>";
    }

    $(parentdom).attr("value",tipos[0].keyid).html(tipos[0].valuetp);

    $(domid).html(msg);
 };

var CargarCatalogo = function(who, whereid) {
	
	$.ajax({
		data : {keyall:" ", typecat:who },
		url  : "img_manager.php",
		type : "get",
		beforeSend: function() {
			al = new Alert("Procesando...", TypeAlert.Info, EnumDismiss.None);
			$("#"+whereid).html(al.get());
		},
		success: function(response) {
			if(typeof response === 'string'){
                try {
                    response = $.parseJSON(response); 
                } catch (error) {
                    console.log(error);                        
                    return;
                }
            }
			console.log("Ok:"+whereid);
			if(response.data.length < 1){
				al = new Alert("<b>No hay imagenes en este apartado. </b> Puede agregar una pulsando el boton superior", TypeAlert.Info, EnumDismiss.None);
				$("#"+whereid).html(al.get());
			}else{
				alldata[whereid] = response.data;
				ProcesarCatalogo(whereid, response.data);
			}
		},
		error: function() {
			al = new Alert("Error en el procesamiento de la peticion", TypeAlert.Warning, EnumDismiss.Dismiss);
			$("#"+whereid).html(al.get());
		}
	});
};

var cat_html = "";
var im = new ImagenThumbnail(0,"","");
var th = new ThumbnailBootstrap(im, "col-xs-6 col-md-4");

var ProcesarCatalogo = function ( whereid, data ) {
	cat_html = "";
	cat_html += "<div class=\"row\">"; 
	for (var i = 0; i < data.length; i++) {
		btnEdit.data['data-catalogo'] = whereid;
		btnElim.data['data-catalogo'] = whereid;
		cat_html += ContruirModelo( i, "../res/"+data[i].urlnombre, data[i].nombre, data[i].descripcion, whereid);
		if(i%2 == 1)
			cat_html += "<div class=\"clearfix visible-xs visible-sm \"></div>";
		if(i%3 == 2)
			cat_html += "<div class=\"clearfix visible-md visible-lg\"></div>";
	}
	cat_html += "</div>";
	$("#"+whereid).html(cat_html);
};

var btnEdit = new DOMObj(
			"button", 
			{
				"class": "btn btn-info", 
				"onclick":"", 
				"role":"button", 
				"data-toggle":"modal", 
				"data-target":"#modal_edit_image",
				"data-catalogo":""
			},
			"Editar");
var btnElim = new DOMObj(
			"button", 
			{
				"class": "btn btn-danger", 
				"onclick":"", 
				"role":"button",
				"data-catalogo": ""
			}, 
			"Eliminar");

var ContruirModelo = function( id, src, name, desc, whereid ) {
	im.id = id;
	im.srcfile = src;
	im.namefile = name;
	th.imgthumnail = im;
	
	btnEdit.data['onclick'] = "EditarInformacion(\'" + whereid + "\', " + id + ");";
	btnElim.data['onclick'] = "EliminarImagen(\'" + whereid + "\', " + id + ");";
	th.captionbody = "<div>"+btnEdit.getHtml()+btnElim.getHtml()+"</div><p>"+desc+"</p>";
	
	 return th.getHtml();
};
var gEdit = "";
var EditarInformacion = function ( whereid, id ) {
	$("#eid").attr("value", alldata[whereid][id].id);
	$("#eimage").attr("src", "../res/"+alldata[whereid][id].urlnombre);
	$("#ename").val(alldata[whereid][id].nombre);
	chgval("#sel_etimg", alldata[whereid][id].tipo);

	$("#etext").jqteVal(alldata[whereid][id].descripcion);
	gEdit = whereid;
};

var SalvarCambiosImagen = function(){
	var up = {
		edit: "",
		eid: $("#eid").val(),
		ename: $("#ename").val(),
		etype: $("#sel_etimg").attr("value"),
		etext: $("#etext").val()
	};
		
	$.ajax({
		data: up,
		type: "post",
		url: "img_manager.php",
		beforeSend: function() {
			var al = new Alert("Enviando", TypeAlert.Info, EnumDismiss.Dismiss);
			$("#editstat").html(al.get());
		},
		success: function(response) {
			if(typeof response === 'string'){
                try {
                    response = $.parseJSON(response); 
                } catch (error) {
                    console.log(error);                        
                    return;
                }
            }
			if(response.code == 201)
			{
				var al = new Alert(response.message, TypeAlert.Success, EnumDismiss.Dismiss);
				$("#editstat").html(al.get());
				alldata = Array();
				CargarCatalogo();
			}else{
				var al = new Alert(response.message, TypeAlert.Warning, EnumDismiss.Dismiss);
				$("#editstat").html(al.get());
			}
		},
		error: function() {
			var al = new Alert("Error", TypeAlert.Danger, EnumDismiss.None);
			$("#editstat").html(al.get());
		}
	});
};

var EliminarImagen = function(whereid, id ) {
	$.ajax({
		data:{delet:"", key:alldata[whereid][id].id},
		type:"post",
		url:"img_manager.php",
		beforeSend: function() {
			var al = new Alert("Procesando...", TypeAlert.Info, EnumDismiss.None);
			$("#st_cat").html(al.get());
		},
		success: function(response) {
			if(typeof response === 'string'){
                try {
                    response = $.parseJSON(response); 
                } catch (error) {
                    console.log(error);                        
                    return;
                }
            }
			if(response.code == 201){
				var al = new Alert(response.message, TypeAlert.Success, EnumDismiss.Dismiss);
				$("#st_cat").html(al.get());
				alldata = Array();
				CargarTodo();
			}else if(response.code == 404){
				var al = new Alert(response.message + "("+response.code+")" , TypeAlert.Warning, EnumDismiss.Dismiss);
				$("#st_cat").html(al.get());
			}else{
				var al = new Alert("Error: ("+response.code+")", TypeAlert.Danger, EnumDismiss.None);
				$("#st_cat").html(al.get());
			};
		},
		error: function() {
			al = new Alert("Procesando...", TypeAlert.Info, EnumDismiss.None);
			$("#st_cat").html(al.get());
		}
	});
};
