$(document).ready(function(){
    CargarCorreoEnvio();
    $("#pnl_body_edit_mail").slideToggle("fast");
    CargarLosMensajes();
    $("#reload_msgs").on("click", CargarLosMensajes);
});

var ResponseObj = function(){
    this.code = 0;
    this.message = "";
    this.data = Array();
};

var ResponseObj = function(sResponse){
    if(typeof sResponse === 'string'){
        sResponse = $.parseJSON(sResponse);
    }
	this.code = sResponse.code;
    this.message = sResponse.message;
    this.data = sResponse.data;
};

var bEditMsg = false;
var EditarCorreoEnvio = function () { 
    bEditMsg = !bEditMsg;

    if( bEditMsg ){
        $("#input_email_to_send").removeAttr("disabled");
        $("#btn_chg_mail_send")
            .removeAttr("disabled")
            .toggleClass("btn-default btn-primary");
        $("#btn_edit_mail")
            .html('<span class="glyphicon glyphicon-remove"></span>&nbsp;Cancelar')
            .toggleClass("btn-success btn-danger");
    }else {
        $("#input_email_to_send").attr("disabled", "disabled");
        $("#btn_chg_mail_send")
            .attr("disabled", "disabled")
            .toggleClass("btn-default btn-primary");
        $("#btn_edit_mail")
            .html('<span class="glyphicon glyphicon-refresh"></span>&nbsp;Editar Correo')
            .toggleClass("btn-success btn-danger");
    }
    $("#pnl_body_edit_mail").slideToggle("fast");
};

var res_sendmail = "";

var CargarCorreoEnvio = function () { 
    $.ajax({
        data: {getnmail:""},
        url: "../control/mensajes.php",
        type: "post",
        beforeSend: function () { 

        },
        success: function (res) { 
            res = new ResponseObj(res);
            console.log(res);

            res_sendmail = res;

            if(res.code == 200){
                $("#show_mail_send").text(res.data.toemail);
                $("#input_email_to_send").val(res.data.toemail);
            }else if(res.code == 400 || res.code == 404){
                $("#show_mail_send").html("<b>Error de Entrada...</b> <i>Code " + res.code + "</i>");
            }else {
                $("#show_mail_send").html("<b>Error de Entrada...</b> <i>Code " + res.code + "</i>");
            }
        },
        error: function () { 
            console.log("Error: getnmail");
        }

    });
};

var GuardarCorreoEnvio = function () { 
    var sMail = $("#input_email_to_send").val().trim();

    //if

    if( sMail != "" && /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test( sMail ) ){
        $.ajax({
            data:{chg_mail:"", setnmail: sMail},
            url: "../control/mensajes.php",
            type: "post",
            beforeSend: function () { 
                var alert = new Alert("Procesando...", TypeAlert.Success, EnumDismiss.None);
	            $("#num_msg").html(alert.get());
            },
            success: function (res) { 
                res = new ResponseObj(res);
                console.log(res);

                if(res.code == 201) {
                    var alert = new Alert(res.message, TypeAlert.Success, EnumDismiss.Dismiss);
	                $("#num_msg").html(alert.get());
                    EditarCorreoEnvio();
                }else if(res.code == 400 || res.code == 404){
                    var alert = new Alert(res.message + " (" + res.code + ")", TypeAlert.Warning, EnumDismiss.Dismiss);
                    $("#num_msg").html(alert.get());
                }else {
                    var alert = new Alert(res.message + " (" + res.code + ")", TypeAlert.Danger, EnumDismiss.Dismiss);
                    $("#num_msg").html(alert.get());
                }
            },
            error: function () {  
                var alert = new Alert("Error Al Procesar Su Petici&oacute;n", TypeAlert.Danger, EnumDismiss.None);
	            $("#num_msg").html(alert.get());
            }
        });
    } 
    else 
    {
        var alerta = new Alert("La información en el campo de <b>correo</b> es inv&aacute;lida", TypeAlert.Warning, EnumDismiss.Dismiss);
        $("#num_msg").html(alerta.get());
    }
};

var data_msgs = Array();
var oDateSystem = new Date();

var CargarLosMensajes = function() {
    $.ajax({
		data: {},
		type: "get",
		url: "../control/mensajes.php",
		beforeSend: function() {
            var alert = new Alert("<label>Procesando...</label>", TypeAlert.Info, EnumDismiss.None);
            $("#num_msg").html(alert.get());
		},
		success: function(res) {
            oDateSystem = new Date();
            res = new ResponseObj(res);
            console.log(res);
            if(res.code == 200) {
                var alert = new Alert(res.message, TypeAlert.Success, EnumDismiss.Dismiss);
                $("#num_msg").html(alert.get());

                ProcesarMensajes(res.data);
            } else if(res.code == 400 || res.code == 404){
                var alert = new Alert(res.message + " (" + res.code + ")", TypeAlert.Warning, EnumDismiss.Dismiss);
                $("#num_msg").html(alert.get());
            } else {
                var alert = new Alert(res.message + " (" + res.code + ")", TypeAlert.Danger, EnumDismiss.Dismiss);
                $("#num_msg").html(alert.get());
            }
        },
        error: function() {
            var alert = new Alert("Error Al Procesar Su Petici&oacute;n", TypeAlert.Danger, EnumDismiss.None);
	        $("#num_msg").html(alert.get());
        }
    });
};

var ProcesarMensajes = function(data) {
    if(data.length == 0) {
        return;
    }
    else {
        data_msgs = data;

        var sMsgsHTML = "";
        for(var i = 0; i < data_msgs.length; i++){
            btn_1.data['onclick'] = "DetalleMensaje("+i+")";
            btn_2.data['onclick'] = "EliminarMensaje("+i+")";
            sMsgsHTML += ConstruirMensaje(data_msgs[i]);
        }

        $("#buzon").html(sMsgsHTML);
    }
};
var oLiRoot = new DOMObj("li", {"class": "list-group-item" }, "");
var oDivNode = new DOMObj("div", {"class": "row"}, "");
var oDivColNode_One = new DOMObj("div", {"class": "col-xs-6"}, "");
var oDivColNode_Two = new DOMObj("div", {"class": "col-xs-3"}, "");
var btn_group = new DOMObj("div", {"class": "btn-group", "role":"group"}, "");
var span_search = new DOMObj("span",{"class":"glyphicon glyphicon-search"}, "<!--close-->");
var span_remove = new DOMObj("span",{"class":"glyphicon glyphicon-remove"}, "<!--close-->");
var btn_1 = new DOMObj(
    "button", 
    {
        "id":"", 
        "class":"btn btn-info", 
        "onclick":"", 
        "role":"button", 
        "data-toggle":"modal", 
        "data-target":"#show_message_modal"
    }, 
    span_search.getHtml() + "Detalles");
var btn_2 = new DOMObj(
    "button", 
    {
        "id":"", 
        "class":"btn btn-danger", 
        "onclick":""
    }, 
    span_remove.getHtml() + "Eliminar");

var ConstruirMensaje = function (oMsg){
    var sFecha = ProcesarFecha(oMsg.fecha);
    var head_c = "<label>Recibido: </label>" + sFecha  + "<br/>" + "<label>De: </label><i>" + oMsg.nombre + "</i>";
    oDivColNode_One.inner = head_c;
    
    var stat = "<label>Estatus</label><br><i>" + (oMsg.env == "0" ? "Almacenado" : "Enviado Al Correo") +"</i>";
    oDivColNode_Two.inner = stat;
    var col_3_1 = oDivColNode_Two.getHtml();

    btn_group.inner = btn_1.getHtml() + btn_2.getHtml();
    
    oDivColNode_Two.inner = btn_group.getHtml();
    
    var col_3_2 = oDivColNode_Two.getHtml();

    oDivNode.inner = oDivColNode_One.getHtml() + col_3_1 + col_3_2;

    oLiRoot.inner = oDivNode.getHtml();

    return oLiRoot.getHtml();
};

var ProcesarFecha = function(sDate) {
    var dateMsg = new Date(sDate);
    var timeCompare = oDateSystem - dateMsg;

    timeCompare /= 1000; //Obtener Segundos

    if(timeCompare < 10){
        return "Hace un momento";
    }else if(timeCompare < 60) {
        return "Hace " + Math.round(timeCompare) + " segundos";
    }else {
        timeCompare /= 60; //Obtener Minutos

        if(timeCompare < 60) {
            return "Hace " + Math.round(timeCompare) + " minutos";
        }else {
            timeCompare /= 60; //Obtener Horas

            if(timeCompare < 1.1){
                return "Hace 1 hora";
            }else if(timeCompare < 2){
                return "Hace m&aacute;s de 1 hora";
            }else if(timeCompare < 24) {
                return "Hace " + Math.round(timeCompare) + " horas";
            }else {
                timeCompare /= 24; //Obtener Dias
                if(timeCompare < 7) {
                    return "Hace " + Math.round(timeCompare) + " dias";
                }else if(timeCompare <= 8 ) {
                    return "Hace 1 semana";
                }else if(timeCompare < 14) {
                    return "Hace " + Math.round(timeCompare) + " dias";
                }else if(timeCompare < 15) {
                    return "Hace 2 semanas";
                }else if(timeCompare < 30){
                    return Dias[dateMsg.getDay()].abbr + " " + dateMsg.getDate() + " " + MesDef[dateMsg.getMonth()].nombre;
                }else if(timeCompare < 32) {
                    return "Hace 1 mes";
                }else if(timeCompare < 60) {
                    return "Hace m&aacute;s de 1 mes";
                }else if(timeCompare < 62) {
                    return "Hace 2 meses";
                }else if(timeCompare < 90) {
                    return "Hace m&aacute;s de 2 meses";
                }else {
                    timeCompare /= 365; //Obtener años en notacion decimal
                    if(timeCompare < 1){
                        return "Hace " + Math.floor(timeCompare * 12) +" meses";
                    }else if(timeCompare < 1.1) {
                        return "Hace 1 a&ntilde;o";
                    }else if(timeCompare < 2) {
                        return "Hace m&aacute;s de un a&ntilde;o";
                    }else {
                        return "Hace " + Math.round(timeCompare) + " a&ntilde;os";
                    }
                }
            }
        }
    }
};

var Dias = [
    {nombre: "Domingo", abbr:"Dom"},
	{nombre: "Lunes", abbr:"Lun"},
	{nombre: "Martes", abbr:"Mar"},
	{nombre: "Miercoles", abbr:"Mie"},
	{nombre: "Jueves", abbr:"Jue"},
	{nombre: "Viernes", abbr:"Vie"},
	{nombre: "Sabado", abbr:"Sab"},
	{nombre: "Domingo", abbr:"Dom"}
];

var MesDef = [
    {nombre: "Enero", abbr: "Ene"},
    {nombre: "Febrero", abbr: "Feb"},
    {nombre: "Marzo", abbr: "Mar"},
    {nombre: "Abril", abbr: "Abr"},
    {nombre: "Mayo", abbr: "May"},
    {nombre: "Junio", abbr: "Jun"},
    {nombre: "Julio", abbr: "Jul"},
    {nombre: "Agosto", abbr: "Ago"},
    {nombre: "Septiembre", abbr: "Sep"},
    {nombre: "Octubre", abbr: "Oct"},
    {nombre: "Noviembre", abbr: "Nov"},
    {nombre: "Diciembre", abbr: "Dic"},
];


var DetalleMensaje = function(id){
	$("#mid").text(data_msgs[id].id);
	$("#mname").text(data_msgs[id].nombre);
	$("#mlug").text(data_msgs[id].lugar);
	$("#mtel").text(data_msgs[id].telefono);
	if(data_msgs[id].correo != "")
		$("#mmail").html("<a id=\"msg\" href=\"mailto:"+data_msgs[id].correo+"\">"+data_msgs[id].correo+"</a>");
	else
		$("#mmail").html("<b>No brind&oacute; correo</b>");
	$("#mdate").text(data_msgs[id].fecha);
	$("#mtext").html(data_msgs[id].texto);
};

var EliminarMensaje = function(id) {
	var i = data_msgs[id].id;
	
	$.ajax({
		data:{del:"", key: i},
		type:"post",
		url:"mensajes.php",
		beforeSend: function() {
			console.log("Eliminando...");
		},
		success: function(response) {
			if(typeof response === 'string'){
                try {
                    response = $.parseJSON(response); 
                } catch (error) {
                    console.log(error);                        
                    return;
                }
            }
			if(response.code == 200){
				console.log(response.message);
				CargarLosMensajes();
			}else if( response.code == 404){
				console.log(response.message);
			}else{
				console.log(response.message + "("+response.code+")");
			}
		},
		error: function() {
			console.log("Error al procesar la peticion");
		}
	});
};

