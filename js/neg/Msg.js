/**
 * 
 */
var allms = new Array();

$(document).ready( function() {
	CargarMensajes();
} );

var CargarMensajes = function() {
	$.ajax({
		data: {},
		type: "get",
		url: "mensajes.php",
		beforeSend: function() {
			$("#num_msg").html("<label>Procesando...</label>");
		},
		success: function(response) {
			if(typeof response === 'string'){
                try {
                    response = $.parseJSON(response); 
                } catch (error) {
                    console.log(error);                        
                    return;
                }
            }
			$("#num_msg").html("");
			$("#allmessages").html("");
			allms = response.data;
			if(response.code == 200){
				console.log("Ok!: "+response.message);				
				var msg = "";
				for (var i = 0; i < allms.length; i++) {
					msg += "<tr>";
					msg += "<td>"+allms[i].fecha+"</td>";
					msg += "<td>"+allms[i].nombre+"</td>";
					var btnEdit = new DOMObj(
						"button", 
						{
							"class": "btn btn-default", 
							onclick:"DetalleMensaje("+i+")", 
							role:"button", 
							"data-toggle":"modal", 
							"data-target":"#show_message_modal"
						},
						"Mas Detalles");
					var btnDelet = new DOMObj(
						"button", 
						{
							"class": "btn btn-default", 
							onclick:"EliminarMensaje("+i+")", 
							role:"button", 
						},
						"Eliminar");
					
					msg += "<td>"+btnEdit.getHtml() + btnDelet.getHtml()+"</td>";
					msg += "</tr>";
				}
				$("#allmessages").html(msg);
				$("#num_msg").html(response.message);
			}else if(response.code == 199){
				$("#num_msg").html(response.message);
			}else if(response.code == 400 || response.code == 404){
				var al = new Alert(response.message+" ("+response.code+")", TypeAlert.Warning, EnumDismiss.Dismiss);
				$("#num_msg").html(al.get());
			}else{
				var al = new Alert(response.message+" ("+response.code+")", TypeAlert.Danger, EnumDismiss.Dismiss);
				$("#num_msg").html(al.get());
			}
		},
		error: function() {
			var al = new Alert(response.message+" ("+response.code+")", TypeAlert.Warning, EnumDismiss.None);
			$("#num_msg").html(al.get());
		}
	});
};

var DetalleMensaje = function(id){
	$("#mid").text(allms[id].id);
	$("#mname").text(allms[id].nombre);
	$("#mlug").text(allms[id].lugar);
	$("#mtel").text(allms[id].telefono);
	if(allms[id].correo != "")
		$("#mmail").html("<a id=\"msg\" href=\"mailto:"+allms[id].correo+"\">"+allms[id].correo+"</a>");
	else
		$("#mmail").html("<b>No brind&oacute; correo</b>");
	$("#mdate").text(allms[id].fecha);
	$("#mtext").html(allms[id].texto);
};

var EliminarMensaje = function(id) {
	var i = allms[id].id;
	
	$.ajax({
		data:{del:"", key: i},
		type:"post",
		url:"mensajes.php",
		beforeSend: function() {
			console.log("Eliminando...");
		},
		success: function(response) {
			if(typeof response === 'string'){
                try {
                    response = $.parseJSON(response); 
                } catch (error) {
                    console.log(error);                        
                    return;
                }
            }
			if(response.code == 200){
				console.log(response.message);
				CargarMensajes();
			}else if( response.code == 404){
				console.log(response.message);
			}else{
				console.log(response.message + "("+response.code+")");
			}
		},
		error: function() {
			console.log("Error al procesar la peticion");
		}
	});
};

