/**
 * 
 */
/* Usuario de Sesion */
$(document).ready(function() {
	Usuario();
});

var Usuario = function() {
	$.ajax({
		data: {nameu: "get"},
		url: "Perfil.php",
		type: "get",
		beforeSend: function(){
			console.log();
		},
		success: function(res){
			if(typeof res === 'string'){
                try {
                    response = $.parseJSON(res); 
                } catch (error) {
                    console.log(error);                        
                    return;
                }
            }
			if(res.code == 200){
				$("#user_name").html(res.message);
			}else if (res.code == 400) {
				$("#user_name").html(res.message);
			}else {

			}
		},
		error: function() {

		}
	});
};

/*Panel: Administracion: Perfil*/
var FormularioEmpresa = {
	nombre: "a",
	direccion: {
		calle : "b",
		numext: "c",
		numint: "d",
		colonia: "e",
		municipio: "f",
		estado: "g",
	},
	tel: [0,0],
	mail: "h",
	descripcion: "i"
};

var BanderaEditarPerfil = false;

$(document).ready( function() {
	CargarPerfil();
	$("#entry_empresa_desc").css("display","none");
} );

var CargarPerfil = function() {
	$.ajax({
		data:{keyall:""},
		url:"Perfil.php",
		type:"get",
		beforeSend: function (){
			alerta = new Alert("Procesando...", TypeAlert.Info, EnumDismiss.None);
			$("#estatus_perfil").html(alerta.get());
		},
		success: function(response) {
			if(typeof response === 'string'){
                try {
                    response = $.parseJSON(response); 
                } catch (error) {
                    console.log(error);                        
                    return;
                }
            }
			if(response.code == 200){
				EscribirCampos_Perfil(response.data);
				$("#estatus_perfil").html("");
			}else if(response.code == 400 || response.code == 404){
				alerta = new Alert(response.message+" ("+response.code+")", TypeAlert.Warning, EnumDismiss.Dismiss);
				$("#estatus_perfil").html(alerta.get());
			}else {
				alerta = new Alert(response.message+" ("+response.code+")", TypeAlert.Danger, EnumDismiss.Dismiss);
				$("#estatus_perfil").html(alerta.get());
			}
		},
		error: function() {
			alerta = new Alert("Error al procesar su solicitud", TypeAlert.Danger, EnumDismiss.None);
			$("#estatus_perfil").html(alerta.get());
		}
	});
};

var EditarPerfil = function() {
	BanderaEditarPerfil = !BanderaEditarPerfil;
	ConfigurarControles_Perfil(BanderaEditarPerfil);
	if(!BanderaEditarPerfil)
		CargarPerfil();
};

var ConfigurarControles_Perfil = function( _bool ) {
	if(BanderaEditarPerfil){
		/* Habilitar Campos */
		$("#entry_empresa_desc").css("display","block");
		$(".fInfo").removeAttr("disabled");
		var texto_desc = $("#empresa_desc").val();
		$("#empresa_desc").jqteVal(texto_desc);
		$("#empresa_desc_html").css("display", "none");
		$("#empresa_desc").css("display", "block");
		$("#edit-info").text("Cancelar");
		$("#edit-info").toggleClass("btn-primary btn-danger");
		$("#save-info").removeAttr("disabled");
		$("#save-info").toggleClass("btn-default btn-success");
	}else {
		/* Cancelar Operacion */
		$("#entry_empresa_desc").css("display","none");
		$("#empresa_desc_html").css("display", "block");
		$(".fInfo").attr("disabled","disabled");
		$("#edit-info").text("Editar");
		$("#edit-info").toggleClass("btn-primary btn-danger");
		$("#save-info").attr("disabled", "disabled");
		$("#save-info").toggleClass("btn-default btn-success");
	}
	if(!BanderaEditarPerfil)
		$("#empresa_desc").css("display", "none");
};

var SalvarPerfil = function() {
	PrepararDatos_Perfil();
	var alerta = new Alert();
	if(ValidarCampos_Perfil()){
		$.ajax({
			data: FormularioEmpresa,
			url: "Perfil.php",
			type:"post",
			beforeSend: function() {
				alerta = new Alert("Procesando...", TypeAlert.Info, EnumDismiss.None);
				$("#estatus_perfil").html(alerta.get());
			},
			success: function(response) {
				if(typeof response === 'string'){
                    try {
                        response = $.parseJSON(response); 
                    } catch (error) {
                        console.log(error);                        
                        return;
                    }
                }
				if(response.code == 201){
					alerta = new Alert(response.message, TypeAlert.Success, EnumDismiss.Dismiss);
					$("#estatus_perfil").html(alerta.get());
					BanderaEditarPerfil = !BanderaEditarPerfil;
					ConfigurarControles_Perfil(BanderaEditarPerfil);
					FormularioEmpresa = response.data;
					$("#empresa_desc_html").html(FormularioEmpresa.descripcion);
				}else if(response.code == 401){
					alerta = new Alert(response.message, TypeAlert.Warning, EnumDismiss.Dismiss);
					$("#estatus_perfil").html(alerta.get());
				}else {
					alerta = new Alert(response.message+"("+response.code+")", TypeAlert.Warning, EnumDismiss.Dismiss);
					$("#estatus_perfil").html(alerta.get());
				}
			},
			error: function() {
				alerta = new Alert("Error al procesar su petici&oacute;n <br> Intente nuevamente", TypeAlert.Danger, EnumDismiss.Dismiss);
				$("#estatus_perfil").html(alerta.get());
			}
		});
		
	}else {
		alerta = new Alert("Campo(s) Inv&aacute;lido(s)", TypeAlert.Warning, EnumDismiss.Dismiss);
		$("#estatus_perfil").html(alerta.get());
	}
};

var PrepararDatos_Perfil = function() {
	FormularioEmpresa.nombre = $("#empresa_nombre").val().trim();
	FormularioEmpresa.direccion.calle = $("#empresa_calle").val().trim();
	FormularioEmpresa.direccion.numext = $("#empresa_num_ext").val();
	FormularioEmpresa.direccion.numint = $("#empresa_num_int").val();
	FormularioEmpresa.direccion.colonia = $("#empresa_colonia").val().trim();
	FormularioEmpresa.direccion.municipio = $("#empresa_municipio").val().trim();
	FormularioEmpresa.direccion.estado = $("#empresa_estado").val().trim();
	FormularioEmpresa.tel[0] = $("#empresa_tel_1").val();
	FormularioEmpresa.tel[1] = $("#empresa_tel_2").val();
	FormularioEmpresa.mail = $("#empresa_email").val().trim();
	FormularioEmpresa.descripcion = $("#empresa_desc").val().trim();
};

var ValidarCampos_Perfil = function() {
	if( FormularioEmpresa.mail == "" )
		return true;
	else if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(FormularioEmpresa.mail)))
		return false;
	
	return true;
};

var EscribirCampos_Perfil = function(data) {
	FormularioEmpresa = data;
	
	$("#empresa_nombre").val(FormularioEmpresa.nombre);
	$("#empresa_calle").val(FormularioEmpresa.direccion.calle);
	$("#empresa_num_ext").val(FormularioEmpresa.direccion.numext);
	$("#empresa_num_int").val(FormularioEmpresa.direccion.numint);
	$("#empresa_colonia").val(FormularioEmpresa.direccion.colonia);
	$("#empresa_municipio").val(FormularioEmpresa.direccion.municipio);
	$("#empresa_estado").val(FormularioEmpresa.direccion.estado);
	$("#empresa_tel_1").val(FormularioEmpresa.tel[0]);
	$("#empresa_tel_2").val(FormularioEmpresa.tel[1]);
	$("#empresa_email").val(FormularioEmpresa.mail);
	$("#empresa_desc").val(FormularioEmpresa.descripcion);
	$("#empresa_desc_html").html(FormularioEmpresa.descripcion);
};

/*Video Principal*/
$(document).ready(function() {
	Obtener_Video();
});

var data_video = {
	id:0,
	idvideo : 0,
	video : "",
	playlist: "",
	activo : 0
};

var Obtener_Video = function() {
	$.ajax({
		data: {getvideo: ""},
		url: "youtube.php",
		type: "post",
		beforeSend: function() {
			
		},
		success: function(res) {
			if(typeof res === 'string'){
                try {
                    response = $.parseJSON(res); 
                } catch (error) {
                    console.log(error);                        
                    return;
                }
            }
			if(res.code == 200){
				data_video = res.data;
				console.log(data_video);
				var v = "http://www.youtube.com/embed/" + data_video.video;
				var youtube_embed = new DOMObj("iframe", {"src": v, "allowfullscreen":"true"}, "<!--close-->");
				$("#yt_").html(youtube_embed.getHtml());
				$("#empresa_video").val("http://www.youtube.com/watch?v="+data_video.video);
			}else if( res.code == 400 || res.code == 404 ){
				$("#yt_").html("<h3>"+res.message+"</h3>");
			}
		},
		error: function() {
			
		}
	});
};

var Bandera_Video = false;
var Editar_Video = function() {
	Bandera_Video = !Bandera_Video;
	
	var emp_video = $("#empresa_video");
	var btn_edit_video = $("#btnEditVideo");
	var btn_save_video = $("#btnSaveVideo");
	var group_save_video = $("#group_sav_video");
	if(Bandera_Video){
		emp_video.removeAttr("disabled");
		btn_edit_video.text("Cancelar");
		btn_edit_video.toggleClass("btn-primary btn-danger");
		group_save_video.css("display", "table-cell");
		btn_save_video.toggleClass("btn-default btn-success");
		btn_save_video.removeAttr("disabled");
	}else {
		emp_video.attr("disabled", "disabled");
		btn_edit_video.text("Editar");
		btn_edit_video.toggleClass("btn-primary btn-danger");
		group_save_video.css("display", "none");
		btn_save_video.toggleClass("btn-default btn-success");
		btn_save_video.attr("disabled", "disabled");
	}
};

var Salvar_Video = function() {
	if(dat.video != undefined){
		if(dat.video != data_video.video){
			data_video.video = dat.video;
			data_video.playlist = "";
			console.log(data_video);
			$.ajax({
				data: {
					changevideo: "", 
					cvideo: data_video.video, 
					cplaylist: data_video.playlist, 
					cidvideo: data_video.id 
				},
				url: "youtube.php",
				type: "post",
				beforeSend: function() {
					
				},
				success: function(res) {
					if(typeof res === 'string'){
	                    try {
	                        response = $.parseJSON(res); 
	                    } catch (error) {
	                        console.log(error);                        
	                        return;
	                    }
	                }
					console.log(res);
					Obtener_Video();
				},
				error: function() {
					
				}
			});
		}else{
			
		}
	}
};
var dat = new Array();

var Comprobar_Video = function() {
	var link = $("#empresa_video").val().trim();
	
	$.ajax({
		data: {setvideo: link},
		url: "youtube.php",
		type: "post",
		beforeSend: function() {
			
		},
		success: function(response) {
			if(typeof response === 'string'){
                try {
                    response = $.parseJSON(response); 
                } catch (error) {
                    console.log(error);                        
                    return;
                }
            }
			console.log(response);
			dat = response.data;
			if(response.code == 201){
				var v = "http://www.youtube.com/embed/"+ (dat['video'] != null ? dat['video'] : "");
				var youtube_embed = new DOMObj("iframe", {"src": v}, "<!--close-->");
				$("#yt_").html(youtube_embed.getHtml());
				console.log($("#yt_").html());
			}else if(response.code == 404){
				
			}else {
				
			}
		},
		error: function() {
			console.log("Error");
		}
	});
};

/* Cambiar Usuario */
var user_flag = false;

var CambiarUsuario = function() {
	user_flag = !user_flag;
	var btn_uchg = $("#change_user");
	var btn_usve = $("#save_user");
	
	$("#change_user").toggleClass("btn-primary btn-danger");
	
	if(user_flag){
		$("#change_user").text("Cancelar");
		$("#save_user").css("display", "block");
		$("#save_user").removeAttr("disabled");
		$("#usuario").removeAttr("disabled");
		$("#n_usuario").removeAttr("disabled");
		
	}else{
		$("#change_user").text("Editar");
		$("#save_user").css("display", "none");
		$("#save_user").attr("disabled", "disabled");
		$("#usuario").attr("disabled", "disabled");
		$("#n_usuario").attr("disabled", "disabled");
		
	}
};

var SalvarUsuario = function() {
	var antiguo = $("#usuario").val().trim();
	var nuevo = $("#n_usuario").val().trim();

	if(antiguo=="" || nuevo==""){
		$("#st_chg_user").html("Campo vacío");
		return;
	}
	else{
		$.ajax({
		data:{uedit:"",anick:antiguo,unick:nuevo},
		url:"login.php",
			type:"post",
			beforeSend:function(){

			},
			success:function(response){
				if(typeof response === 'string'){
                    try {
                        response = $.parseJSON(response); 
                    } catch (error) {
                        console.log(error);                        
                        return;
                    }
                }
				if(response.code==200){
					CambiarUsuario();
				}
				else if(response.code == 400){

				}
				$("#st_chg_user").html(response.message);
			},
			error:function(){

			}
		} );

	}
};


/* Cambiar Contrasenia */
var Bandera_Contrasenia = false;

var CambiarContrasenia= function() {
	Bandera_Contrasenia = !Bandera_Contrasenia;

	$("#change_password").toggleClass("btn-primary btn-danger");
	
	if(Bandera_Contrasenia){
		$("#change_password").text("Cancelar");
		$("#save_password").css("display", "block");
		$("#save_password").removeAttr("disabled");
		$("#contrasenia").removeAttr("disabled");
		$("#n_contrasenia").removeAttr("disabled");
		$("#rn_contrasenia").removeAttr("disabled");
	}else{
		$("#change_password").text("Editar");
		$("#save_password").css("display", "none");
		$("#save_password").attr("disabled", "disabled");
		$("#contrasenia").attr("disabled", "disabled");
		$("#n_contrasenia").attr("disabled", "disabled");
		$("#rn_contrasenia").attr("disabled", "disabled");
	}
};

var antiguo = "";
var nuevo = "";
var rnuevo = "";

var SalvarContraseña = function() {
	antiguo = $("#contrasenia").val().trim();
	nuevo = $("#n_contrasenia").val().trim();
	rnuevo = $("#rn_contrasenia").val().trim();
	
	if(Prep_Contrasenia()){
		$.ajax({
			data:{uedit:"",apsw:antiguo,upsw:nuevo},
			url:"login.php",
			type:"post",
			success:function(response){
				if(typeof response === 'string'){
					try {
						response = $.parseJSON(response); 
					} catch (error) {
						console.log(error);                        
						return;
					}
				}
				if(response.code==200){
					CambiarContrasenia();
					var al = new Alert(response.message, TypeAlert.Success, EnumDismiss.Dismiss);
					$("#st_chg_psw").html(al.get());
				}
				else if(response.code == 400 || response.code == 404){
					var al = new Alert(response.message, TypeAlert.Warning, EnumDismiss.Dismiss);
					$("#st_chg_psw").html(al.get());
				} else {
					var al = new Alert(response.message + " (" + response.code + ")", TypeAlert.Danger, EnumDismiss.Dismiss);
					$("#st_chg_psw").html(al.get());
				}

				antiguo = "";
				nuevo = "";
				rnuevo = "";
			},
			
			error:function(){
				var al = new Alert("Error procesando su peticion", TypeAlert.Success, EnumDismiss.Dismiss);
				$("#st_chg_psw").html(al.get());
				antiguo = "";
				nuevo = "";
				rnuevo = "";
			}
		} );
	}
};

var Prep_Contrasenia = function() {
	var resultado = true;
	if(antiguo == "")
		resultado = false;
	
	if(nuevo == "")
		resultado = false;
	
	if(rnuevo == "")
		resultado = false;
	
	if(!resultado){
		var al = new Alert("Existen Campos Vacios", TypeAlert.Warning, EnumDismiss.Dismiss);
		$("#st_chg_psw").html(al.get());
		return false;
	}
	
	if(nuevo != rnuevo){
		var al = new Alert("Las contraseñas son diferentes", TypeAlert.Warning, EnumDismiss.Dismiss);
		$("#st_chg_psw").html(al.get());
		return false;
	}
	
	if(nuevo.length < 4){
		var al = new Alert("La contrase&ntilde;a es muy corta.", TypeAlert.Warning, EnumDismiss.Dismiss);
		$("#st_chg_psw").html(al.get());
		return false;
	}
	
	return true;
};