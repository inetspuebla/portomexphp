var rec_num = 0;
var rec_msg = "";

var new_psw = {
    setrcvy: "",
    datpws: ""
}

var ResetPassMod = function(){
    if(PrepareDataRec()){
        $.ajax({
            data: new_psw,
            type: "post",
            url: "./control/recovery.php",
            beforeSend: function () { 

            },
            success: function (res) { 
                if(typeof response === 'string'){
                    try {
                        response = $.parseJSON(response); 
                    } catch (error) {
                        console.log(error);
                        var al = new Alert("Error De Servidor.", TypeAlert.Danger, EnumDismiss.None);
        				$("#EstatusContacto").html(al.get());
                        return;
                    }
                }

                HandleResponse_Recovery(res);
                if(res.code == 200 || res.code == 201){
                    var al = new Alert("Serás redireccionado en unos segundos... <br>Por favor Espera...", TypeAlert.Success, EnumDismiss.Dismiss);
                    $("#action_recovery_form .panel-footer").append(al.get());
                    console.log("Seras Redireccionado");
                    setTimeout (RedireccionarExitoso_Recovery, 2000);
                }
            },
            error: function () { 

            }
        });
    }else {
        var al = new Alert(rec_msg, TypeAlert.Warning, EnumDismiss.Dismiss);
        $("#action_recovery_form .panel-footer").html(al.get());
    }
};

var PrepareDataRec = function () {
    var s1 = $("#pws_one").val().trim();
    var s2 = $("#pws_two").val().trim();

    if(s1 == "" || s2 == "" ){
        rec_msg = "¡Campos Vac&iacute;os!";
        return false;
    }else if(s1 != s2){
        rec_msg = "¡Las contrase&ntilde;as son diferentes!";
        return false;
    }
    
    new_psw.datpws = s1;
    return true;
};

var HandleResponse_Recovery = function ( res ) { 
    if(res.code == 201 || res.code == 200){
        var al = new Alert(res.message, TypeAlert.Success, EnumDismiss.Dismiss);
        $("#action_recovery_form .panel-footer").html(al.get());
        $("#pws_one").val("");
        $("#pws_two").val("");
    }else if(res.code == 400 || res.code == 404){
        var al = new Alert(res.message+ "("+ res.code + ")", TypeAlert.Warning, EnumDismiss.Dismiss);
		$("#action_recovery_form .panel-footer").html(al.get());
    }else {
        var al = new Alert(res.message+ "("+ res.code + ")", TypeAlert.Danger, EnumDismiss.Dismiss);
		$("#action_recovery_form .panel-footer").html(al.get());
    }
};

var RedireccionarExitoso_Recovery = function () { 
    location.href = "acceso.php";
};