var enviarsugerencia = function () {
    var txt1 = $("#text1").val();
    var txt2 = $("#text2").val();
    var txt3 = $("#text3").val();

    $.ajax({
        data: {nombre:txt1, asunto:txt2, texto:txt3},
        url: "Sugerencia.php",
        type: "post",
        beforeSend: function(){
            console.log("Enviando...");
            $("#sendsugerencia").attr("disabled", "disabled");
        },
        success: function(response){
            RespuestaMsg(response);
        },
        error: function(){
            console.log("Error X_X");
            var alerta = new Alert("Error al procesar", TypeAlert.Danger, EnumDismiss.Dismiss);
            $("#estatussugerencia").html(alerta.get());
            $("#sendsugerencia").removeAttr("disabled");
        }
    });
};

var RespuestaMsg = function(response) {
	$("#sendsugerencia").removeAttr("disabled");
	
	var alerta = null; 
	
	if(response.code == 201){
		alerta = new Alert(response.message, TypeAlert.Success, EnumDismiss.Dismiss);		
		$("#text1").val("");
        $("#text2").val("");
        $("#text3").val("");
        $("#sendsugerencia").removeAttr("disabled");
	}else if(response.code == 403){
		alerta = new Alert(response.message, TypeAlert.Warning, EnumDismiss.Dismiss);
	}else{
		alerta = new Alert(response.code+ ":"+response.message, TypeAlert.Danger, EnumDismiss.None);
	}
	
	$("#estatussugerencia").html(alerta.get());
};