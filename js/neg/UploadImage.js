/**
 * 
 */

var EnviarImage = function() {
	var form = new FormData( document.getElementById("form-upload") );
	
	$.ajax({
		data: form,
		url: "uploader.php",
		type: "post",
		datatype: "html",
		cache: false,
		contentType: false,
		processData: false,
		beforeSend: function() {
			var al = new Alert("Enviando", TypeAlert.Info, EnumDismiss.None);
			$("#estatus_upload").html(al.get());
		},
		success: function(response) {
			HandleUploadImage(response);
		},
		error: function() {
			var al = new Alert("Error", TypeAlert.Danger, EnumDismiss.None);
			$("#estatus_upload").html(al.get());
		}
	});

};
var newnameimage = "";
var orgnameimage = "";

var RegistrarImagen = function(){
	var texto = $("#descripcion_imagen").val().trim();
	var ntipoimg = $("#sel_rtimg").attr("value");
	orgnameimage = $("#nombre_imagen").val().trim();
	$.ajax({
		data: { 
			name: newnameimage, 
			orgn: orgnameimage, 
			ntype: ntipoimg,
			tex : texto 
			},
		type:"post",
		url :"uploader.php",
		beforeSend: function() {
			$("#nombre_imagen").attr("disabled", "disabled");
			$("#descripcion_imagen").attr("disabled", "disabled");
			var al = new Alert("Enviando", TypeAlert.Info, EnumDismiss.None);
			$("#estatus_upload").html(al.get());
		},
		success:function(response){
			var ni = $("#nombre_imagen");
			var di = $("#descripcion_imagen");
			
			if(response.code == 201){
				ni.val("");
				di.jqteVal("");
				var al = new Alert(response.message, TypeAlert.Success, EnumDismiss.Dismiss);
				$("#estatus_upload").html(al.get());
				$("#image_upload_panel").toggleClass("hideElem");
				$("#pnl-desc").toggleClass("hideElem");
				var upload_input = new DOMObj( "input", 
					{
						"id": "archivo_imagen",
						"type":"file",
						"name": "uploadedfile",
						"accept": "image/*",
						"class": "btn btn-block btn-info",
						"onchange":"HandleOnChange_FileChooser();"
					});
				$("#inp_img_selct").html(upload_input.getHtml());
				var dimg = $("#imagen-up");
				dimg.attr("src","../images/no-thumbnail-square.png");
				dimg.attr("alt","No Image");
				$("#imgupload").attr("disabled","disabled");
				
				CargarTodo();
			}else if(response.code == 400){
				var al = new Alert(response.message, TypeAlert.Warning, EnumDismiss.Dismiss);
				$("#estatus_upload").html(al.get());
			}else {
				var al = new Alert(response.message+"("+response.code+")", TypeAlert.Danger, EnumDismiss.Dismiss);
				$("#estatus_upload").html(al.get());
			}
			
			ni.removeAttr("disabled");
			di.removeAttr("disabled");
		},
		error: function() {
			$("#nombre_imagen").removeAttr("disabled");
			$("#descripcion_imagen").removeAttr("disabled");
			var al = new Alert("Error", TypeAlert.Danger, EnumDismiss.None);
			$("#estatus_upload").html(al.get());
		}
	});
};

var HandleUploadImage = function(response) {
	var al = "";
	if(response.code == 201 )
	{
		newnameimage = response.data.newname;
		orgnameimage = response.data.nameorg;
		$("#imagen-up").attr("src","../res/"+newnameimage);
		$("#imagen-up").attr("alt",newnameimage);
		$("#nombre_imagen").val(orgnameimage);
		al = new Alert(response.message, TypeAlert.Success, EnumDismiss.Dismiss);
		$("#image_upload_panel").addClass("hideElem");
		$("#pnl-desc").removeClass("hideElem");
	}
	else if (response.code == 400)
	{
		al = new Alert(response.message, TypeAlert.Warning, EnumDismiss.Dismiss);
	}
	else
	{
		al = new Alert(response.message, TypeAlert.Danger, EnumDismiss.None);
	}
	
	$("#estatus_upload").html(al.get());
};

var Compare = function(type) {
	if(/^image\/\D*/.test(type)) {
		return true;
	}else{
		return false;
	}
};

var HandleOnChange_FileChooser = function() {
	var FileArray = document.getElementById('archivo_imagen').files;
	
	if(FileArray.length > 0){
		if(Compare(FileArray[0].type)){
			$("#imgupload").removeAttr("disabled");
			var al = new Alert("Pulse el bot&oacute;n <label>Subir archivo </label> para continuar", TypeAlert.Info, EnumDismiss.None);
			$("#estatus_upload").html(al.get() );
		}else{
			var al = new Alert("<b>Formato de archivo inv&aacute;lido.</b> Elija otro archivo por favor", TypeAlert.Warning, EnumDismiss.Dismiss);
			$("#imgupload").attr("disabled", "disabled");
			$("#estatus_upload").html(al.get());
		}
	}
};
