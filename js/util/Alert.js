/**
 * @file		Alert.js
 * @author		Miguel Vazquez
 * @description	Funciones para la creacion dinamicas de Alertas usando
 * 				las descripciones de Bootstrap 3
 */

/** 
 * TypeAlert Enum for Alert Bootstrap
 */
var TypeAlert = {
	Success: "alert-success",
	Info: "alert-info",
	Warning: "alert-warning",
	Danger: "alert-danger",
	None: ""
};

/**
 * EnumDismiss Enum for Dismissible Alert
 * */
var EnumDismiss = {
	Dismiss: "alert-dismissible",
	None: ""
};

/**
 * Button Dismissible
 * */
var button = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">" +
	"<span aria-hidden=\"true\">&times;</span>" + "</button>";

/**
 * Constructor Default
 * */
var Alert = function() {
	this.Message = "";
	this.Type = TypeAlert.None;
	this.Dismiss = EnumDismiss.None;
};

/**
 * Constructor with params
 * */
var Alert = function(_message, _typeAlert, _dismissible) {
	this.Message = _message;
	this.Type = _typeAlert;
	this.Dismiss = _dismissible;
};

/**
 * @method get
 * @return String HTML format representation
 * */
Alert.prototype.get = function() {
	
	if(this.Message == "")
		return "";	
	
	var l = "";
	if(this.Dismiss == EnumDismiss.Dismiss)
		l = button;
	
	return "<div class =\"alert " + this.Type + " " + this.Dismiss +"\">" + this.Message + l +"</div>";
};