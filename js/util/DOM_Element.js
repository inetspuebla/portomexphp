/**
 * @file 		DOM_Element.js
 * @author		Miguel Vazquez
 * @description	Funciones para contruir estructuras HTML dinamicamente
 * @notes		
 */

var DOMObj = function( label, prop , inner) {
	this.etq = label;
	this.inner = "";
	if(inner != undefined)
		this.inner = inner.trim();
	if(this.inner == "<!--close-->")
		this.inner = " ";
	this.data = prop;
};

DOMObj.prototype.getHtml = function() {
	var msg = "";
	msg += "<"+this.etq;
	
	for ( var i in this.data) {
		msg += " "+i+"=\""+this.data[i]+"\"";
	}

	if(this.inner != ""){
		msg +=" >";
		msg += this.inner + "</"+this.etq+">";
	} else {
		msg +=" />";
	}
	return msg;
};