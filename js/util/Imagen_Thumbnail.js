/**
 * @file 	Imagen_Thumbmail.js
 * @author	Miguel Vazquez
 * @description	Estructuras para la creacion de thumbnail´s de Bootstrap
 * @notes		"ImageGeneric() function" puede ser implementado por DOMObj() 
 * 				del archivo DOM_Element.js
 */

var ImagenThumbnail = function(_id, _src, _name) {
	this.id = _id;
	this.srcfile = _src;
	this.namefile = _name;
};

ImagenThumbnail.prototype.getHtml = function () {
	return "<img id=\""+this.id +"\" src=\""+this.srcfile+"\" alt=\""+this.namefile+"\">";
};

var ImageGeneric = function (_id, _src, _name, _class) {
	this.Id = _id;
	this.Src = _src;
	this.Alt = _name;
	this.Class = _class;
};

ImageGeneric.prototype.getHtml = function() {
	return "<img id=\""+this.Id+"\" src=\""+this.Src+"\" alt=\""+this.Alt+"\" class=\""+this.Class+"\" />";
};