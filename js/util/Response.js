/**
 * Response Object Class. Wrapper for JSON Responses
 *  */
var ResponseObj = function(){
    this.code = 0;
    this.message = "";
    this.data = Array();
};

var ResponseObj = function(_code, _message, _data){
	this.code = _code;
    this.message = _message;
    this.data = _data;
};

var ResponseObj = function(sResponse){
    if(typeof sResponse === 'string'){
        sResponse = $.parseJSON(sResponse);
    }
	this.code = sResponse['code'];
    this.message = sResponse['message'];
    this.data = sResponse['data'];
};