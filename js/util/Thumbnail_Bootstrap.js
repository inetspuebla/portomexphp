/**
 * @file		Thumbnail_Bootstrap.js
 * @author 		Miguel Vazquez
 * @description	Prototipo para construir Thumbnails con Bootstrap 3. 
 * @require 	"Imagen_Thumbnail.js"
 */

var ThumbnailBootstrap = function( _imgthumbnail, _class, _caption) {
	this.imgthumnail = _imgthumbnail;
	this.classattr = _class;
	this.captionbody = _caption;
};

ThumbnailBootstrap.prototype.getHtml = function(){
	var msg = "<div class=\""+this.classattr+" \">";
	msg += "<div class=\"thumbnail\">" ;
	msg += this.imgthumnail.getHtml();
	
	if( this.captionbody != undefined )
		if( this.captionbody.trim() != "" )
			msg += "<div class=\"caption\">" + this.captionbody.trim() + "</div>";
	msg += "</div>";
	msg += "</div>";
	
	return msg;
};
