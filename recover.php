<?php
$accion = 0;
$message = "";
try{
    if($_SERVER['REQUEST_METHOD'] == "GET"){
        if(isset($_GET['peticion'])){
            $numero = $_GET['peticion'];

            include_once("./control/core.php");

            $bd_conn = $bd_conn= ConexionBD::obtenerInstancia()->obtenerBD();

            $sql = "SELECT validkey as apikey, user as uid, email as mail  FROM recuperacion WHERE validkey = ? and activo = 1";

            $prep = $bd_conn->prepare($sql);
            $prep->bindParam(1, $numero);

            $res = $prep->execute();

            if($res){
                $arr = $prep->fetchAll(PDO::FETCH_ASSOC);
                if(count($arr) == 1){
                    $arr = $arr[0];

                    $sesion = new SesionManager();
                    $sesion->SetValue('DataRec', $arr);

                    $accion = 1;
                    $message = "Ok";
                }else {
                    throw new PetitionException("<b>No existe la solicitud</b><br>Direci&oacute;n Inv&aacute;lida o Petici&oacute;n ya ha sido procesada. ", 404);
                }
            }else {
                throw new PetitionException("No se pudo proces&oacute; su consulta", 400);
            }
        }
        else {
            header("Location: acceso.php");
            exit();
        }
    }
    else
    {
        header("Location: acceso.php");
        exit();
    }
}
catch (PetitionException $ptex){
	$code = $ptex->getCode();
	$message = $ptex->getMessage();
}
catch (PDOException $pdoex)
{
	$code = $pdoex->getCode();
	$message = "Error con la Base de Datos";
}
catch(Exception $ex){
    $accion = $ex->getCode();
    $message = "Error Con el Servidor";
}        
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>M&oacute;dulo de Recuperaci&oacute;n de Contrase&ntilde;a</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Local -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="./css/bootstrap-theme.min.css">
    <!-- Remote-->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>
<div style="background-color:rgba(0, 200, 0, 0.6); height:100px;text-align:center;">&nbsp;</div>
<br><br>
<div class="container">
    <div clas="row">
        <div class="col-xs-8 col-xs-offset-2 col-md-6 col-md-offset-3">
        <?php

        if($accion == 1){
         echo '<div id="action_recovery_form" class="panel panel-default">
            <div class="panel-heading">
                <h3 >Recuperaci&oacute;n de contrase&ntilde;a</h3>
            </div>
            <div class="panel-body">
                <label>Escriba su nueva contrase&ntilde;a</label>
                <input id="pws_one" type="password" placeholder="*****" class="form-control">
                <label>Escriba su nueva contrase&ntilde;a</label>
                <input id="pws_two" type="password" placeholder="*****" class="form-control">
                <br>
                <input type="button" value="Enviar" onclick="ResetPassMod();" class="btn btn-success"/>
            </div>
            <div class="panel-footer"></div> 
        </div>';
        } else if($accion = 400 or $accion == 404){
            echo '<div id="error_recovery_form" class="panel panel-warning">
            <div class="panel-heading">
                <h3>Hubo un problema procesando su actividad.</h3>
            </div>
            <div class="panel-body" style="text-aling:center;">
                <label> Error: '.$accion.'.</label>
                <br>
                <label>Descripci&oacute;n</label>:'.$message.'
                <br><br>
                <a href="acceso.php" class="btn">Ir a la P&aacute;gina de Acceso al Panel</a>
                <br>
                <a href="." class="btn">Ir a la P&aacute;gina Principal</a>
            </div>
        </div>';
        }
        ?>
        </div>
    </div>
</div>
    <!-- Scripts Area-->
    <!-- Local -->
    <!-- JQuery -->
    <script type="text/javascript" src="./js/jquery-2.2.4.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    <!-- Remote -->
    <!-- JQuery -->
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    
    <script type="text/javascript" src="./js/util/Alert.js"></script>
<?php
    if($accion == 1){
        echo '<script type="text/javascript" src="./js/neg/Recovery.js"></script>';
    };
?>
</body>
</html>